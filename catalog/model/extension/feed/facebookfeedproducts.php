<?php 
class ModelExtensionFeedFacebookFeedProducts extends Model {
     
     public function getGoogleCategorysByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$category_id . "'");

	return $query->row;
	}

    public function getCategorys() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_category");

        return $query->rows;
    }
    public function getProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
        if ($query->num_rows) {
            return array(
                'product_id'       => $query->row['product_id'],
                'name'             => $query->row['name'],
                'description'      => $query->row['meta_description'],
                'model'            => $query->row['model'],
                'sku'              => $query->row['sku'],
                'quantity'         => $query->row['quantity'],
                'image'            => $query->row['image'],
                'tax_class_id'     => $query->row['tax_class_id'],
                'manufacturer_id'  => $query->row['manufacturer_id'],
                'manufacturer'     => $query->row['manufacturer'],
                'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
                'special'          => $query->row['special']
            );
        } else {
            return false;
        }
    }

    public function getProducts() {
        $sql = "SELECT p.product_id, g2c.* FROM " . DB_PREFIX . "google_category g2c LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = g2c.category_id) LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id) WHERE p.quantity > 0 AND p.status = 1 ";


        $product_data = array();

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            $product_data[$result['product_id']]['google_category'] = ($result['google_category_id'])?$result['google_category_id']:$result['google_main_category_id'];
        }

        return $product_data;
    }
    
}