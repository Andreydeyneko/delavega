<?php
class ModelLocalisationBannerZone extends Model {
	public function getBannerZonebyCode($region_cod) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_zone bz LEFT JOIN " . DB_PREFIX . "banner_zone_text bzt ON (bz.zone_id = bzt.zone_id) WHERE code = '" . $region_cod . "' AND bzt.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        if (!$query->num_rows){
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_zone bz LEFT JOIN " . DB_PREFIX . "banner_zone_text bzt ON (bz.zone_id = bzt.zone_id) WHERE code = '00' AND bzt.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        }

		return $query->row;
	}
    public function getPromoCodeByCustomer($customer_id) {
        $query = $this->db->query("SELECT coupon_id FROM " . DB_PREFIX . "customer_coupon WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['coupon_id'];
    }

    public function getCustomerByPhone($phone) {
        $query = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customer WHERE telephone = '" . $this->db->escape($phone) . "'");

        return $query->row['customer_id'];
    }

    public function addCoupon($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "coupon SET name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', discount = '" . (float)$data['discount'] . "', type = '" . $this->db->escape($data['type']) . "', total = '" . (float)$data['total'] . "', logged = '" . (int)$data['logged'] . "', shipping = '" . (int)$data['shipping'] . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "', uses_total = '" . (int)$data['uses_total'] . "', uses_customer = '" . (int)$data['uses_customer'] . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

        $coupon_id = $this->db->getLastId();

        return $coupon_id;
    }

    public function addCouponToCustomer($coupon_id,$customer_id) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_coupon SET customer_id = '" . (int)$customer_id . "', coupon_id = '" . (int)$coupon_id . "'");

        $customer_coupon_id = $this->db->getLastId();

        return $customer_coupon_id;
    }

    public function checkCoupon($code) {
        $query = $this->db->query("SELECT coupon_id FROM " . DB_PREFIX . "coupon WHERE code = '" . $this->db->escape($code) . "'");

        return $query->row['coupon_id'];
    }

}