<?php
class ModelLocalisationLocation extends Model {
    public function getLocation($location_id) {
        $query = $this->db->query("SELECT location_id, name, address, geocode, telephone, fax, image, open, comment FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");

        return $query->row;
    }

    public function getLocations() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location l 
		 LEFT JOIN " . DB_PREFIX . "location_description ld 
		 ON (l.location_id = ld.location_id) 
		 WHERE ld.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY l.sort ASC");

        return $query->rows;
    }
}