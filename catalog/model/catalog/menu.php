<?php
class ModelCatalogMenu extends Model {
    public function getMenuFooter($parent_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "menu m 
        LEFT JOIN " . DB_PREFIX . "menu_description md
        ON (m.menu_id = md.menu_id) 
        WHERE m.parent_id = '" . (int)$parent_id . "' 
        AND md.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY m.sort_order ASC");

        if ($query->num_rows) {
            return $query->rows;
        } else {
            return false;
        }
    }

    public function getMenuFooterParentId($parent_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "menu m 
        LEFT JOIN " . DB_PREFIX . "menu_description md
        ON (m.menu_id = md.menu_id) 
        WHERE m.parent_id = '" . (int)$parent_id . "' 
        AND md.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY m.sort_order ASC");

        if ($query->num_rows) {
            return $query->rows;
        } else {
            return false;
        }
    }

    public function getMenuHeader($parent_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "menu_header m 
        LEFT JOIN " . DB_PREFIX . "menu_header_description md
        ON (m.menu_id = md.menu_id) 
        WHERE m.parent_id = '" . (int)$parent_id . "' 
        AND md.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY m.sort_order ASC");

        if ($query->num_rows) {
            return $query->rows;
        } else {
            return false;
        }
    }

    public function getMenuHeaderParentId($parent_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "menu_header m 
        LEFT JOIN " . DB_PREFIX . "menu_header_description md
        ON (m.menu_id = md.menu_id) 
        WHERE m.parent_id = '" . (int)$parent_id . "' 
        AND md.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY m.sort_order ASC");

        if ($query->num_rows) {
            return $query->rows;
        } else {
            return false;
        }
    }
}