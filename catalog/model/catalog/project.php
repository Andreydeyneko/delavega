<?php
class ModelCatalogProject extends Model {

    public function getProjects($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "project i 
        LEFT JOIN " . DB_PREFIX . "project_description id ON (i.project_id = id.project_id) 
        WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' 
        AND i.status = '1' ORDER BY i.sort_order ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 6;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        if($query->num_rows){
            return $query->rows;
        } else {
            return false;
        }
    }

    public function getProjectImages($project_id) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_image WHERE project_id = '" . (int)$project_id . "' ORDER BY sort_order ASC");

        if($query->num_rows){
            return $query->rows;
        } else {
            return false;
        }
    }

    public function getProductRelated($product_id) {
        $product_data = array();

        $this->load->model('catalog/product');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_related pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) WHERE pr.project_id = '" . (int)$product_id . "' AND p.status = '1' AND p.date_available <= NOW()");

        foreach ($query->rows as $result) {
            $product_data[$result['related_id']] = $this->model_catalog_product->getProduct($result['related_id']);
        }

        return $product_data;
    }

    public function getTotalProjects() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project");

        return $query->row['total'];
    }

    public function getProjectPageDescriptions() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_page WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }
}