
<header id="header" class=" variant typeheader-<?php echo isset($typeheader) ? $typeheader : '1'?>">

	<!-- HEADER TOP -->
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-sm-3 col-xs-12">
        <div id="logo">
			<?php if ($logo) { ?>
				<?php if ($class == 'common-home') { ?>
					<img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
				<?php } else { ?>
					<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
				<?php } ?>
			<?php } else { ?>﻿﻿
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
				</div>
				<div class="col-lg-6 col-sm-6 col-xs-12">
					<div class="header_search">
						<div class="search-header-content">
							<a href="javascript:void(0)" title="Close" class="btn-search">
								<span class="hidden">Search</span>
							</a>
							<!-- <button id="dropdownSearch"  class="dropdown-toggle bt-search hidden" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-search" aria-hidden="true"></i>	</button> -->
							<div class="dropdown-menu-search">
							<?php  echo $content_search; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-3 col-xs-12 ">
					<div class="custom">
						<div class="block-cart">
							<div class="shopping_cart">
							 	<?php echo $cart; ?>
							</div>
						</div>
						<div class="setting">
							<div class="dropdown-toggle">
								<i class="fa fa-cog" aria-hidden="true"></i>
							</div>
							<div class="dropdown-menu">
								<div class="box-lang-cunrent">
									<?php if($lang_status):?>	
										<div class="switcher"> <?php echo $currency; ?> </div>
										<div class="switcher"><?php echo $language; ?></div>
									<?php endif; ?>

								</div>
								<div class="account" id="my_account1"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="btn-xs dropdown-toggle" data-toggle="dropdown"> <span class="dropdown-toggle"><?php echo $text_account; ?></span></a>
									<ul class="dropdown">
										<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
										<li class="wishlist"><a href="<?php echo $wishlist; ?>" id="wishlist-total" class="top-link-wishlist" title="<?php echo $text_wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
										<li class="compare"><a href="<?php echo $compare; ?>" id="compare-total" class="top-link-compare" title="<?php echo $text_compare; ?>"><?php echo $text_compare; ?></a></li>
										<?php if ($logged) { ?>
										<li class="log logout"><a class="link-lg" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
										
										<?php } else { ?>
										<li class="log login"><a class="link-lg" href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
										<?php } ?>
									</ul>
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>	
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="row">			
				<div class="text-left col-md-3 col-xs-6">
					<div class="menu-vertical">
						<?php echo $content_menu1; ?>
					</div>
						
				</div>
				
				<div class="col-md-9 col-xs-6">
					<!-- BOX CONTENT MENU -->
					<?php if ($header_block1) : ?>	
						<div class="header-block1 hidden-xs hidden-lg">
						<?php echo $header_block1; ?>
						</div>
					<?php endif; ?>
					<?php echo $content_menu; ?>		
					<?php if ($header_block1) : ?>	
						<div class="header-block1 hidden-xs hidden-md hidden-sm">
						<?php echo $header_block1; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Navbar switcher -->
	<?php if (!isset($toppanel_status) || $toppanel_status != 0) : ?>
	<?php if (!isset($toppanel_type) || $toppanel_type != 2 ) :  ?>
	<div class="navbar-switcher-container">
		<div class="navbar-switcher">
			<span class="i-inactive">
				<i class="fa fa-caret-down"></i>
			</span>
			 <span class="i-active fa fa-times"></span>
		</div>
	</div>
	<?php endif; ?>
	<?php endif; ?>
</header>