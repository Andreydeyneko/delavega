<?php
$tag_id = 'so_basic_products_'.$moduleid.'_'.rand().time();
$class_respl0 = 'preset00-'.$nb_column0.' preset01-'.$nb_column1.' preset02-'.$nb_column2.' preset03-'.$nb_column3.' preset04-'.$nb_column4;
?>
<div class="module <?php echo $class_suffix; ?>">
	<?php if($disp_title_module) { ?>
		<h3 class="modtitle"><?php echo $head_name; ?></h3>
	<?php } ?>
	<?php if($pre_text != ''){?>
		<div class="form-group">
			<?php echo html_entity_decode($pre_text);?>
		</div>
	<?php } ?>
	<div class="modcontent">
		<div class="so-basic-product" id="<?php echo $tag_id ?>">
		<?php if(!empty($products)){?>
		<?php 
			switch($layout_theme){
				case 'default':
					include ("layout_default.tpl");
				break;
				case 'acc_vertical':
					include ("layout_acc_vertical.tpl");
				break;
			}
		?>
		<?php }else{ 
			echo $objlang->get('text_noproduct');
		}?>
	</div>
	</div> <!-- /.modcontent-->
	<?php if($post_text != ''){?>
		<div class="form-group">
			<?php echo html_entity_decode($post_text);?>
		</div>
	<?php }?>
</div>
