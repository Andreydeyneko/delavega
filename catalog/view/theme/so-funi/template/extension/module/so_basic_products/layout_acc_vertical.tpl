<div class="so-acc-vertical">
	<ul class="yt-accordion">
	<?php $j = 0; foreach($products as $product)  { $j++; ?>
		<li class="yt-acc-item <?php if($j== 1) echo 'enable';?>">
			<div class="acc-heading">
				<h3>
					<?php if($display_title){ 
						echo html_entity_decode($product['name_maxlength']); 
					}?>
				</h3>		
			</div>
			<div class="acc-inner product-thumb">
				<div class="image">
					<?php if ($product['special'] && $display_sale) : ?>
						<span class="label label-sale"><?php echo $objlang->get('text_sale'); ?></span>
					<?php endif; ?>
					<?php if ($product['productNew'] && $display_new) : ?>
						<span class="label label-new"><?php echo $objlang->get('text_new'); ?></span>
					<?php endif; ?>
					<?php if($product_image) { ?>
						<a href="<?php echo $product['href'];?>" target="<?php echo $item_link_target;?>" title= "<?php echo $product['name'] ?>">
							<?php if($product_image_num ==2){?>
								<img src="<?php echo $product['thumb']?>" class="img-thumb1" alt="<?php echo $product['name'] ?>">
								<img src="<?php echo $product['thumb2']?>" class="img-thumb2" alt="<?php echo $product['name'] ?>">
							<?php }else{?>
								<img src="<?php echo $product['thumb']?>" alt="<?php echo $product['name'] ?>">
							<?php }?>
						</a>
					<?php } ?>
				</div>
				<div class="acc-inner-content">
				<?php if ($display_rating) { ?>
					<div class="rating">
					  <?php for ($i = 1; $i <= 5; $i++) { ?>
					  <?php if ($product['rating'] < $i) { ?>
						<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
						<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } ?>
					  <?php } ?>
					</div>
				<?php } ?>
				<?php if ($display_price) { ?>
					<?php if ($product['price']) { ?>
					<p class="price">
					  <?php if (!$product['special']) { ?>
					  <?php echo $product['price']; ?>
					  <?php } else { ?>
					  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
					  <?php } ?>
					  <?php if ($product['tax']) { ?>
					  <span class="price-tax"><?php echo $objlang->get('text_tax'); ?> <?php echo $product['tax']; ?></span>
					  <?php } ?>
					</p>
					<?php } ?>
				<?php } ?>
				<?php if($display_description){ ?>
					<div class="acc-inner-desc">
						<?php echo html_entity_decode($product['description_maxlength']); ?>
					</div>
				<?php } ?>
				</div>
			<?php if($display_add_to_cart || $display_wishlist || $display_compare)	{	?>
				<div class="button-group">
				<?php if ($display_add_to_cart) { ?>
					<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $objlang->get('button_cart'); ?></span></button>
				<?php } ?>
				<?php if ($display_wishlist) { ?>
					<button type="button" data-toggle="tooltip" title="<?php echo $objlang->get('button_wishlist'); ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
				<?php } ?>
				<?php if ($display_compare) { ?>
					<button type="button" data-toggle="tooltip" title="<?php echo $objlang->get('button_compare'); ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
				<?php } ?>
				</div>
			<?php }	?>
			</div>
		</li>
	<?php }?>
	</ul>
</div>
<script>
jQuery(document).ready(function($) {
	$("ul.yt-accordion li").each(function() {
		if($(this).hasClass('enable')){
			$(this).addClass("active");
			$(this).children(".acc-inner").slideDown('slow');	
		}
		var ua = navigator.userAgent,
		event = (ua.match(/iPad/i)) ? "touchstart" : "click";
		$('.acc-heading',this).bind(event, function() {
			$(this).parent().parent().children("li").removeClass("active");
			$(this).parent().parent().children("li").children(".acc-inner").slideUp('slow');
			$(this).parent().addClass("active");
			$(this).parent().children(".acc-inner").slideDown('slow');	
		});
	});
});
</script>