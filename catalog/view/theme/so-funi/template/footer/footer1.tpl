
<footer class="footer-container typefooter-<?php echo isset($typefooter) ? $typefooter : '1'?>">


	<!-- FOOTER TOP -->
	<?php if ($footertop) : ?>
	<div class="footer-top">
		<div class="container">
				<?php echo $footertop; ?> 
		</div>
		
	</div>
	<?php endif; ?>

	<!-- FOOTER CENTER -->
	<div class="footer-middle">
		<?php if(isset($backtop) && $backtop== 1):?>
			<div class="back-to-top">
				<div class="icon">
				<i class="fa fa-angle-double-up"></i>
				</div>
			</div>
		<?php endif; ?>
		
		<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<?php if ($footer_block1) : ?>	
					<?php echo $footer_block1; ?>
				<?php endif; ?>
				<?php if (isset($imgpayment_status) && $imgpayment_status != 0) : ?>
				<div class="imgpayment">
					<?php
					if ((isset($imgpayment) && $imgpayment != '') ) { ?>
						<img src="image/<?php echo  $imgpayment ?>"  alt="imgpayment">
					<?php } ?>
				</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box-sevicer">
				<div class="module clearfix">
					<h3 class="footer-title"><?php echo $text_service; ?></h3>
					<div  class="modcontent" >
						<ul class="menu">
							<li><a title="<?php echo $text_contact; ?>" href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
							<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
							<li><a title="<?php echo $text_wishlist; ?>" href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
				       		<li><a title="<?php echo $text_sitemap; ?>" href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
							<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
							<li><a title="<?php echo $text_order; ?>" href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
						</ul>
					</div>
				</div>
			
			</div>
			<!-- BOX INFOMATION --> 
			<?php if ($informations) :?>
				<div class="col-sm-6 col-md-3 col-sm-6 box-information">
					<div class="module clearfix">
						<h3 class="footer-title"><?php echo $text_information; ?></h3>
						<div  class="modcontent" >
							<ul class="menu">
								<?php foreach ($informations as $information) { ?>
								<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="module clearfix">
					<h3 class="footer-title"><?php echo $text_account; ?></h3>
					<div  class="modcontent" >
						<ul class="menu">
							<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
							<li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
							<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
							<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
							<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
							<li><a title="<?php echo $text_return; ?>" href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>	
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		</div>
	</div>

	<!-- FOOTER BOTTOM -->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<?php if ($footerbottom) : ?>	
				<div class="col-lg-6 col-md-6  col-xs-12 pull-right">
					<?php echo $footerbottom; ?>
				</div>
				<?php endif; ?>
				<div class="col-lg-6 col-md-6  col-xs-12 copyright-footer pull-left">
					<?php 
					$datetime = new DateTime();
					$cur_year	= $datetime->format('Y');
					echo (!isset($copyright) || !is_string($copyright) ? $powered : str_replace('{year}', $cur_year,html_entity_decode($copyright, ENT_QUOTES, 'UTF-8')));?>
				</div>
			</div>
		</div>
	</div>
</footer>