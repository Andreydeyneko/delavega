function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

if ($(window).width() > 1220) {
	$('.click_only_class').click(function(event){
		event.preventDefault();
	});
}

// START feedback form send
$(document).on('submit', '#feedback-form', function (event) {
    event.preventDefault();

    var $that = $(this),
        data_form = new FormData($that.get(0));

    $.ajax({
        contentType: false,
        processData: false,
        url: 'index.php?route=common/footer/feedbackFormSend',
        type: 'post',
        data: data_form,
        dataType: 'json',
        beforeSend: function() {
            $('.question-form__col').removeClass('error');
        },
        success: function (json) {
            if (json['error']) {
                $.each(json['error'], function (key, value) {
                    var _field = $("input[name='" + key + "']");
                    _field.parent().addClass('error');
                });

            }
            if (json['success']) {
                $('#feedback-form input').val('');
                $('.popup--thanks').addClass('active');
                datalayerpush();
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});


// START promo code send
$(document).on('click', '#discount__button_send', function (event) {
    event.preventDefault();

    var data_form = $('.discount input[name=\'form_name\'],.discount input[name=\'form_phone\'],.discount input[name=\'discount\']');
    $("input[name='form_phone']").parent().removeClass('error');
    $("input[name='form_name']").parent().removeClass('error');
    $('.discount__row .error').remove();


    $.ajax({
        url: 'index.php?route=common/footer/getPromoCode',
        type: 'post',
        data: data_form,
        dataType: 'json',
        beforeSend: function() {
            $('.popup--discounts').removeClass('error');
        },
        success: function (json) {
            if (json['error']) {
                $.each(json['error'], function (key, value) {

                	if (key=='coupon'){
                        $("input[name='form_phone']").parent().addClass('error');
                        $("input[name='form_name']").parent().addClass('error');
                        $('#discount__button_send').after('<p class="error" style="text-align: center;width: 100%;bottom: 15px;">'+value+'</p>');

                    }
                    if (key=='form_phone'){
                        $("input[name='form_phone']").parent().addClass('error');
                        $('.discount__row-parent input[name=\'form_phone\']').after('<p class="error">'+value+'</p>');
                    }
                    if (key=='form_name'){
                        $("input[name='form_name']").parent().addClass('error');
                        $('.discount__row-parent input[name=\'form_name\']').after('<p class="error">'+value+'</p>');
                    }
                });

            }
            if (json['success']) {
                $('.popup--discounts input').val('');
                $('.popup--discounts').removeClass('active');
                $('.popup--discounts-ok').addClass('active');
                dataLayer.push({'event':'form_promo'});

            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});
// END promo code send


// START Contact form send
$(document).on('click', '#contact_form_send', function (event) {
    event.preventDefault();

    var data_form = $('#contact_form input[name=\'form_name\'], #contact_form input[name=\'form_phone\'], #contact_form input[name=\'form_email\']');
    $.ajax({
        url: 'index.php?route=common/footer/contactFormSend',
        type: 'post',
        data: data_form,
        dataType: 'json',
        beforeSend: function() {
            $('.question-form__col').removeClass('error');
        },
        success: function (json) {
            if (json['error']) {
                $.each(json['error'], function (key, value) {
                    var _field = $("input[name='" + key + "']");
                    _field.parent().addClass('error');
                });

            }
            if (json['success']) {
                $('#contact_form input').val('');
                $('.popup--thanks').addClass('active');
				datalayerpush();
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});
// END Contact form send

// START subscribe form
$(document).on('click', '#subscribe-button', function (event) {
    event.preventDefault();

    $.ajax({
        url: 'index.php?route=common/footer/subscribeForm',
        type: 'post',
        data: $('input[name=\'subscribe_email\']'),
        dataType: 'json',
        beforeSend: function() {
            $('input[name=\'subscribe_email\']').removeClass('error');
        },
        success: function (json) {
            if (json['error']) {
                $.each(json['error'], function (key, value) {
                    var _field = $("input[name='" + key + "']");
                    _field.addClass('error');
                });
            }
            if (json['success']) {
                $('#subscribe-form input').val('');
                $('.popup--subscribe').addClass('active');
                dataLayer.push({'event':'form_subscription'});
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});
// END subscribe form

// START resend password
$(document).on('click', '#resend-button', function (event) {
    event.preventDefault();

    $.ajax({
        url: 'index.php?route=account/register_confirm/resendPassword',
        type: 'post',
        dataType: 'json',
        success: function (json) {
            if (json['success']) {
                $('.popup--text').addClass('active');
                $(this).css('pointer-events', 'none');
                setTimeout(function() {
                    $(this).removeAttr('style');
                }, 5000);

            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});
// END resend password

// START resend password (login)
$(document).on('click', '#resend-login-button', function (event) {
    event.preventDefault();

    $.ajax({
        url: 'index.php?route=account/login_confirm/resendPassword',
        type: 'post',
        dataType: 'json',
        success: function (json) {
            if (json['success']) {
                // ЗДЕСЬ НОВЫЙ ПОПАП БУДЕТ
                $('.popup--password').addClass('active');
                $(this).css('pointer-events', 'none');
                setTimeout(function() {
                    $(this).removeAttr('style');
                }, 60000);

            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});
// END resend password (login)

if ($('#total-products').text() === '0') {
    $('#total-products').text()
}


$('.remove-wishlist').on('click', function (event) {
	event.preventDefault();
	$('.popup--choose').addClass('active');
	$('#confirm-remove-wishlist').attr('href', $(this).attr('href'));
})

$(document).ready(function() {
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#form-currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#form-currency input[name=\'code\']').val($(this).attr('name'));

		$('#form-currency').submit();
	});

	// Language
	$('#form-language .language-select').on('click', function(e) {
		e.preventDefault();

		$('#form-language input[name=\'code\']').val($(this).attr('name'));

		$('#form-language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
		var url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('#search input[name=\'search\']').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('#search input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 10) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
		$('#grid-view').removeClass('active');
		$('#list-view').addClass('active');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		var cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		$('#list-view').removeClass('active');
		$('#grid-view').addClass('active');

		localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
		$('#list-view').addClass('active');
	} else {
		$('#grid-view').trigger('click');
		$('#grid-view').addClass('active');
	}

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});
});

// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('.alert-dismissible, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
					}, 100);

					$('html, body').animate({ scrollTop: 0 }, 'slow');

					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
    'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/refresh_popup_data',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    // update in cart and checkout
                    $('.popup_cart_total[data-prod-id="'+key+'"]').html(json['product_total']);
                    $('.popup_cart_quantity[data-prod-id="'+key+'"]').val(json['quantity_cart']);
                    $('#total-products').html(json['all_products']);
                }, 500);
            }
        });
    },
    'remove': function(key) {
        $.ajaxSetup ({cache: false});
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            success: function(json) {

                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart_quantity').html(json['total']);
                    $('#total-products').html(json['total']);
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    setTimeout(function(){
                        $('#cart_re').load('index.php?route=common/cart/info');
                    },100);
                }
                if(json['total']==0){
                    location.reload();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'updatesmcart': function(key, quantity) {
        $.ajaxSetup ({cache: false});
        $.ajax({
            url: 'index.php?route=checkout/cart/editsmcart',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                var timeerr = 100;
                if(json.error){
                    $('.amount-input-block').addClass('error');
                    timeerr = 1000;
                }
                setTimeout(function () {
                    $('#cart_quantity').html(json['total']);
                    $('#total-products').html(json['total']);
                    setTimeout(function(){
                        $('#cart_re').load('index.php?route=common/cart/info');
                        setTimeout(function () {
                            $('.amount-input-block').removeClass('error');
                        }, 500);
                    },100);
                }, timeerr);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {

				if (json['not_logged']) {
					$('.popup--login').addClass('active');
				}

				if (json['success']) {
                    $('#wishlist-total').html(json['total']);
                    $('.add-to-favorite[data-id="'+product_id+'"]').addClass('active');
                    $('.add-to-favorite[data-id="'+product_id+'"]').attr('onclick', 'wishlist.remove('+product_id+')');

                }
			},
			error: function(xhr, ajaxOptions, thrownError) {
				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/remove',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                if (json['success']) {
                    $('#wishlist-total').html(json['total']);
                    $('.add-to-favorite[data-id="'+product_id+'"]').removeClass('active');
                    $('.add-to-favorite[data-id="'+product_id+'"]').attr('onclick', 'wishlist.add('+product_id+')');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert-dismissible').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div>';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			}

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			}

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	}
})(window.jQuery);
