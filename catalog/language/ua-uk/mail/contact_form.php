<?php
// Text
$_['text_subject']    = 'Зворотний дзвінок - Delavega';
$_['text_name']       = 'Ім\'я: %s';
$_['text_email']      = 'Email: %s';
$_['text_phone']      = 'Телефон: %s';
$_['subject']    = 'Запит персональної знижки';
$_['text_body']    = 'Покупець хоче отримати персональну знижку';
$_['text_code']    = 'Код %s на знижку %s';

$_['text_message'] = 'Питання';
$_['text_subject_feedback'] = 'Задати питання';