<?php
// Text
$_['text_subject']      = '%s - Ваше замовлення №%s сформоване';
$_['text_greeting']         = 'Дякуємо Вам за інтерес до товарів Фабрики м\'яких меблів преміум-класу <a href="%s" style="color: #fff; line-height: 1.8; text-decoration: none; font-weight: 600;">Delavega.ua</a> <br>Ваше замовлення отримане і надійде в обробку найближчим часом';
$_['text_title']         = 'Українська фабрика м\'яких меблів преміум-класу';
$_['text_in_order']         = 'У Вашому замовленні:';
$_['text_link']             = 'Для просмотра Вашего заказа перейдите по ссылке:';
$_['text_order_detail']     = 'Детализация заказа';
$_['text_instruction']      = 'Инструкции';
$_['text_order_id']         = 'Замовлення:';
$_['text_date_added']       = 'Дата замовлення:';
$_['text_order_status']     = 'Статус:';
$_['text_payment_method']   = 'Спосіб оплати:';
$_['text_shipping_method']  = 'Спосіб доставки:';
$_['text_email']            = 'E-mail:';
$_['text_telephone']        = 'Телефон:';
$_['text_ip']               = 'IP-адрес:';
$_['text_payment_address']  = 'Адрес плательщика';
$_['text_shipping_address'] = 'Адрес доставки';
$_['text_products']         = 'Товары';
$_['text_product']          = 'Товар';
$_['text_model']            = 'Модель';
$_['text_quantity']         = 'К-сть:';
$_['text_sht']              = 'шт';
$_['text_price']            = 'Ціна:';
$_['text_order_total']      = 'Заказ итого';
$_['text_total']            = 'Разом';
$_['text_social']           = 'Ми в соціальних мережах';
$_['text_control']          = 'Відділ контролю якості:';
$_['text_current_year']     = '© %s %s';
$_['text_download']         = 'После подтверждения оплаты, загружаемые товары будут доступны по ссылке:';
$_['text_comment']          = 'Комментарий к Вашему заказу:';
$_['text_footer']           = '© 2010-%s Delavega.ua. Всі права захищені';
$_['pay_onlayn_text'] = '<ul style="list-style: none;line-height: 2;"><li>Реквізити для оплати онлайн:</li><li>ФОП Підгорний Артур Григорович</li><li>ЄДРПОУ 2522711672, тел. 0501416587</li><li>Р/р UA183206490000026004052624358 в ФКВ "ПРИВАТБАНК", РОЗРАХУНКОВИЙ ЦЕНТР</li><li>МФО 320649</li><li>Адреса: м. Київ, вул. Ревуцького, 11А, кв. 199</li><li><br/></li><li>ВАЖЛИВО: Не проводьте оплату замовлення без попереднього узгодження деталей замовлення з нашим менеджером</li></ul>';


