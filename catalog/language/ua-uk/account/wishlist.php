<?php
// Heading
$_['heading_title'] = 'Особистий кабінет';

// Text
$_['text_account'] = 'Особистий Кабінет';
$_['text_instock'] = 'наявності';
$_['text_wishlist'] = 'Закладки (%s)';
$_['text_login'] = 'Ви повинні <a href="%s \ ">увійти</a> або <a href="%s">створити обліковий запис</a>, щоб зберегти <a href="%s">%s</a> ваш <a href="%s">список закладок</a>!';
$_['text_success'] = 'Ви додали <a href="%s">%s</a> <a href="%s">Закладки</a>!';
$_['text_exists'] = '<a href="%s">%s</a> вже є <a href="%s">закладки</a>!';
$_['text_remove'] = 'Список закладок успішно змінено!';
$_['text_empty'] = 'Ваші закладки порожні';
$_['text_empty_wishlist'] = 'Немає обраних товарів';
$_['text_remove_wishlist'] = 'Прибрати з обранних';
$_['text_before_remove'] = 'Ви впевнені, що хочете видалити цей товар?';

// Column
$_['column_image'] = 'Зображення';
$_['column_name'] = 'Назва товару';
$_['column_model'] = 'Модель';
$_['column_stock'] = 'Наявність';
$_['column_price'] = 'Ціна за одиницю товару';
$_['column_action'] = 'Дія';

$_['button_remove'] = 'Видалити';
$_['button_cancel'] = 'Відміна';