<?php
// Heading
$_['heading_title'] = 'Історія замовлень';

// Text
$_['text_account'] = 'Особистий Кабінет';
$_['text_order'] = 'Замовлення';
$_['text_order_detail'] = 'Деталі замовлення';
$_['text_invoice_no'] = '№ Рахунку';
$_['text_order_id'] = 'Номер замовлення';
$_['text_order_number'] = 'Номер замовлення';
$_['text_order_price'] = 'Сума замовлення';
$_['text_order_status'] = 'Статус замовлення';
$_['text_date_added'] = 'Дата замовлення';
$_['text_shipping_address'] = 'Адреса доставки';
$_['text_shipping_method'] = 'Спосіб доставки';
$_['text_payment_address'] = 'Платіжний адресу';
$_['text_payment_method'] = 'Спосіб оплати';
$_['text_comment'] = 'Коментар до замовлення';
$_['text_history'] = 'Історія замовлень';
$_['text_success'] = 'Товари з замовлення <a href="%s">%s</a> успішно додані <a href="%s">у ваш кошик</a>!';
$_['text_empty'] = 'Ви ще не здійснювали покупок!';
$_['text_no_products'] = 'Немає замовлень';
$_['text_error'] = 'Запитаний замовлення не знайдено!';
$_['text_table_list'] = 'Для коректного перегляду таблиці, проскрольте вправо';
$_['text_qnt']              = 'К-сть:';
$_['text_qnt_product']      = 'шт';
$_['text_service_lift']     = 'Наявність вантажного ліфта';
$_['text_lift_yes']         = 'Є';
$_['text_lift_no']          = 'Немає';

// Column
$_['column_order_id'] = '№ Замовлення';
$_['column_product'] = 'Кількість';
$_['column_customer'] = 'Клієнт';
$_['column_name'] = 'Назва товару';
$_['column_model'] = 'Модель';
$_['column_quantity'] = 'Кількість';
$_['column_price'] = 'Ціна';
$_['column_total'] = 'Всього';
$_['column_action'] = 'Дія';
$_['column_date_added'] = 'Додано';
$_['column_status'] = 'Статус';
$_['column_comment'] = 'Коментар до замовлення';

// Error
$_['error_reorder'] = '%s в даний момент не доступний....';