<?php
// Heading
$_['heading_title'] = 'Особистий Кабінет';

// Text
$_['text_account'] = 'Особистий Кабінет';
$_['text_my_account'] = 'Мій обліковий запис';
$_['text_my_orders'] = 'Мої замовлення';
$_['text_my_newsletter'] = 'Передплата';
$_['text_edit'] = 'Змінити контактну інформацію';
$_['text_password'] = 'Змінити пароль';
$_['text_address'] = 'Змінити мої адреси';
$_['text_credit_card'] = 'Керування кредитними картами';
$_['text_wishlist'] = 'Показати закладки';
$_['text_order'] = 'Історія замовлень';
$_['text_download'] = 'Файли для скачування';
$_['text_reward'] = 'Бонусні бали';
$_['text_return'] = 'Запити на повернення';
$_['text_transaction'] = 'Історія транзакцій';
$_['text_newsletter'] = 'Підписатися або відмовитися від розсилки новин';
$_['text_recurring'] = 'Періодичні платежі';
$_['text_transactions'] = 'Транзакції';
$_['text_not_send_email'] = 'Не отримувати розсилки';
$_['text_save'] = 'Зберегти';
$_['text_no_special'] = 'На не акційні товари.';
$_['text_to_catalog'] = 'В каталог';
$_['text_discount'] = 'Персональна знижка';

$_['entry_firstname'] = 'Ім\'я:';
$_['entry_lastname'] = 'Прізвище:';
$_['entry_telephone'] = 'Телефон:';
$_['entry_email'] = 'Email:';
$_['entry_shipping'] = 'Адреса доставки:';
$_['entry_birthday'] = 'Дата народження:';