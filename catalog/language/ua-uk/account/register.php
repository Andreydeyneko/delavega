<?php
// Heading
$_['heading_title'] = 'Реєстрація';

// Text
$_['text_account'] = 'Особистий Кабінет';
$_['text_register'] = 'Реєстрація';
$_['text_account_already'] = 'Якщо Ви вже зареєстровані, перейдіть на сторінку <a href="%s">авторизації</a>.';
$_['text_your_details'] = 'Основні дані';
$_['text_your_address'] = 'адреса';
$_['text_newsletter'] = 'Розсилка новин';
$_['text_your_password'] = 'пароль';
$_['text_agree'] = 'Я прочитав <a href="%s" class="agree"><b>%s</b></a> і згоден з умовами';
$_['text_register_note']   = 'Введіть email і номер телефону, на який буде висланий пароль.';
$_['text_register_password'] = 'На введений вами номер телефону був відправлений пароль.';
$_['text_change_phone'] = 'Змінити номер телефону';
$_['text_resend'] = 'Відправити ще раз';
$_['text_not_send'] = 'Не прийшов пароль?';
$_['text_resend_popup'] = 'Пароль повторно відправлений';

// Entry
$_['entry_customer_group'] = 'Напрям бізнесу';
$_['entry_firstname'] = 'Ім&#39;я';
$_['entry_lastname'] = 'Прізвище';
$_['entry_email'] = 'Email';
$_['entry_telephone'] = 'Телефон';
$_['entry_fax'] = 'Факс';
$_['entry_company'] = 'Компанія';
$_['entry_address_1'] = 'Адреса 1';
$_['entry_address_2'] = 'Адреса 2';
$_['entry_postcode'] = 'Індекс';
$_['entry_city'] = 'Місто';
$_['entry_country'] = 'Країна';
$_['entry_zone'] = 'Регіон / Область';
$_['entry_newsletter'] = 'Підписка на новини';
$_['entry_password'] = 'Пароль із СМС';
$_['entry_confirm'] = 'Підтвердження пароля';

// Error
$_['error_exists'] = 'Цей E-Mail вже зареєстрований!';
$_['error_firstname'] = 'Ім&#39;я повинно містити від 1 до 32 символів!';
$_['error_lastname'] = 'Прізвище повинна бути від 1 до 32 символів!';
$_['error_email'] = 'E-Mail введено неправильно!';
$_['error_telephone'] = 'Телефон повинен містити від 3 до 32 цифр!';
$_['error_address_1'] = 'Адреса має бути від 3 до 128 символів!';
$_['error_city'] = 'Назва міста має бути від 2 до 128 символів!';
$_['error_postcode'] = 'Індекс повинен бути від 2 до 10 символів!';
$_['error_country'] = 'Оберіть країну!';
$_['error_zone'] = 'Оберіть регіон / область!';
$_['error_custom_field'] = '%s обов&#39;язково до заповнення!';
$_['error_password'] = 'Невірний пароль!';
$_['error_confirm'] = 'Паролі і пароль підтвердження не збігаються!';
$_['error_agree'] = 'Ви повинні прочитати і погодитися з %s!';