<?php
// Text
$_['text_home'] = 'Головна';
//$_['text_wishlist'] = '%s';
$_['text_wishlist_custom'] = 'Список бажань';
$_['text_shopping_cart'] = 'Кошик';
$_['text_category'] = 'Каталог';
$_['text_contant'] = 'Контакти';
$_['text_company'] = 'Компанія';
$_['text_account'] = 'Особистий кабінет';
$_['text_register'] = 'Реєстрація';
$_['text_login'] = 'Авторизація';
$_['text_order'] = 'Історія замовлень';
$_['text_transaction'] = 'Транзакції';
$_['text_download'] = 'Завантаження';
$_['text_logout'] = 'Вихід';
$_['text_checkout'] = 'Оформлення замовлення';
$_['text_search'] = 'Пошук';
$_['text_all'] = 'Дивитися всі';
$_['text_download_catalog'] = 'Каталоги PDF';
$_['text_download_link'] = '/catalog-ua';
$_['text_choice_lang'] = 'Обрати мову';
$_['text_choice_currency'] = 'Обрати валюту';