<?php
// Text
$_['text_items'] = '%s товар(ів) - %s';
$_['text_empty'] = 'Ваш кошик порожній!';
$_['text_cart'] = 'Перейти до кошику';
$_['text_checkout'] = 'Оформити замовлення';
$_['text_recurring'] = 'Платіжний профіль';
$_['text_cart_quantity'] = 'К-сть:';
$_['text_cart_sht'] = 'шт';
$_['text_cart_price'] = 'Ціна:';
$_['text_continue_shopping'] = 'Продовжити покупки';
$_['text_cart_title'] = 'Товар доданий в кошик';