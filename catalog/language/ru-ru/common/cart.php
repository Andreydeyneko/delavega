<?php
// Text
$_['text_items']     = 'Товаров %s (%s)';
$_['text_empty']     = 'Ваша корзина пуста!';
$_['text_cart']      = 'Перейти в корзину';
$_['text_checkout']  = 'Оформить заказ';
$_['text_checkout']  = 'Оформить заказ';
$_['text_recurring'] = 'Платежный профиль';
$_['text_cart_quantity'] = 'Кол-во:';
$_['text_cart_sht'] = 'шт';
$_['text_cart_price'] = 'Цена:';
$_['text_continue_shopping'] = 'Продолжить покупки';
$_['text_cart_title'] = 'Товар добавлен в корзину';