<?php
// Text
$_['text_home']          = 'Главная';
//$_['text_wishlist']      = '%s';
$_['text_wishlist_custom']      = 'Список желаний';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Каталог';
$_['text_contant']       = 'Контакты';
$_['text_company']       = 'Компания';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';
$_['text_download_catalog'] = 'Каталоги PDF';
$_['text_download_link'] = '/ru/catalog-ru';
$_['text_choice_lang'] = 'Выбрать язык';
$_['text_choice_currency'] = 'Выбрать валюту';