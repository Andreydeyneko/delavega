<?php
// Text
$_['text_search']              = 'Поиск';
$_['text_brand']               = 'Производитель';
$_['text_size']                = 'Размеры PDF';
$_['text_manufacturer']        = 'Производитель:';
$_['text_model']               = 'Модель:';
$_['text_reward']              = 'Бонусные баллы:';
$_['text_points']              = 'Цена в бонусных баллах:';
$_['text_stock']               = 'Наличие:';
$_['text_instock']             = 'Есть в наличии';
$_['text_tax']                 = 'Без НДС:';
$_['text_discount']            = ' или более: ';
$_['text_option']              = 'Доступные варианты';
$_['text_minimum']             = 'Минимальное количество для заказа: %s';
$_['text_reviews']             = '%s отзывов';
$_['text_write']               = 'Написать отзыв';
$_['text_login']               = 'Пожалуйста <a href="%s">авторизируйтесь</a> или <a href="%s">создайте учетную запись</a> перед тем как написать отзыв';
$_['text_no_reviews']          = 'Отзывов о данном товаре пока нет.';
$_['text_note']                = '<span style="color: #FF0000;">Примечание:</span> HTML разметка не поддерживается! Используйте обычный текст.';
$_['text_success']             = 'Спасибо за ваш отзыв. Он поступил администратору для проверки на спам и вскоре будет опубликован.';
$_['text_related']             = 'Рекомендуемые товары';
$_['text_tags']                = '<i class="fa fa-tags"></i>';
$_['text_error']               = 'Товар не найден!';
$_['text_payment_recurring']   = 'Платежный профиль';
$_['text_trial_description']   = 'Сумма: %s; Периодичность: %d %s; Кол-во платежей: %d, Далее ';
$_['text_payment_description'] = 'Сумма: %s; Периодичность:  %d %s; Кол-во платежей:  %d ';
$_['text_payment_cancel']      = 'Сумма: %s; Периодичность:  %d %s ; Кол-во платежей: до отмены';
$_['text_day']                 = 'день';
$_['text_week']                = 'недели';
$_['text_semi_month']          = 'полмесяца';
$_['text_month']               = 'месяц';
$_['text_year']                = 'год';
$_['text_individual_size']     = 'Индивидуальный размер';
$_['text_final_price']         = 'Итого:';
$_['text_also_buy']            = 'С этим товаром покупают';
$_['text_description_product'] = 'Описание изделия';
$_['text_review_title']        = 'Оставьте отзыв';
$_['text_review_send']         = 'Отправить отзыв';
$_['text_own_size_title']      = 'Индивидуальный заказ';
$_['text_own_size_text']       = 'Спасибо! Мы свяжемся с вами для уточнения деталей заказа.';
$_['text_own_size_send']       = 'Отправить';
$_['text_video']               = 'Видео';
$_['text_3d_model']            = '3D модель';
$_['text_3d_form_title']       = 'Запросить 3D для дизайнеров';
$_['text_mechanism_tip']       = 'При выборе этого пункта Вы добавляете к товару специальный<strong>подъемный механизм</strong>';
$_['text_download_size']       = 'Скачать размер';
$_['text_3d_overview']         = 'Смотреть 3d обзор';
$_['text_video_overview']      = 'Видео механизма';
$_['text_review_thanks']       = 'Спасибо за отзыв';
$_['text_review_after']        = 'Он появится на сайте после проверки модератором.';
$_['text_about_size']          = 'Узнать больше';
$_['text_about_discounts']     = 'О скидках';
$_['text_select_color']        = 'Цвет';
$_['text_select_color_type']   = '-- Выберите цвет ткани --';
$_['text_vid'] = 'от ';

$_['text_location']  = 'Наши шоу-румы';
$_['text_store']     = 'Шоу рум в Киеве';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Отдел контроля качества';
$_['text_block_sale'] = 'Отдел продаж';
$_['text_block_marketing'] = 'Отдел маркетинга и рекламы';
$_['text_where_buy'] = 'Где купить';
$_['text_socials']   = 'Мы в социальных сетях';
$_['price_annot']   = '*стоимость в стандартном размере и сроке до 3 месяцев';
$_['price_discount']   = '*стоимость с 30% скидкой, стандартный размер, изготовление 3 мес.';
$_['add_wishlist'] = 'Добавить в список желаний';

$_['button_download3d']       = 'Запросить файл';

// Entry
$_['entry_qty']                = 'Количество';
$_['entry_name']               = 'Имя';
$_['entry_lastname']           = 'Фамилия';
$_['entry_company']            = 'Компания';
$_['entry_city']               = 'Город';
$_['entry_site']               = 'Сайт';
$_['entry_email']              = 'Email';
$_['entry_review']             = 'Ваш отзыв';
$_['entry_phone']              = 'Телефон';
$_['entry_rating']             = 'Оценка:';
$_['entry_good']               = 'Хорошо';
$_['entry_bad']                = 'Плохо';

// Tabs
$_['tab_description']          = 'Описание';
$_['tab_attribute']            = 'Характеристики';
$_['tab_review']               = 'Отзывы о товаре';

// Error
$_['error_name']               = 'Имя должно быть от 3 до 25 символов!';
$_['error_text']               = 'Текст отзыва должен быть от 25 до 1000 символов!';
$_['error_rating']             = 'Пожалуйста, выберите оценку!';

