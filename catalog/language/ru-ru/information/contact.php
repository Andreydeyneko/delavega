<?php
// Heading
$_['heading_title']  = 'Контакты';

// Text
$_['text_location']  = 'Наши шоу-румы';
$_['text_store']     = 'Шоу рум в Киеве';
$_['text_contact']   = 'Форма обратной связи';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Отдел контроля качества';
$_['text_comment']   = 'Дополнительная информация';
$_['text_success']   = '<p>Ваше сообщение успешно отправлено владельцу магазина!</p>';
$_['text_agree']     = 'Я прочитал <a href="%s" class="agree"><b>%s</b></a> и согласен с <br /> условиями безопасности и обработки персональных данных <input name="personal" checked required type="checkbox">  ';
$_['text_block_sale'] = 'Отдел продаж';
$_['text_block_design'] = 'Отдел по работе с дизайнерами и дилерами';
$_['text_block_suppliers'] = 'Отдел по работе с поставщиками материалов и комплектующиx';
$_['text_block_marketing'] = 'Отдел маркетинга и рекламы';
$_['text_socials']   = 'Мы в социальных сетях';
$_['button_submit']  = 'Отправить сообщение';
$_['email_owner']  = 'Написать письмо владельцу компании';
$_['feedback_button']  = 'Форма обратной связи';

// Entry
$_['entry_name']     = 'Ваше имя';
$_['entry_email']    = 'Ваш E-Mail';
$_['entry_enquiry']  = 'Ваш вопрос или сообщение';

// Email
$_['email_subject']  = 'Сообщение от %s';

$_['title_form'] = 'Написать нам';
$_['form_name'] = 'Ваше имя';
$_['form_number'] = 'Номер телефона';
$_['form_question'] = 'Оставить вопрос';
$_['form_send'] = 'Отправить';

// Errors
$_['error_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_email']    = 'E-mail адрес введен неверно!';
$_['error_enquiry']  = 'Длина текста должна быть от 10 до 3000 символов!';

