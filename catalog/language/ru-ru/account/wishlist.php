<?php
// Heading
$_['heading_title'] = 'Мои закладки';

// Text
$_['text_account']  = 'Личный Кабинет';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = 'Закладки (%s)';
$_['text_login']    = 'Вы должны <a href="%s">выполнить вход</a> или <a href="%s">создать аккаунт</a> чтобы сохранить <a href="%s">%s</a> в свой <a href="%s">список закладок</a>!';
$_['text_success']  = 'Вы добавили <a href="%s">%s</a> в <a href="%s">Закладки</a>!';
$_['text_remove']   = 'Список закладок успешно изменен!';
$_['text_empty']    = 'Ваши закладки пусты';
$_['text_empty_wishlist'] = 'Нет избранных товаров';
$_['text_remove_wishlist'] = 'Убрать из избранного';
$_['text_before_remove'] = 'Вы уверены, что хотите удалить этот товар?';

// Column
$_['column_image']  = 'Изображение';
$_['column_name']   = 'Название товара';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'Наличие';
$_['column_price']  = 'Цена за единицу товара';
$_['column_action'] = 'Действие';

$_['button_remove'] = 'Удалить';
$_['button_cancel'] = 'Отмена';