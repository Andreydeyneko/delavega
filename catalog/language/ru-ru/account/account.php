<?php
// Heading
$_['heading_title']       = 'Личный Кабинет';

// Text
$_['text_account']        = 'Личный Кабинет';
$_['text_my_account']     = 'Моя учетная запись';
$_['text_my_orders']      = 'Мои заказы';
$_['text_my_affiliate']   = 'Мой партнерский аккаунт';
$_['text_my_newsletter']  = 'Подписка';
$_['text_edit']           = 'Изменить контактную информацию';
$_['text_password']       = 'Изменить свой пароль';
$_['text_address']        = 'Изменить мои адреса';
$_['text_credit_card']    = 'Управление кредитными картами';
$_['text_wishlist']       = 'Посмотреть закладки';
$_['text_order']          = 'История заказов';
$_['text_download']       = 'Файлы для скачивания';
$_['text_reward']         = 'Бонусные баллы';
$_['text_return']         = 'Запросы на возврат';
$_['text_transaction']    = 'История транзакций';
$_['text_newsletter']     = 'Подписаться или отказаться от рассылки новостей';
$_['text_recurring']      = 'Периодические платежи';
$_['text_transactions']   = 'Транзакции';
$_['text_affiliate_add']  = 'Регистрация партнерского аккаунта';
$_['text_affiliate_edit'] = 'Изменить партнерскую информацию';
$_['text_tracking']       = 'Код отслеживания партнерских отношений';
$_['text_not_send_email'] = 'Не получать рассылки';
$_['text_save']           = 'Сохранить';
$_['text_no_special']     = 'На не акционные товары.';
$_['text_to_catalog']     = 'В каталог';
$_['text_discount']       = 'Персональная скидка';

$_['entry_firstname'] = 'Имя:';
$_['entry_lastname'] = 'Фамилия:';
$_['entry_telephone'] = 'Телефон:';
$_['entry_email'] = 'Email:';
$_['entry_shipping'] = 'Адрес доставки:';
$_['entry_birthday'] = 'Дата рождения:';