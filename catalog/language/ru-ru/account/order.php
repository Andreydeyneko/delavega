<?php
// Heading
$_['heading_title']         = 'История заказов';

// Text
$_['text_account']          = 'Личный Кабинет';
$_['text_order']            = 'Заказ';
$_['text_order_detail']     = 'Детали заказа';
$_['text_invoice_no']       = '№ Счета';
$_['text_order_id']         = 'Номер заказа';
$_['text_order_number']     = 'Номер заказа';
$_['text_date_added']       = 'Дата заказа';
$_['text_order_price']      = 'Сумма заказа';
$_['text_order_status']     = 'Статус заказа';
$_['text_shipping_address'] = 'Адрес доставки';
$_['text_shipping_method']  = 'Способ доставки';
$_['text_payment_address']  = 'Платёжный адрес';
$_['text_payment_method']   = 'Способ оплаты';
$_['text_comment']          = 'Комментарий к заказу';
$_['text_history']          = 'История заказов';
$_['text_success']          = 'Товары из заказа <a href="%s">%s</a> успешно добавлены <a href="%s">в вашу корзину</a>!';
$_['text_empty']            = 'Вы еще не совершали покупок!';
$_['text_no_products']      = 'Нет заказов';
$_['text_error']            = 'Запрошенный заказ не найден!';
$_['text_table_list']       = 'Для корректного просмотра таблицы, проскрольте вправо';
$_['text_qnt']              = 'Кол-во:';
$_['text_qnt_product']      = 'шт';
$_['text_service_lift']     = 'Наличие грузового лифта';
$_['text_lift_yes']         = 'Есть';
$_['text_lift_no']          = 'Нет';

// Column
$_['column_order_id']       = '№ Заказа';
$_['column_customer']       = 'Клиент';
$_['column_product']        = 'Количество';
$_['column_name']           = 'Название товара';
$_['column_model']          = 'Модель';
$_['column_quantity']       = 'Количество';
$_['column_price']          = 'Сумма заказа';
$_['column_total']          = 'Всего';
$_['column_action']         = 'Действие';
$_['column_date_added']     = 'Добавлено';
$_['column_status']         = 'Статус';
$_['column_comment']        = 'Комментарий к заказу';

// Error
$_['error_reorder']         = '%s в данный момент не доступен....';

