<?php
// Heading
$_['heading_title']        = 'Регистрация';

// Text
$_['text_account']         = 'Личный Кабинет';
$_['text_register']        = 'Регистрация';
$_['text_account_already'] = 'Если Вы уже зарегистрированы, перейдите на страницу <a href="%s">авторизации</a>.';
$_['text_your_details']    = 'Основные данные';
$_['text_newsletter']      = 'Рассылка новостей';
$_['text_your_password']   = 'Ваш пароль';
$_['text_agree']           = 'Я прочитал <a href="%s" class="agree"><b>%s</b></a> и согласен с <br /> условиями безопасности и обработки персональных данных';
$_['text_register_note']   = 'Введите email и номер телефона, на который будет выслан пароль.';
$_['text_register_password'] = 'На введенный вами номер телефона был отправлен пароль.';
$_['text_change_phone'] = 'Изменить номер телефона';
$_['text_resend'] = 'Отправить еще раз';
$_['text_not_send'] = 'Не пришел пароль?';
$_['text_resend_popup'] = 'Пароль повторно отправлен';

// Entry
$_['entry_customer_group'] = 'Направление бизнеса';
$_['entry_firstname']      = 'Имя';
$_['entry_lastname']       = 'Фамилия';
$_['entry_email']          = 'Email';
$_['entry_telephone']      = 'Телефон';
$_['entry_newsletter']     = 'Подписка на новости';
$_['entry_password']       = 'Пароль из СМС';
$_['entry_confirm']        = 'Подтверждение пароля';

// Error
$_['error_exists']         = 'Данный E-Mail уже зарегистрирован!';
$_['error_firstname']      = 'Имя должно быть от 1 до 32 символов!';
$_['error_lastname']       = 'Фамилия должна быть от 1 до 32 символов!';
$_['error_email']          = 'E-Mail введен неправильно!';
$_['error_telephone']      = 'Телефон должен быть от 3 до 32 цифр!';
$_['error_custom_field']   = '%s обязательно к заполнению!';
$_['error_password']       = 'Неверный пароль!';
$_['error_confirm']        = 'Пароли и пароль подтверждения не совпадают!';
$_['error_agree']          = 'Вы должны прочитать и согласится с %s!';

