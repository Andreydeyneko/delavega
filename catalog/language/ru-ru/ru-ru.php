<?php
// Locale
$_['code']                  = 'ru';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l, d F Y';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ' ';
$_['button_search_404'] = 'Поиск по сайту';

// Text
$_['text_home']             = 'Главная';
$_['text_yes']              = 'Да';
$_['text_no']               = 'Нет';
$_['text_none']             = ' --- Не выбрано --- ';
$_['text_select']           = ' --- Выберите --- ';
$_['text_all_zones']        = 'Все зоны';
$_['text_pagination']       = 'Показано с %d по %d из %d (всего %d страниц)';
$_['text_loading']          = 'Загрузка...';
$_['text_no_results']       = 'Нет данных!';
$_['text_more']             = 'Подробнее';
$_['text_close']            = 'закрыть';
$_['text_load_more']        = 'Загрузить еще';
$_['text_vid'] = 'от ';
$_['discount__button_send'] = 'ЗАПРОСИТЬ ПРОМО';


// Buttons
$_['button_address_add']    = 'Добавить адрес';
$_['button_back']           = 'Назад';
$_['button_continue']       = 'Продолжить';
$_['button_home']           = 'На главную';
$_['button_cart']           = 'Добавить в корзину';
$_['button_cancel']         = 'Отмена';
$_['button_compare']        = 'В сравнение';
$_['button_wishlist']       = 'В закладки';
$_['button_checkout']       = 'Оформление заказа';
$_['button_confirm']        = 'Подтверждение заказа';
$_['button_coupon']         = 'Применение купона';
$_['button_delete']         = 'Удалить';
$_['button_download']       = 'Скачать';
$_['button_edit']           = 'Редактировать';
$_['button_filter']         = 'Поиск';
$_['button_new_address']    = 'Новый адрес';
$_['button_change_address'] = 'Изменить адрес';
$_['button_reviews']        = 'Отзывы';
$_['button_write']          = 'Написать отзыв';
$_['button_login']          = 'Войти';
$_['button_update']         = 'Обновить';
$_['button_remove']         = 'Удалить';
$_['button_reorder']        = 'Дополнительный заказ';
$_['button_return']         = 'Вернуть';
$_['button_shopping']       = 'Продолжить покупки';
$_['button_search']         = 'Поиск';
$_['button_shipping']       = 'Применить Доставку';
$_['button_submit']         = 'Применить';
$_['button_guest']          = 'Оформление заказа без регистрации';
$_['button_view']           = 'Просмотр';
$_['button_voucher']        = 'Применить подарочный сертификат';
$_['button_upload']         = 'Загрузить файл';
$_['button_reward']         = 'Применить бонусные баллы';
$_['button_quote']          = 'Узнать цены';
$_['button_list']           = 'Список';
$_['button_grid']           = 'Сетка';
$_['button_map']            = 'Посмотреть карту';
$_['button_register']       = 'Зарегистрироваться';
$_['button_login']          = 'Войти';
$_['button_confirm_order'] = 'Купить';

// Contact form
$_['text_name'] = 'Имя';
$_['text_phone'] = 'Телефон';
$_['text_email'] = 'Email';
$_['text_send'] = 'Отправить';
$_['text_viber'] = 'Связаться через Вайбер';
$_['text_telegram'] = 'Связаться через Telegram';
$_['text_instagram'] = 'Мы в Instagram';
$_['text_pinterest'] = 'Мы в Pinterest';
$_['text_facebook'] = 'Мы в Facebook';
$_['text_call'] = 'Позвонить нам';

// Popup
$_['text_thanks_title'] = 'Спасибо!';
$_['text_thanks_contact'] = 'В ближайшее время с Вами свяжется наш менеджер для уточнения деталей заказа.';
$_['text_thanks_individ'] = 'После проверки Ваших данных файл будет выслан Вам на почту.';
$_['text_thanks_promo'] = 'С вами свяжется менеджер для уточнения деталей.';

// Error
$_['error_exception']       = 'Ошибка кода(%s): %s в %s на строке %s';
$_['error_upload_1']        = 'Предупреждение: Размер загружаемого файла превышает значение upload_max_filesize в php.ini!';
$_['error_upload_2']        = 'Предупреждение: Загруженный файл превышает MAX_FILE_SIZE значение, которая была указана в настройках!';
$_['error_upload_3']        = 'Предупреждение: Загруженные файлы были загружены лишь частично!';
$_['error_upload_4']        = 'Предупреждение: Нет файлов для загрузки!';
$_['error_upload_6']        = 'Предупреждение: Временная папка!';
$_['error_upload_7']        = 'Предупреждение: Ошибка записи!';
$_['error_upload_8']        = 'Предупреждение: Запрещено загружать файл с данным расширением!';
$_['error_upload_999']      = 'Предупреждение: Неизвестная ошибка!';
$_['error_curl']            = 'CURL: Ошибка кода(%s): %s';
$_['error_name'] = 'Введите имя!';
$_['error_phone'] = 'Телефон введен неправильно!';
$_['error_promo'] = 'Промокод уже у Вас есть!';

/* Когда нужен перевод скриптов, просто добавь код языка */

// Datepicker
$_['datepicker']                    = 'ru';

