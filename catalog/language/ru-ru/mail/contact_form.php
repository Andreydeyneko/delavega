<?php
// Text
$_['text_subject']    = 'Обратный звонок - Delavega';
$_['text_name']       = 'Имя: %s';
$_['text_email']      = 'Email: %s';
$_['text_phone']      = 'Телефон: %s';
$_['subject']    = 'Запрос персональной скидки';
$_['text_body']    = 'Покупатель хочет получить персональную скидку';
$_['text_code']    = 'Код %s на скидку %s';

$_['text_message'] = 'Вопрос';
$_['text_subject_feedback'] = 'Задать вопрос';