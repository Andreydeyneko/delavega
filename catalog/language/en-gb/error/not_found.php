<?php
// Heading
$_['heading_title'] = 'Error!';

// Text
$_['text_error']    = 'Error! The page you are looking for has been moved, deleted, renamed or never existed';

