<?php
// Text
$_['text_success']           = 'Order was updated';

// Error
$_['error_permission']       = 'Attention! Access denied to API!';
$_['error_customer']         = 'Customer data is required!';
$_['error_payment_address']  = 'Payer"s address is required!';
$_['error_payment_method']   = 'Payment method required!';
$_['error_no_payment']       = 'Attention: There are no available payment methods!';
$_['error_shipping_address'] = 'Delivery address required!';
$_['error_shipping_method']  = 'A delivery method is required!';
$_['error_no_shipping']      = 'Attention: There are no available delivery methods!';
$_['error_stock']            = 'Products marked with *** are not available in the required quantity or are not in stock!';
$_['error_minimum']          = 'Minimum order quantity %s is %s!';
$_['error_not_found']        = 'Warning! Order not found';

