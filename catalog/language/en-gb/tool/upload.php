<?php
// Text
$_['text_upload']    = 'File uploaded successfully!';

// Error
$_['error_filename'] = 'The file name must be from 3 to 64 characters!';
$_['error_filetype'] = 'Wrong file type!';
$_['error_upload']   = 'You must download the file!';

