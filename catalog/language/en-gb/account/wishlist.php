<?php
// Heading
$_['heading_title'] = 'My bookmarks';

// Text
$_['text_account']  = 'Personal Area';
$_['text_instock']  = 'In stock';
$_['text_wishlist'] = 'Bookmarks (%s)';
$_['text_login']    = 'You must <a href="%s"> sign in </a> or <a href="%s"> create an account </a> to save <a href="%s">% s </a> to your <a href="%s"> list of bookmarks </a>!';
$_['text_success']  = 'You have added <a href="%s">% s </a> to <a href="%s"> Bookmarks </a>!';
$_['text_remove']   = 'Bookmark list successfully changed!';
$_['text_empty']    = 'Your bookmarks are empty';
$_['text_empty_wishlist'] = 'No featured items';
$_['text_remove_wishlist'] = 'Remove from favorites';
$_['text_before_remove'] = 'Are you sure you want to remove this product?';

// Column
$_['column_image']  = 'Image';
$_['column_name']   = 'Product name';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Availability';
$_['column_price']  = 'Unit price';
$_['column_action'] = 'Action';

$_['button_remove'] = 'Delete';
$_['button_cancel'] = 'Cancel';