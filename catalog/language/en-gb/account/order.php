<?php
// Heading
$_['heading_title']         = 'Order History';

// Text
$_['text_account']          = 'Personal Area';
$_['text_order']            = 'Order';
$_['text_order_detail']     = 'Order details';
$_['text_invoice_no']       = 'Bill number';
$_['text_order_id']         = 'Order number';
$_['text_order_number']     = 'Order number';
$_['text_date_added']       = 'Order date';
$_['text_order_price']      = 'Order amount';
$_['text_order_status']     = 'Order status';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping method';
$_['text_payment_address']  = 'Billing address';
$_['text_payment_method']   = 'Payment method';
$_['text_comment']          = 'Order comment';
$_['text_history']          = 'Order history';
$_['text_success']          = 'Products from order <a href="%s">% s </a> have been successfully added <a href="%s"> to your shopping cart </a>!';
$_['text_empty']            = 'You have not made purchases!';
$_['text_no_products']      = 'No orders';
$_['text_error']            = 'The requested order could not be found!';
$_['text_table_list']       = 'To view the table correctly, skip to the right';
$_['text_qnt']              = 'Amount:';
$_['text_qnt_product']      = 'pcs';
$_['text_service_lift']     = 'The presence of a freight elevator';
$_['text_lift_yes']         = 'Yes';
$_['text_lift_no']          = 'No';

// Column
$_['column_order_id']       = 'Order No.';
$_['column_customer']       = 'Customer';
$_['column_product']        = 'Amount';
$_['column_name']           = 'Product Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Amount';
$_['column_price']          = 'Order amount';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Addedded';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Order comment';

// Error
$_['error_reorder']         = '%s is currently not available ....';

