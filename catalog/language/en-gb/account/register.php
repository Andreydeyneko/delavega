<?php
// Heading
$_['heading_title']        = 'Registration';

// Text
$_['text_account']         = 'Personal Account';
$_['text_register']        = 'Registration';
$_['text_account_already'] = 'If you are already registered, go to the <a href="%s"> login page </a>.';
$_['text_your_details']    = 'Master data';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your password';
$_['text_agree']           = 'I read <a href="%ss class="agree"> <b>% s </ b> </a> and I agree with <br /> terms of security and personal handling data';
$_['text_register_note']   = 'Enter the email and phone number to which the password will be sent.';
$_['text_register_password'] = 'A password was sent to the phone number you entered.';
$_['text_change_phone'] = 'Change phone number';
$_['text_resend'] = 'Send again';
$_['text_not_send'] = 'The password did not come?';
$_['text_resend_popup'] = 'Password re-sent';

// Entry
$_['entry_customer_group'] = 'Business direction';
$_['entry_firstname']      = 'Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'Email';
$_['entry_telephone']      = 'Phone';
$_['entry_newsletter']     = 'Subscribe to newsletter';
$_['entry_password']       = 'Password from SMS';
$_['entry_confirm']        = 'Verify Password';

// Error
$_['error_exists']         = 'This E-Mail-mail is already registered!';
$_['error_firstname']      = 'This e-mail is already registered!';
$_['error_lastname']       = 'Last name must be from 1 to 32 characters!';
$_['error_email']          = 'E-Mail is entered incorrectly!';
$_['error_telephone']      = 'The phone must be from 3 to 32 digits!';
$_['error_custom_field']   = '%s must be filled!';
$_['error_password']       = 'Invalid password!';
$_['error_confirm']        = 'Passwords and confirmation password do not match!';
$_['error_agree']          = 'You must read and agree with %s!';

