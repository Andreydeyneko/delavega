<?php
// Heading
$_['heading_title']       = 'Personal Area';

// Text
$_['text_account']        = 'Personal Area';
$_['text_my_account']     = 'My account';
$_['text_my_orders']      = 'My orders';
$_['text_my_affiliate']   = 'My affiliate account';
$_['text_my_newsletter']  = 'Subscription';
$_['text_edit']           = 'Change contact information';
$_['text_password']       = 'Change your password';
$_['text_address']        = 'Change my addresses';
$_['text_credit_card']    = 'Credit card management';
$_['text_wishlist']       = 'View Bookmarks';
$_['text_order']          = 'Purchase history';
$_['text_download']       = 'Files to download';
$_['text_reward']         = 'Bonus points';
$_['text_return']         = 'Return Requests';
$_['text_transaction']    = 'Transaction history';
$_['text_newsletter']     = 'Subscribe or unsubscribe from newsletter';
$_['text_recurring']      = 'Recurring payments';
$_['text_transactions']   = 'Transactions';
$_['text_affiliate_add']  = 'Partner account registration';
$_['text_affiliate_edit'] = 'Change affiliate information';
$_['text_tracking']       = 'Partner tracking code';
$_['text_not_send_email'] = 'Do not receive mailings';
$_['text_save']           = 'Save';
$_['text_no_special']     = 'For non-promotional items.';
$_['text_to_catalog']     = 'To catalog';
$_['text_discount']       = 'Personal discount';

$_['entry_firstname'] = 'Name:';
$_['entry_lastname'] = 'Surname:';
$_['entry_telephone'] = 'Phone:';
$_['entry_email'] = 'Email:';
$_['entry_shipping'] = 'Delivery address:';
$_['entry_birthday'] = 'Date of Birth:';