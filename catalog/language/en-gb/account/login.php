<?php
// Heading
$_['heading_title']                = 'Authorization';

// Text
$_['text_account']                 = 'Personal Area';
$_['text_login']                   = 'Authorization';
$_['text_new_customer']            = 'New client';
$_['text_register']                = 'Registration';
$_['text_register_account']        = 'Creating an account will help buy faster. You can monitor the status of the order, as well as view orders made earlier. You can accumulate reward points and get discount coupons. <BR> And for regular customers we offer a flexible system of discounts and personal service. <BR>';
$_['text_returning_customer']      = 'Registered customer';
$_['text_i_am_returning_customer'] = 'Login to My Account';
$_['text_forgotten']               = 'Forgot your password?';
$_['text_go_account']              = 'Sign in';
$_['text_enter_phone']             = 'Enter the phone number <br> to which the password will be sent.';
$_['text_enter_password']          = 'Password was sent to your phone.<br>Enter it in the field below.';
$_['text_register_password']       = 'A password has been sent to the phone number you entered.';
$_['text_change_phone']            = 'Change phone number';
$_['text_resend']                  = 'Send again';
$_['text_not_send']                = 'Did not come the password?';


// Entry
$_['entry_email']                  = 'E-Mail';
$_['entry_phone']                  = 'Phone';
$_['entry_password']               = 'Password';

$_['button_login_account']         = 'Authorization';

// Error
$_['error_login']                  = 'Incorrectly filled in the E-Mail and / or password!';
$_['error_attempts']               = 'You have exceeded the maximum number of login attempts. Retry authorization on the site after 1 hour';
$_['error_approved']               = 'You need to confirm your account before authorization.';
$_['error_phone_db']               = 'Enter the phone number of the registered user, to which the password will be sent';

