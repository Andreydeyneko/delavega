<?php
// Heading
$_['heading_title']        = 'Calculate Shipping Cost';

// Text
$_['text_success']         = 'The calculation was completed successfully!';
$_['text_shipping']        = 'Specify your region for calculating shipping costs.';
$_['text_shipping_method'] = 'Please select your preferred shipping method for the current order.';

// Entry
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Region / State';
$_['entry_postcode']       = 'Index';

// Error
$_['error_postcode']       = 'The index must be from 2 to 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / region!';
$_['error_shipping']       = 'You must specify the delivery method!';
$_['error_no_shipping']    = 'Delivery to this address is not possible. Please <a href="%s"> contact the store management </a>!';
