<?php
// Heading
$_['heading_title'] = 'Use Bonus Points (% s points available)';

// Text
$_['text_reward']   = 'Bonus points (%s)';
$_['text_order_id'] = 'Order number: %s';
$_['text_success']  = 'Points successfully applied!';

// Entry
$_['entry_reward']  = 'Available for use (maximum %s)';

// Error
$_['error_reward']  = 'Please enter the number of bonus points to pay for this order!';
$_['error_points']  = 'You do not have %s bonus points!';
$_['error_maximum'] = 'The maximum available points that can be applied: %s!';

