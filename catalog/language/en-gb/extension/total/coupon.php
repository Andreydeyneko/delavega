<?php
// Heading
$_['heading_title'] = 'Use coupon';

// Text
$_['text_coupon']   = 'Coupon (%s)';
$_['text_success']  = 'Discount coupon successfully applied!';

// Entry
$_['entry_coupon']  = 'Enter coupon code';

// Error
$_['error_coupon']  = 'Error. Invalid discount coupon code. It is possible that the validity period has expired or the usage limit has been reached!';
$_['error_empty']   = 'Attention! Enter coupon code';

