<?php
// Heading
$_['heading_title'] = 'Use Gift Certificate';

// Text
$_['text_voucher']  = 'Gift certificate (%s)';
$_['text_success']  = 'Your Gift Certificate has been successfully applied!';

// Entry
$_['entry_voucher'] = 'Enter the Gift Voucher Code';

// Error
$_['error_voucher'] = 'Error. Incorrect Gift Certificate code or this certificate has already been used!';
$_['error_empty']   = 'Attention: Enter the certificate code!';

