<?php
// Heading
$_['heading_title'] = 'Choose a store';

// Text
$_['text_default']  = 'Main';
$_['text_store']    = 'Please select the store you want to visit.';

