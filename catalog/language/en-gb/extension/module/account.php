<?php
// Heading
$_['heading_title']    = 'My Account';

// Text
$_['text_register']    = 'Registration';
$_['text_login']       = 'Log in';
$_['text_logout']      = 'Log out';
$_['text_forgotten']   = 'Forgot your password?';
$_['text_account']     = 'Profile';
$_['text_edit']        = 'Edit contact information';
$_['text_password']    = 'Password';
$_['text_address']     = 'Address Book';
$_['text_wishlist']    = 'Favorites';
$_['text_order']       = 'Purchase history';
$_['text_download']    = 'Files to download';
$_['text_reward']      = 'Bonus points';
$_['text_return']      = 'Returns';
$_['text_transaction'] = 'Transaction history';
$_['text_newsletter']  = 'E-Mail Newsletter';
$_['text_recurring']   = 'Recurring payments';

