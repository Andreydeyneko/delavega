<?php
// Text
$_['text_title']       = 'Payment to the bank account';
$_['button_confirm_order'] = 'Buy';
$_['text_instruction'] = 'Bank Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following bank account: details here';
$_['text_payment']     = 'The order will not be processed until the money reaches our bank account.';

