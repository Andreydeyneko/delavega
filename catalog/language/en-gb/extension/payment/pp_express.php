<?php
// Heading
$_['express_text_title']      = 'Order confirmation';

// Text
$_['text_title']              = 'PayPal Express payments (including Visa and MasterCard)';
$_['text_cart']               = 'Shopping cart';
$_['text_shipping_updated']   = 'Delivery updated';
$_['text_trial']              = 'Amount: %s; Periodicity: %s %s; Number of payments: %s, Next ';
$_['text_recurring']          = 'Amount: %s Periodicity: %s %s';
$_['text_recurring_item']     = 'Repeating payments';
$_['text_length']             = 'Number of payments: %s';

// Entry
$_['express_entry_coupon']    = 'Enter coupon code:';

// Button
$_['button_express_coupon']   = 'Add';
$_['button_express_confirm']  = 'Confirm';
$_['button_express_login']    = 'Sign in to PayPal';
$_['button_express_shipping'] = 'Update Delivery';

// Error
$_['error_heading_title']	  = 'An error occurred ...';
$_['error_too_many_failures'] = 'An error occurred during the payment process';
$_['error_unavailable'] 	  = 'Please use the full design for this order';
$_['error_no_shipping']    	  = 'Warning! No delivery options available. Please <a href="%s"> write to us </a> for consultation! ';
