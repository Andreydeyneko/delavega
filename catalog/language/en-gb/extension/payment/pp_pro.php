<?php
// Text
$_['text_title']				= 'Visa and MasterCard (PayPal) cards';
$_['text_wait']					= 'Please wait!';
$_['text_credit_card']			= 'Payment card information';

// Entry
$_['entry_cc_type']				= 'Card type:';
$_['entry_cc_number']			= 'Card number:';
$_['entry_cc_start_date']		= 'Card validity start date:';
$_['entry_cc_expire_date']		= 'Card expiration date:';
$_['entry_cc_cvv2']				= 'Security Code (CVV2):';
$_['entry_cc_issue']			= 'Card Code (Issue):';

// Help
$_['help_start_date']			= '(if known)';
$_['help_issue']				= '(only for Maestro and Solo cards)';

