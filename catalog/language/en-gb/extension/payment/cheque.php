<?php
// Text
$_['text_title']       = 'Payment in cash or using a terminal in the showroom (Kiev), at our dealers in other cities';
$_['button_confirm_order'] = 'Buy';
$_['text_instruction'] = 'Payment in cash or using a terminal in the showroom (Kiev), at our dealers in other cities';
$_['text_payable']     = 'Payee: ';
$_['text_address']     = 'Accepting payments at:';
$_['text_payment']     = 'Your order will not be processed until we receive payment.';

