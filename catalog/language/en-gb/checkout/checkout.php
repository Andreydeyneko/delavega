<?php
// Heading
$_['heading_title']                  = 'Checkout';

// Text
$_['text_cart']                      = 'Shopping cart';
$_['text_checkout_option']           = 'Step %s: Ordering method';
$_['text_checkout_account']          = 'Step %s: Profile &amp; Billing information';
$_['text_checkout_payment_address']  = 'Step %s: Billing information';
$_['text_checkout_shipping_address'] = 'Step %s: Delivery Address';
$_['text_checkout_shipping_method']  = 'Shipping method:';
$_['text_checkout_payment_method']   = 'Payment method:';
$_['text_payment_first_method']      = '-- Select a payment method --';
$_['text_shipping_first_method']     = '-- Choose a shipping method --';
$_['text_checkout_confirm']          = 'Step 6: Order confirmation';
$_['text_modify']                    = 'Edit &raquo;';
$_['text_new_customer']              = 'New customer';
$_['text_returning_customer']        = 'Registered user';
$_['text_checkout']                  = 'Ordering options';
$_['text_i_am_returning_customer']   = 'I made purchases here earlier and registered';
$_['text_register']                  = 'Registration';
$_['text_guest']                     = 'Place an order without registering';
$_['text_register_account']          = 'Creating an account will help make purchases faster and more convenient, as well as receive discounts as a regular customer.';
$_['text_forgotten']                 = 'Forgot your password?';
$_['text_your_details']              = 'Personal data';
$_['text_your_address']              = 'Address';
$_['text_your_password']             = 'Password';
$_['text_agree']                     = 'I read <a href="%s" class="agree"><b>%s</b></a> and I agree with <br /> terms of security and personal handling data';
$_['text_address_new']               = 'I want to use the new address';
$_['text_address_existing']          = 'I want to use an existing address';
$_['text_shipping_method']           = 'Choose a convenient shipping method for this order';
$_['text_payment_method']            = 'Select a payment method for this order';
$_['text_comments']                  = 'You can add a comment to your order';
$_['text_recurring_item']            = 'Periodicity element';
$_['text_payment_recurring']         = 'Billing profile';
$_['text_trial_description']         = 'Cost: %s; Periodicity: %d %s; Number of payments: %d;  Next,  ';
$_['text_payment_description']       = 'Cost: %s; Periodicity: %d %s; Number of payments: %d';
$_['text_payment_cancel']            = '%s every %d %s(й) until canceled';
$_['text_day']                       = 'day';
$_['text_week']                      = 'week';
$_['text_semi_month']                = 'fortnight';
$_['text_month']                     = 'month';
$_['text_year']                      = 'year';

// Column
$_['column_name']                    = 'Product name';
$_['column_model']                   = 'Model';
$_['column_quantity']                = 'Amount';
$_['column_price']                   = 'Price';
$_['column_total']                   = 'Total';

// Entry
$_['entry_email_address']            = 'E-Mail address';
$_['entry_email']                    = 'E-Mail';
$_['entry_password']                 = 'Password';
$_['entry_confirm']                  = 'Verify password';
$_['entry_firstname']                = 'Name';
$_['entry_lastname']                 = 'Last Name';
$_['entry_telephone']                = 'Phone';
$_['entry_address']                  = 'Select an address';
$_['entry_company']                  = 'Company';
$_['entry_customer_group']           = 'Business Type';
$_['entry_address_1']                = 'Address';
$_['entry_address_2']                = 'Address 2';
$_['entry_postcode']                 = 'Index';
$_['entry_city']                     = 'City';
$_['entry_country']                  = 'Country';
$_['entry_zone']                     = 'Region / State';
$_['entry_newsletter']               = 'I want to subscribe to News %s .';
$_['entry_shipping']                 = 'My shipping address is the same as your billing address.';

// Error
$_['error_warning']                  = 'There was a problem processing your order! If the problem occurs again, try another payment method or <a href="%s"> contact the store administrator </a>.';
$_['error_login']                    = 'Error: Incorrect E-Mail and / or password.';
$_['error_attempts']                 = 'You have exceeded the maximum number of login attempts. Retry authorization on the site after 1 hour';
$_['error_approved']                 = 'Before you can log in, the administration must approve your account.';
$_['error_exists']                   = 'Error: this E-Mail address is already registered!';
$_['error_firstname']                = 'The name must be from 1 to 32 characters!';
$_['error_lastname']                 = 'Last name must be from 1 to 32 characters!';
$_['error_email']                    = 'E-mail address is entered incorrectly!';
$_['error_telephone']                = 'The phone number must be from 3 to 32 characters!';
$_['error_password']                 = 'The password must be from 4 to 20 characters!';
$_['error_confirm']                  = 'Passwords do not match!';
$_['error_address_1']                = 'Address must be from 3 to 128 characters!';
$_['error_city']                     = 'The name of the city must be from 2 to 128 characters!';
$_['error_postcode']                 = 'The index must be from 2 to 10 characters!';
$_['error_country']                  = 'Please select a country!';
$_['error_zone']                     = 'Please select a region / region';
$_['error_agree']                    = 'You must read and agree with %s!';
$_['error_address']                  = 'You must specify the address!';
$_['error_shipping']                 = 'You must specify the delivery method!';
$_['error_no_shipping']              = 'There are no available delivery methods. Please <a href="%s"> contact the administration </a>!';
$_['error_payment']                  = 'You must specify a payment method!';
$_['error_no_payment']               = 'There are no available payment methods. Please <a href="%s"> contact the administration </a>!';
$_['error_custom_field']             = '%s is required!';

