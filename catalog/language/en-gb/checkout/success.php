<?php
// Heading
$_['heading_title']        = 'Your order has been successfully formed!';

// Text
$_['text_basket']          = 'Basket';
$_['text_checkout']        = 'Place order';
$_['text_success']         = 'Thank you!';
$_['text_customer']        = 'Your order <a href="%s" class="thanks__number">№%s</a> successfully generated and sent a notification to Email';
$_['text_guest']           = '<p>Your order is accepted!</p><p>If you have any questions, please <a href="%s"> contact us </a>.</p><p> Thank you for shopping in our online store! </p>';
$_['text_account_title']   = 'My Account';
$_['text_latest_articles'] = 'Recent publications';
$_['text_all_news']        = 'All news';
$_['text_we_socials']      = 'We are in social networks';

$_['button_to_home'] = 'Continue shopping';