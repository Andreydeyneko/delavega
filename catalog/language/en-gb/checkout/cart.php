<?php
// Heading
$_['heading_title']            = 'Shopping basket';

// Text
$_['text_success']             = '<a href="%s">%s</a> added <a href="%s">to your shopping cart</a>!';
$_['text_remove']              = 'Your shopping cart has been changed!';
$_['text_login']               = 'You need to <a href="%s">log in</a> or <a href="%s">create an account</a> to view prices!';
$_['text_items']               = 'Products %s  (%s)';
$_['text_points']              = 'Bonus points: %s';
$_['text_next']                = 'What would you like to do next?';
$_['text_next_choice']         = 'If you have a discount coupon code or bonus points that you want to use, select the appropriate item below. And also, you can find out about the cost of shipping to your region.';
$_['text_empty']               = 'Your cart is empty!';
$_['text_day']                 = 'day';
$_['text_week']                = 'week';
$_['text_semi_month']          = 'fortnight';
$_['text_month']               = 'month';
$_['text_year']                = 'year';
$_['text_trial']               = 'Cost: %s; Periodicity: %s %s; Number of payments: %s; Next, ';
$_['text_recurring']           = 'Cost: %s; Periodicity: %s %s';
$_['text_length']              = 'Number of payments: %s';
$_['text_payment_cancel']     = 'until canceled';
$_['text_recurring_item']      = 'Recurring payments';
$_['text_payment_recurring']   = 'Billing profile';
$_['text_trial_description']   = 'Cost: %s; Periodicity: %d %s; Number of payments: %d;  Next,  ';
$_['text_payment_description'] = 'Cost: %s; Periodicity: %d %s; Number of payments: %d';
$_['text_payment_cancel']      = 'Cost: %s; Periodicity: %d %s; Number of payments: until canceled';

// Column
$_['column_image']             = 'Image';
$_['column_name']              = 'Name';
$_['column_model']             = 'Model';
$_['column_quantity']          = 'Amount';
$_['column_price']             = 'Price per unit';
$_['column_total']             = 'Total';

// Error
$_['error_stock']              = 'Products marked *** are missing in the right quantity or they are not in stock!';
$_['error_minimum']            = 'The minimum quantity for ordering an item %s is %s!';
$_['error_required']           = '%s required!';
$_['error_product']            = 'You have no items in your shopping cart!';
$_['error_recurring_required'] = 'ПоPlease select a payment frequency!';

