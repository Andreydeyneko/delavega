<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l, d F Y';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ' ';

// Text
$_['text_home']             = 'Home';
$_['text_yes']              = 'Yes';
$_['text_no']               = 'No';
$_['text_none']             = ' --- Not selected --- ';
$_['text_select']           = ' --- Select  --- ';
$_['text_all_zones']        = 'All zones';
$_['text_pagination']       = 'Displayed from %d to %d of %d (total %d pages)';
$_['text_loading']          = 'Loading...';
$_['text_no_results']       = 'No data!';
$_['text_more']             = 'More';
$_['text_name']             = 'Name';
$_['text_close']            = 'close';
$_['text_load_more']        = 'Load more';
$_['discount__button_send'] = 'REQUEST A PROMO';
$_['button_search_404'] = 'Site search';


// Buttons
$_['button_address_add']    = 'Add address';
$_['button_back']           = 'Back';
$_['button_continue']       = 'Continue';
$_['button_home']           = 'Home';
$_['button_cart']           = 'Add to cart';
$_['button_cancel']         = 'Cancel';
$_['button_compare']        = 'Add to comparison';
$_['button_wishlist']       = 'dd to bookmarks';
$_['button_checkout']       = 'Checkout';
$_['button_confirm']        = 'Order confirmation';
$_['button_coupon']         = 'Apply coupon';
$_['button_delete']         = 'Delete';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Edit';
$_['button_filter']         = 'Search';
$_['button_new_address']    = 'Новый адрес';
$_['button_change_address'] = 'New Address';
$_['button_reviews']        = 'Feedback';
$_['button_write']          = 'Write a feedback';
$_['button_login']          = 'Log In';
$_['button_update']         = 'Refresh';
$_['button_remove']         = 'Delete';
$_['button_reorder']        = 'Additional order';
$_['button_return']         = 'Return';
$_['button_shopping']       = 'Continue shopping';
$_['button_search']         = 'Search';
$_['button_shipping']       = 'Apply Delivery';
$_['button_submit']         = 'Apply';
$_['button_guest']          = 'Placing an order without registration';
$_['button_view']           = 'View';
$_['button_voucher']        = 'Apply Gift Certificate';
$_['button_upload']         = 'Download file';
$_['button_reward']         = 'Apply reward points';
$_['button_quote']          = 'Get prices';
$_['button_list']           = 'List';
$_['button_grid']           = 'Grid';
$_['button_map']            = 'View Map';
$_['button_register']       = 'Register';
$_['button_login']          = 'Log in';
$_['button_confirm_order'] = 'Buy';

// Contact form
$_['text_name'] = 'Name';
$_['text_phone'] = 'Phone';
$_['text_email'] = 'Email';
$_['text_send'] = 'Send';

// Popup
$_['text_thanks_title'] = 'Thank you!';
$_['text_thanks_contact'] = 'Your request was sent. Our manager will contact you soon.';
$_['text_thanks_individ'] = 'After checking your data, the file will be sent to your email.';
$_['text_thanks_promo'] = 'Our manager will contact you soon for details';
$_['text_viber'] = '"Connect with Viber';
$_['text_telegram'] = 'Connect with Telegram';
$_['text_instagram'] = 'We are in Instagram';
$_['text_pinterest'] = 'We are in Pinterest';
$_['text_facebook'] = 'We are in Facebook';
$_['text_call'] = 'Call us';

// Error
$_['error_exception']       = 'Error code(%s): %s in %s stroke %s';
$_['error_upload_1']        = 'Warning: The size of the upload file exceeds the value of upload_max_filesize in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE value that was specified in the settings!';
$_['error_upload_3']        = 'Warning: The downloaded files were only partially uploaded!';
$_['error_upload_4']        = 'Warning: No downloads!';
$_['error_upload_6']        = 'Warning: Temporary folder!';
$_['error_upload_7']        = 'Warning: Error writing!';
$_['error_upload_8']        = 'Warning: It is forbidden to upload a file with this extension!';
$_['error_upload_999']      = 'Warning: Unknown error!';
$_['error_curl']            = 'CURL: Error code(%s): %s';
$_['error_name']     = 'Enter your name!';
$_['error_phone'] = 'Phone entered incorrectly!';
$_['error_promo'] = 'You already have a promotional code!';


/* Когда нужен перевод скриптов, просто добавь код языка */

// Datepicker
$_['datepicker']                    = 'en';

