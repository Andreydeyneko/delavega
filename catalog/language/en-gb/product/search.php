<?php
// Heading
$_['heading_title']     = 'Search';
$_['heading_search']    = 'Search results';
$_['heading_tag']       = 'By tag - ';

// Text
$_['text_search']       = 'Search results';
$_['text_search_found'] = 'Products matching the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';
$_['text_sub_category'] = 'Search in subcategories';
$_['text_empty']        = 'There are no products that match the search criteria.';
$_['text_quantity']     = 'Amount:';
$_['text_manufacturer'] = 'Manufacturer:';
$_['text_model']        = 'Model:';
$_['text_points']       = 'Bonus points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'Without VAT:';
$_['text_reviews']      = 'Total reviews: %s';
$_['text_compare']      = 'Compare Products (%s)';
$_['text_sort']         = 'Sort:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_rating_asc']   = 'Price (low &gt; high)';
$_['text_rating_desc']  = 'Price (high &gt; low)';
$_['text_rating_asc']   = 'Rating (starting from low)';
$_['text_rating_desc']  = 'Rating (starting with high)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';

// Entry
$_['entry_search']      = 'Search';
$_['text_no_results']   = 'No results were found for the query "%s". Try again.';
$_['text_no_results_found']   = 'Nothing was found by request. Try again.';
$_['entry_description'] = 'Search in the product description';
$_['add_wishlist'] = 'Add to wishlist';
