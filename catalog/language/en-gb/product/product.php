<?php
// Text
$_['text_search']              = 'Search';
$_['text_brand']               = 'Manufacturer';
$_['text_size']                = 'Dimensions PDF';
$_['text_manufacturer']        = 'Manufacturer:';
$_['text_model']               = 'Model:';
$_['text_reward']              = 'Bonus points:';
$_['text_points']              = 'Price in bonus points:';
$_['text_stock']               = 'Availability:';
$_['text_instock']             = 'In stock';
$_['text_tax']                 = 'Without VAT:';
$_['text_discount']            = ' or more:';
$_['text_option']              = 'Available options';
$_['text_minimum']             = 'Minimum order quantity: %s';
$_['text_reviews']             = '%s reviews';
$_['text_write']               = 'Write a review';
$_['text_login']               = 'Please  <a href="%s">login </a> or <a href="%s">create an account </a> before writing a review';
$_['text_no_reviews']          = 'There are no reviews for this product yet.';
$_['text_note']                = '<span style="color: #FF0000;">Note:</span> HTML markup is not supported! Use plain text. ';
$_['text_success']             = 'Thank you for your feedback. He entered the administrator to check for spam and will soon be published. ';
$_['text_related']             = 'Featured Products';
$_['text_tags']                = '<i class="fa fa-tags"></i>';
$_['text_error']               = 'Product not found!';
$_['text_payment_recurring']   = 'Billing profile';
$_['text_trial_description']   = 'Total: %s; Periodicity: %d %s; Number of payments: %d, Next  ';
$_['text_payment_description'] = 'Total: %s; Periodicity:  %d %s; Number of payments:  %d ';
$_['text_payment_cancel']      = 'Total: %s; Periodicity:  %d %s ; Number of payments: until canceled';
$_['text_day']                 = 'day';
$_['text_week']                = 'weeks';
$_['text_semi_month']          = 'fortnight';
$_['text_month']               = 'month';
$_['text_year']                = 'year';
$_['text_individual_size']     = 'Custom size';
$_['text_final_price']         = 'Total:';
$_['text_also_buy']            = 'People buy with this product';
$_['text_description_product'] = 'Product description';
$_['text_review_title']        = 'Leave a review';
$_['text_review_send']         = 'Send Feedback';
$_['text_own_size_title']      = 'Custom order';
$_['text_own_size_text']       = 'Thank you! We will contact you soon regarding your order.';
$_['text_own_size_send']       = 'Submit';
$_['text_video']               = 'Video';
$_['text_3d_model']            = '3D Model';
$_['text_3d_form_title']       = 'Request 3D for designers';
$_['text_mechanism_tip']       = 'When you select this item, you add a special <strong> lifting mechanism </strong> to the product';
$_['text_download_size']       = 'Download size';
$_['text_3d_overview']         = 'Watch 3d review';
$_['text_video_overview']      = 'Video of the mechanism';
$_['text_review_thanks']       = 'Thanks for the feedback';
$_['text_review_after']        = 'It will appear on the site after checking the moderator.';
$_['text_where_buy']           = 'Where to buy';
$_['text_about_size']          = 'Learn more';
$_['text_about_discounts']     = 'About discounts';
$_['text_select_color']        = 'Color';
$_['text_select_color_type']   = '-- Select fabric color --';
$_['text_vid'] = 'from ';
$_['price_annot']   = '*cost in standard size and up to 3 months';
$_['price_discount']   = '*price for standard size, production period - 3 months';

$_['text_location']  = 'Our showrooms';
$_['text_store']     = 'Showroom in Kiev';
$_['text_contact']   = 'Feedback Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Phone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Quality control department';
$_['text_comment']   = 'Additional information';
$_['text_block_sale'] = 'Sales department';
$_['text_block_marketing'] = 'Marketing and Advertising';
$_['text_socials']   = 'We are in social networks';
$_['button_submit']  = 'Send message';
$_['add_wishlist'] = 'Add to wishlist';

$_['button_download3d']       = 'Request file';

// Entry
$_['entry_qty']                = 'Amount';
$_['entry_name']               = 'Name';
$_['entry_lastname']           = 'Last Name';
$_['entry_company']            = 'Company';
$_['entry_site']               = 'Website';
$_['entry_city']               = 'City';
$_['entry_email']              = 'Email';
$_['entry_review']             = 'Your Feedback';
$_['entry_phone']              = 'Phone';
$_['entry_rating']             = 'Rating:';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Tabs
$_['tab_description']          = 'Description';
$_['tab_attribute']            = 'Characteristics';
$_['tab_review']               = 'Product Reviews';

// Error
$_['error_name']               = 'The name must be from 3 to 25 characters!';
$_['error_text']               = 'The response text must be from 25 to 1000 characters!';
$_['error_rating']             = 'Please select a rating!';

