<?php
// Heading
$_['heading_title']     = 'Manufacturers';

// Text
$_['text_brand']        = 'Manufacturers';
$_['text_index']        = 'Alphabetical index:';
$_['text_error']        = 'Manufacturer not found!';
$_['text_empty']        = 'There are no products from this manufacturer.';
$_['text_quantity']     = 'Amount:';
$_['text_manufacturer'] = 'Manufacturer:';
$_['text_model']        = 'Model:';
$_['text_points']       = 'Bonus points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'БWithout VAT:';
$_['text_compare']      = 'Compare Products (%s)';
$_['text_sort']         = 'Sort:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (low &gt; high)';
$_['text_price_desc']   = 'Price (high &gt; low)';
$_['text_rating_asc']   = 'Rating (starting from low)';
$_['text_rating_desc']  = 'Rating (starting with high)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';

