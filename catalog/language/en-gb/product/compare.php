<?php
// Heading
$_['heading_title']     = 'Compare Products';

// Text
$_['text_product']      = 'Description';
$_['text_name']         = 'Product';
$_['text_image']        = 'Image';
$_['text_price']        = 'Price';
$_['text_model']        = 'Model';
$_['text_manufacturer'] = 'Manufacturer';
$_['text_availability'] = 'In stock';
$_['text_instock']      = 'Available in stock';
$_['text_rating']       = 'Rating';
$_['text_reviews']      = 'Total reviews: %s';
$_['text_summary']      = 'Short Description';
$_['text_weight']       = 'Weight';
$_['text_dimension']    = 'Dimensions (L x W x H)';
$_['text_compare']      = 'Compare Products (%s)';
$_['text_success']      = 'Product <a href="%s">%s</a> has been added to your <a href="%s">comparison list</a>!';
$_['text_remove']       = 'Removed from comparison list';
$_['text_empty']        = 'You have not selected any products to compare.';

