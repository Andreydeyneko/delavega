<?php
// Text
$_['text_subject']  = '%s - product review';
$_['text_waiting']  = 'New reviews are awaiting your review.';
$_['text_product']  = 'Product: %s';
$_['text_reviewer'] = 'Name: %s';
$_['text_lastname'] = 'Last name: %s';
$_['text_rating']   = 'Rating: %s';
$_['text_review']   = 'Review:';

