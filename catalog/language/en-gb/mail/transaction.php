<?php
// Text
$_['text_subject']  = '%s - Affiliate Commission';
$_['text_received'] = 'Congratulations! You received a commission payment in the amount of% s for the affiliate program ';
$_['text_amount']   = 'You have received a commission:';
$_['text_total']    = 'Total commission balance:';

