<?php
// Text
$_['text_subject']    = 'Callback - Delavega';
$_['text_name']       = 'Name: %s';
$_['text_email']      = 'Email: %s';
$_['text_phone']      = 'Phone: %s';
$_['subject']    = 'Request a personal discount';
$_['text_body']    = 'The buyer wants to get a personal discount';
$_['text_code']    = 'Code %s discount %s';

$_['text_message'] = 'Question';
$_['text_subject_feedback'] = 'Ask question';