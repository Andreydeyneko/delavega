<?php
// Text
$_['text_subject']      = '%s -  Order Update %s';
$_['text_order_id']     = 'Order Number:';
$_['text_date_added']   = 'Order Date:';
$_['text_order_status'] = 'Your Order has been updated with the following status:';
$_['text_comment']      = 'Comment to your order:';
$_['text_link']         = 'To view an order, click the link below:';
$_['text_footer']       = 'If you have any questions, please reply to this message.';

