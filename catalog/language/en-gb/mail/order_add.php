<?php
// Text
$_['text_subject']          = '%s - Your order №%s is formed';
$_['text_greeting']         = 'Thank you for your interest in products upholstered furniture factories <a href="%s" style="color: #fff; line-height: 1.8; text-decoration: none; font-weight: 600;">Delavega.ua</a> Your order has been received and will be processed shortly';
$_['text_title']         = 'Ukrainian factory of premium upholstered furniture';
$_['text_in_order']         = 'In your order:';
$_['text_link']             = 'To view your order, follow the link:';
$_['text_order_detail']     = 'Order Details';
$_['text_instruction']      = 'Instructions';
$_['text_order_id']         = 'Order:';
$_['text_date_added']       = 'Order Date:';
$_['text_order_status']     = 'Status:';
$_['text_payment_method']   = 'Payment method:';
$_['text_shipping_method']  = 'Delivery method:';
$_['text_email']            = 'E-mail:';
$_['text_telephone']        = 'Phone:';
$_['text_ip']               = 'IP-address:';
$_['text_payment_address']  = 'Address of the payer';
$_['text_shipping_address'] = 'Shipping address';
$_['text_products']         = 'Products';
$_['text_product']          = 'Product';
$_['text_model']            = 'Model';
$_['text_quantity']         = 'Q-ty:';
$_['text_sht']              = 'PC';
$_['text_price']            = 'Price:';
$_['text_order_total']      = 'Order total';
$_['text_total']            = 'Total';
$_['text_social']           = 'We are in social networks';
$_['text_control']          = 'Quality Control Department:';
$_['text_current_year']     = '© %s %s';
$_['text_download']         = 'After payment is confirmed, the downloadable goods will be available at the link:';
$_['text_comment']          = 'Comment to your order:';
$_['text_footer']           = '© 2010-%s Delavega.ua. All rights reserved';
$_['service_lift'] = 'Freight elevator';
$_['pay_onlayn_text'] = '<ul style="list-style: none;line-height: 2;"><li>Online payment details:</li><li>FLP Podgorny Artur Grigorovich</li><li>EDRPOU 2522711672, tel. 0501416587</li><li>Payment Account UA183206490000026004052624358 in FC "PRIVATBANK", CALCULATION CENTER</li><li>MFO 320649</li><li>Address: Kyiv, 11A Revutskogo Str., apt. 199</li><li><br/></li><li>IMPORTANT: DO NOT PAY for your order without previous agreeing the details of the order with our managers</li></ul>';


