<?php
// Text
$_['text_subject']  = 'You have sent a Gift Certificate %s';
$_['text_greeting'] = 'Congratulations, you have received a Gift Certificate from %s';
$_['text_from']     = 'You have received a Gift Certificate from %s';
$_['text_message']  = 'The sender has left you a message';
$_['text_redeem']   = 'To redeem this Gift Certificate, save the code <b>% s </ b> then go to the store and order products that you like. You can enter the gift certificate code on the cart view page before you start placing an order. ';
$_['text_footer']   = 'If you have any questions, please reply to this message.';

