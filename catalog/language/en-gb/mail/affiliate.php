<?php
// Text
$_['text_subject']        = '%s - Affiliate Program';
$_['text_welcome']        = 'Congratulations. You have become an affiliate of our% s store! ';
$_['text_login']          = 'Your account has been created and you can log in using your E-mail and password at the link:';
$_['text_approval']       = 'Your account is awaiting confirmation. After confirmation, you can enter the partner section of our site at the link: ';
$_['text_service']        = 'After logging in, you can generate tracking codes, track fees and edit account information.';
$_['text_thanks']         = 'Thank you,';
$_['text_new_affiliate']  = 'New Partner';
$_['text_signup']         = 'A new partner has been registered:';
$_['text_website']        = 'Website:';
$_['text_customer_group'] = 'Group:';
$_['text_firstname']      = 'First name, patronymic:';
$_['text_lastname']       = 'Last Name:';
$_['text_company']        = 'Company:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Phone:';

