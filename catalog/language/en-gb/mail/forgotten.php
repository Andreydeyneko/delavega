<?php
// Text
$_['text_subject']  = '%s - New Password';
$_['text_greeting'] = 'You requested a new password in %s.';
$_['text_change']   = 'To reset your password, click the link:';
$_['text_ip']       = 'A new password was requested from this IP: %s';

