<?php
// Text
$_['text_subject']      = '%s - Order %s';
$_['text_received']     = 'You have received an order.';
$_['text_order_id']     = 'Order Number:';
$_['text_date_added']   = 'Order Date:';
$_['text_order_status'] = 'Order Status:';
$_['text_product']      = 'Products';
$_['text_total']        = 'Total';
$_['text_comment']      = 'Comment to your order:';
