<?php
// Text
$_['text_subject']        = '%s - Thank you for registering';
$_['text_welcome']        = 'Welcome to %s and thank you for registering !!';
$_['text_login']          = 'Your account has been created and you can log in using your E-mail and password at the link:';
$_['text_approval']       = 'Your account has been created and is awaiting confirmation. After confirmation, you can enter the store using E-Mail and password by the link: ';
$_['text_service']        = 'After logging in to the system, you can get access to various services of the Store, such as reviewing recent orders, printing an invoice and editing information about your account.';
$_['text_thanks']         = 'Thank you,';
$_['text_new_customer']   = 'New customer';
$_['text_signup']         = 'A new customer has been registered:';
$_['text_customer_group'] = 'Customer group:';
$_['text_firstname']      = 'First name, Patronymic:';
$_['text_lastname']       = 'Last Name:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Phone:';
