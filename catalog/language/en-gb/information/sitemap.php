<?php
// Heading
$_['heading_title']    = 'Sitemap';

// Text
$_['text_special']     = 'Promotions';
$_['text_account']     = 'My Account';
$_['text_edit']        = 'Personal Information';
$_['text_password']    = 'Password';
$_['text_address']     = 'My Addresses';
$_['text_history']     = 'Purchase history';
$_['text_download']    = 'Files to download';
$_['text_cart']        = 'Shopping cart';
$_['text_checkout']    = 'Checkout';
$_['text_search']      = 'Searcg';
$_['text_information'] = 'Information';
$_['text_contact']     = 'Our contacts';

