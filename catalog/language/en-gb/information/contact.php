<?php
// Heading
$_['heading_title']  = 'Contacts';

// Text
$_['text_location']  = 'Our showrooms';
$_['text_store']     = 'Showroom in Kiev';
$_['text_contact']   = 'Feedback Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Phone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Quality control department';
$_['text_comment']   = 'Additional information';
$_['text_success']   = '<p>Your message has been successfully sent to the store owner! </p>';
$_['text_agree']     = 'I read <a href="%ss class="agree"> <b>%s</b> </a> and I agree with <br /> terms of security and personal handling data <input name = "personal" checked required type = "checkbox">';
$_['text_block_sale'] = 'Sales department';
$_['text_block_marketing'] = 'Marketing and Advertising';
$_['text_socials']   = 'We are in social networks';
$_['button_submit']  = 'Send message';
$_['text_block_design'] = 'Department of work with designers and dealers';
$_['text_block_suppliers'] = 'Department of work with suppliers of materials and components';
$_['email_owner']  = 'Write a letter to the company owner';
$_['feedback_button']  = 'Feedback form';
// Entry
$_['entry_name']     = 'Your name';
$_['entry_email']    = 'Your E-Mail';
$_['entry_enquiry']  = 'Your question or message';

// Email
$_['email_subject']  = 'Message from %s';

$_['title_form'] = 'Write to us';
$_['form_name'] = 'Your Name';
$_['form_number'] = 'Phone Number';
$_['form_question'] = 'Leave a question';
$_['form_send'] = 'Submit';

// Errors
$_['error_name']     = 'The name must be from 3 to 32 characters!';
$_['error_email']    = 'E-mail address is not valid!';
$_['error_enquiry']  = 'Text length must be from 10 to 3000 characters!';

