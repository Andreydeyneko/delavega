<?php
// Heading
$_['heading_title']    = 'Maintenance mode';

// Text
$_['text_maintenance'] = 'Shop temporarily closed';
$_['text_message']     = '<h1 style="text-align:center;">Shop temporarily closed. We are doing maintenance work. <br /> Soon the store will be available. Please come back later.</h1>';

