<?php
// Text
$_['text_home']          = 'Main';
//$_['text_wishlist']      = '%s';
$_['text_wishlist_custom']      = 'A wish list';
$_['text_shopping_cart'] = 'Basket';
$_['text_category']      = 'Catalog';
$_['text_contant']       = 'Contacts';
$_['text_company']       = 'Company';
$_['text_account']       = 'Personal account';
$_['text_register']      = 'Registration';
$_['text_login']         = 'Authorization ';
$_['text_order']         = 'Purchase history';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Log out';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'See all';
$_['text_download_catalog'] = 'Catalog PDF';
$_['text_download_link'] = '/en/catalog-en';
$_['text_choice_lang'] = 'Select Language';
$_['text_choice_currency'] = 'Choose currency';