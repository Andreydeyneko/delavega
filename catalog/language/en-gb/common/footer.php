<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Support';
$_['text_extra']        = 'Advanced';
$_['text_contact']      = 'Contacts';
$_['text_return']       = 'Return of goods';
$_['text_sitemap']      = 'Sitemap';
$_['text_manufacturer'] = 'Manufacturers';
$_['text_voucher']      = 'Gift certificates';
$_['text_affiliate']    = 'Affiliate Program';
$_['text_special']      = 'Promotions';
$_['text_account']      = 'Personal Account';
$_['text_order']        = 'Order history';
$_['text_wishlist']     = 'Bookmarks';
$_['text_newsletter']   = 'Newsletter';
$_['text_quality']      = 'Quality Assurance Department:';
$_['text_question']     = 'Technical Issues';
$_['text_getdiscounts']       = 'Get a discount';

$_['text_room']         = 'ShowRoom';
$_['text_create']       = 'Creating a site';
$_['text_powered']      = '&copy; %s %s';
$_['text_subscribe']    = 'Subscribe to newsletter';
$_['text_subscribe_button']    = 'Subscribe';
$_['text_enter_account']    = 'Login to your account';
$_['text_enter']    = 'LOG IN';
$_['text_register']    = 'Register';
$_['text_subscribe_success']   = 'You are subscribed to the newsletter';
$_['text_password_resend_success'] = 'Re-sent password';
$_['text_instruction']    = 'You must <a href="%s" class="popup__link">sign in</a> or <a href="%s" class="popup__link">create an account</a>, add items to favorites. ';
$_['text_cookies_title']    = 'The site uses Cookies';
$_['text_cookies_text']    = '<b>The site uses Cookies</b>, if you do not agree with the "<a href="%s" class="popup__link">Privacy Policy</a>" and cookies storage on your device, you can discontinue the use of this website.';
$_['text_cookies_more']    = 'MORE INFORMATION';
$_['text_cookies_close']    = 'Close';