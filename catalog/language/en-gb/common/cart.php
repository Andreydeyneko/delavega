<?php
// Text
$_['text_items']     = 'Products %s (%s)';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'Go to cart';
$_['text_checkout']  = 'Place an order';
$_['text_checkout']  = 'Place an order';
$_['text_recurring'] = 'Billing profile';
$_['text_cart_quantity'] = 'Amount:';
$_['text_cart_sht'] = 'pcs';
$_['text_cart_price'] = 'Price:';
$_['text_continue_shopping'] = 'Continue shopping';
$_['text_cart_title'] = 'Product added to cart';