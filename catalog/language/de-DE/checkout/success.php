<?php
// Heading
$_['heading_title']        = 'Ihre Bestellung wurde erfolgreich erstellt!';

// Text
$_['text_basket']          = 'Einkaufswagen';
$_['text_checkout']        = 'Bestellung aufgeben';
$_['text_success']         = 'Danke!';
$_['text_customer']        = 'Ihre Bestellung <a href="%s" class="thanks__number">№%s</a> wurde erfolgreich erstellt und hat eine Benachrichtigung an Email gesendet';
$_['text_guest']           = '<p>Ihre Bestellung wird akzeptiert!</p><p> Wenn Sie Fragen haben, <a href="%s"> kontaktieren Sie uns </a>.</p><p>Vielen Dank für den Einkauf in unserem Online-Shop!</p>';
$_['text_account_title']   = 'Mein Konto';
$_['text_latest_articles'] = 'Neueste Veröffentlichungen';
$_['text_all_news']        = 'Alle Nachrichten';
$_['text_we_socials']      = 'Wir sind in sozialen Netzwerken';

$_['button_to_home'] = 'Einkauf fortsetzen';