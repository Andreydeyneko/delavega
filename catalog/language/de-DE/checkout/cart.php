<?php
// Heading
$_['heading_title']            = 'Einkaufskorb';

// Text
$_['text_success']             = '<a href="%s">%s</a> hat <a href="%s"> zu Ihrem Einkaufswagen hinzugefügt </a>!';
$_['text_remove']              = 'Ihr Einkaufswagen wurde geändert!';
$_['text_login']               = 'Sie müssen sich <a href="%s">anmelden</a> anmelden <a href="%s">ein Konto erstellen</a> um die Preise anzuzeigen!';
$_['text_items']               = 'Produkte %s  (%s)';
$_['text_points']              = 'Bonuspunkte: %s';
$_['text_next']                = 'Was möchtest du als nächstes machen?';
$_['text_next_choice']         = 'Wenn Sie einen Rabatt-Gutscheincode oder Bonuspunkte haben, die Sie verwenden möchten, wählen Sie den entsprechenden Artikel unten. Und Sie können sich auch über die Versandkosten in Ihrer Region informieren.';
$_['text_empty']               = 'Dein Warenkorb ist leer!';
$_['text_day']                 = 'tag';
$_['text_week']                = 'woche';
$_['text_semi_month']          = 'zwei Wochen';
$_['text_month']               = 'monat';
$_['text_year']                = 'jahr';
$_['text_trial']               = 'Kosten: %s; Periodizität: %s %s; Anzahl der Zahlungen: %s; Weiter, ';
$_['text_recurring']           = 'Kosten: %s; Periodizität: %s %s';
$_['text_length']              = ' Anzahl der Zahlungen: %s';
$_['text_payment_cancel']     = 'bis abgebrochen';
$_['text_recurring_item']      = 'Wiederkehrende Zahlungen';
$_['text_payment_recurring']   = 'Abrechnungsprofil';
$_['text_trial_description']   = 'Kosten: %s; Periodizität: %d %s; Anzahl der Zahlungen: %d;  Weiter,  ';
$_['text_payment_description'] = 'Kosten: %s; Periodizität: %d %s; Anzahl der Zahlungen: %d';
$_['text_payment_cancel']      = 'Kosten: %s; Periodizität: %d %s; Anzahl der Zahlungen: до отмены';

// Column
$_['column_image']             = 'Bild';
$_['column_name']              = 'Name';
$_['column_model']             = 'Modell';
$_['column_quantity']          = 'Warenmenge';
$_['column_price']             = 'Preis pro Einheit.';
$_['column_total']             = 'Gesamt';

// Error
$_['error_stock']              = 'Mit *** gekennzeichnete Produkte fehlen in der richtigen Menge oder sind nicht auf Lager!';
$_['error_minimum']            = 'Die Mindestmenge für die Bestellung eines Artikels %s ist %s!';
$_['error_required']           = '%s erforderlich!';
$_['error_product']            = 'Sie haben keine Artikel in Ihrem Warenkorb!';
$_['error_recurring_required'] = 'Bitte wählen Sie eine Zahlungshäufigkeit!';

