<?php
// Heading
$_['heading_title']                  = 'Einkaufskorb';

// Text
$_['text_cart']                      = 'Einkaufskorb';
$_['text_checkout_option']           = 'Schritt %s: Bestellmethode';
$_['text_checkout_account']          = 'Kontaktinformationen';
$_['text_checkout_payment_address']  = 'Schritt %s: Rechnungsinformation';
$_['text_checkout_shipping_address'] = 'Versandart und Bezahlung';
$_['text_checkout_shipping_method']  = 'Versandart:';
$_['text_checkout_payment_method']   = 'Zahlungsart:';
$_['text_also_buy']            = 'Leute kaufen mit diesem Produkt';
$_['text_checkout_confirm']          = 'Schritt 6: Auftragsbestätigung';
$_['text_modify']                    = 'Bearbeiten &raquo;';
$_['text_new_customer']              = 'Neuer Kunde';
$_['text_returning_customer']        = 'Registrierter Benutzer';
$_['text_checkout']                  = 'Bestelloptionen';
$_['text_i_am_returning_customer']   = 'Ich habe hier früher Einkäufe getätigt und mich registriert';
$_['text_register']                  = 'Registrierung';
$_['text_guest']                     = 'Eine Bestellung aufgeben, ohne sich zu registrieren';
$_['text_register_account']          = 'Das Erstellen eines Kontos hilft Ihnen, Einkäufe schneller und bequemer zu machen und als normaler Kunde Rabatte zu erhalten.';
$_['text_forgotten']                 = 'Passwort vergessen?';
$_['text_your_details']              = 'Persönliche Daten';
$_['text_your_address']              = 'Versandadresse';
$_['text_your_password']             = 'Passwort';
$_['text_agree']                     = 'Ich habe <a href="%ss class="agree"> <b>% s </ b> </a> gelesen und stimme den Sicherheitsbestimmungen und der persönlichen Handhabung zu Daten';
$_['text_address_new']               = 'Ich möchte die neue Adresse verwenden';
$_['text_address_existing']          = 'Ich möchte eine bestehende Adresse verwenden';
$_['text_shipping_method']           = 'Wählen Sie eine bequeme Versandart für diese Bestellung';
$_['text_payment_method']            = 'Wählen Sie eine Zahlungsmethode für diesen Auftrag';
$_['text_comments']                  = 'Sie können Ihrer Bestellung einen Kommentar hinzufügen';
$_['text_recurring_item']            = 'Periodizitätselement';
$_['text_payment_recurring']         = 'Abrechnungsprofil';
$_['text_trial_description']         = 'Kosten: %s; Periodizität: %d %s; Anzahl der Zahlungen: %d;  Weiter,  ';
$_['text_payment_description']       = 'Kosten: %s; Periodizität: %d %s; Anzahl der Zahlungen: %d';
$_['text_payment_cancel']            = '%s alle %d %s(й) noch nicht storniert';
$_['text_day']                       = 'tag';
$_['text_week']                      = 'woche';
$_['text_semi_month']                = 'zwei wochen';
$_['text_month']                     = 'monat';
$_['text_year']                      = 'jahr';
$_['text_next_step']                 = 'Nächster Schritt';
$_['text_chouse_shipping']           = 'Auswahl der Versand- und Zahlungsarten';
$_['text_shipping']                  = 'Versand';
$_['text_payment']                   = 'Zahlung';
$_['text_comment_to']                = 'Auftragskommentar';
$_['text_yours_comment']             = 'Kommentar hinzufügen';
$_['text_delete_from_cart']          = 'Aus dem Papierkorb entfernen';
$_['text_sht']                       = 'pcs';
$_['text_your_sale']                 = 'Ihr Rabattcode-Rabatt';
$_['text_service_lift']              = 'Das Vorhandensein eines Lastenaufzugs';
$_['text_promocode']                 = 'Aktionscode';
$_['text_disc_cart']                 = 'rabatt';

// Column
$_['column_name']                    = 'Produktname';
$_['column_model']                   = 'Modell';
$_['column_quantity']                = 'Warenmenge:';
$_['column_price']                   = 'Preis:';
$_['column_total']                   = 'Gesamt';

$_['button_re']						 = 'Bearbeiten';
$_['button_confirm_order']			 = 'Zu kaufen';

// Entry
$_['entry_email_address']            = 'E-Mail Adresse';
$_['entry_email']                    = 'Email';
$_['entry_password']                 = 'Passwort';
$_['entry_confirm']                  = 'Passwort bestätigen';
$_['entry_firstname']                = 'Vorname und Nachname';
$_['entry_lastname']                 = 'Nachname';
$_['entry_telephone']                = 'Telefon';
$_['entry_address']                  = 'Wähle eine Adresse';
$_['entry_company']                  = 'Firma';
$_['entry_customer_group']           = 'Geschäftstyp';
$_['entry_address_1']                = 'Adresse';
$_['entry_address_2']                = 'Adresse 2';
$_['entry_postcode']                 = 'Index';
$_['entry_city']                     = 'Stadt';
$_['entry_country']                  = 'Land';
$_['entry_zone']                     = 'Region / Staat';
$_['entry_newsletter']               = 'Ich möchte Nachrichten %s abonnieren.';
$_['entry_shipping']                 = 'Meine Lieferadresse stimmt mit Ihrer Rechnungsadresse überein.';
$_['entry_about_data']				 = 'Geben Sie Ihre Daten ein';

// Error
$_['error_warning']                  = 'Es gab ein Problem bei der Bearbeitung Ihrer Bestellung! Tritt das Problem erneut auf, versuchen Sie eine andere Zahlungsmethode oder wenden Sie sich an <a href="%s"> an den Store-Administrator </a>. ';
$_['error_login']                    = 'Fehler: Falsche E-Mail und / oder Passwort.';
$_['error_attempts']                 = 'Sie haben die maximale Anzahl von Anmeldeversuchen überschritten. Erneute Genehmigung der Site nach 1 Stunde ';
$_['error_approved']                 = 'Bevor Sie sich einloggen können, muss die Administration Ihr Konto genehmigen.';
$_['error_exists']                   = 'Fehler: die angegebene E-Mail-Adresse ist bereits registriert!';
$_['error_firstname']                = 'Der Name muss aus 1 bis 32 Zeichen bestehen!';
$_['error_lastname']                 = 'Der Nachname muss zwischen 1 und 32 Zeichen lang sein!';
$_['error_email']                    = 'E-Mail-Adresse wurde falsch eingegeben!';
$_['error_telephone']                = 'Die Telefonnummer muss aus 3 bis 32 Zeichen bestehen!';
$_['error_password']                 = 'Das Passwort muss zwischen 4 und 20 Zeichen lang sein!';
$_['error_confirm']                  = 'Passwörter stimmen nicht überein!';
$_['error_address_1']                = 'Die Adresse muss zwischen 3 und 128 Zeichen lang sein!';
$_['error_city']                     = 'Der Name der Stadt muss aus 2 bis 128 Zeichen bestehen!';
$_['error_postcode']                 = 'Der Index muss aus 2 bis 10 Zeichen bestehen!';
$_['error_country']                  = 'Bitte wählen Sie ein Land!';
$_['error_zone']                     = 'Bitte wählen Sie eine Region / Region';
$_['error_agree']                    = 'Sie müssen %s lesen und zustimmen!';
$_['error_address']                  = 'Sie müssen die Adresse angeben!';
$_['error_shipping']                 = 'Sie müssen die Liefermethode angeben!';
$_['error_no_shipping']              = 'Es gibt keine verfügbaren Übermittlungsmethoden.';
$_['error_payment']                  = 'Sie müssen eine Zahlungsmethode angeben!';
$_['error_no_payment']               = 'Es gibt keine verfügbaren Zahlungsmethoden.';
$_['error_custom_field']             = '%s ist erforderlich!';
$_['error_region_empty']             = 'Bereich nicht ausgewählt';

