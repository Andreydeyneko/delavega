<?php
// Text
$_['text_subject']  = '%s - Produktbewertung';
$_['text_waiting']  = 'Neue Rezensionen warten auf Ihre Bewertung.';
$_['text_product']  = 'Produkt: %s';
$_['text_reviewer'] = 'Name: %s';
$_['text_lastname'] = 'Nachname: %s';
$_['text_rating']   = 'Bewertung: %s';
$_['text_review']   = 'Überprüfung:';

