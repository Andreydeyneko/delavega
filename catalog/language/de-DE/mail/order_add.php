<?php
// Text
$_['text_subject']          = '%s - Ihre Bestellung wird gebildet';
$_['text_greeting']         = 'Vielen Dank für Ihr Interesse an Produkten <a href="%s" style="color: #fff; line-height: 1.8; text-decoration: none; font-weight: 600;">Delavega.ua</a> Ihre Bestellung wurde erhalten und wird in Kürze bearbeitet';
$_['text_in_order']         = 'In Ihrer Bestellung:';
$_['text_link']             = 'Um Ihre Bestellung anzuzeigen, folgen Sie dem Link:';
$_['text_order_detail']     = 'Bestelldetails';
$_['text_instruction']      = 'Anweisungen';
$_['text_order_id']         = 'Bestellnummer:';
$_['text_date_added']       = 'Bestelldatum:';
$_['text_order_status']     = 'Auftragsstatus:';
$_['text_payment_method']   = 'Zahlungsart:';
$_['text_shipping_method']  = 'Versandmethode:';
$_['text_email']            = 'E-mail:';
$_['text_telephone']        = 'Telefon:';
$_['text_ip']               = 'IP-Adresse:';
$_['text_payment_address']  = 'Adresse des Zahlers';
$_['text_shipping_address'] = 'Lieferadresse';
$_['text_products']         = 'Artikel';
$_['text_product']          = 'Produkte';
$_['text_model']            = 'Modell';
$_['text_quantity']         = 'Betrag:';
$_['text_sht']              = 'Stück';
$_['text_price']            = 'Preis:';
$_['text_order_total']      = 'Auftragssumme';
$_['text_total']            = 'Gesamt';
$_['text_social']           = 'Wir sind in sozialen Netzwerken';
$_['text_control']          = 'Abteilung für Qualitätskontrolle:';
$_['text_current_year']     = '© %s %s';
$_['text_download']         = 'Nachdem die Zahlung bestätigt wurde, sind die herunterladbaren Waren unter dem folgenden Link verfügbar:';
$_['text_comment']          = 'Kommentar zu Ihrer Bestellung:';
$_['text_footer']           = '© 2010-%s Delavega.ua. Alle Rechte vorbehalten';

