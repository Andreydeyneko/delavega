<?php
// Text
$_['text_subject']  = 'Sie haben ein Geschenkzertifikat gesendet %s';
$_['text_greeting'] = 'Herzlichen Glückwunsch, Sie haben ein Geschenkzertifikat von %s erhalten';
$_['text_from']     = 'Sie haben ein Geschenkzertifikat von %s erhalten';
$_['text_message']  = 'Der Absender hat Ihnen eine Nachricht hinterlassen';
$_['text_redeem']   = 'Um dieses Geschenkgutschein einzulösen, speichern Sie den Code <b>%s</b>, gehen Sie dann zum Geschäft und bestellen Sie die Produkte, die Sie mögen. Sie können den Gutscheincode auf der Warenkorb-Ansicht eingeben, bevor Sie mit der Bestellung beginnen. ';
$_['text_footer']   = 'Wenn Sie Fragen haben, antworten Sie bitte auf diese Nachricht.';

