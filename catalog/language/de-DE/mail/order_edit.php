<?php
// Text
$_['text_subject']      = '%s - Auftragsaktualisierung %s';
$_['text_order_id']     = 'Bestellnummer:';
$_['text_date_added']   = 'Bestelldatum:';
$_['text_order_status'] = 'Ihre Bestellung wurde mit dem folgenden Status aktualisiert:';
$_['text_comment']      = 'Kommentar zu Ihrer Bestellung:';
$_['text_link']         = 'Um eine Bestellung anzuzeigen, klicken Sie auf den folgenden Link:';
$_['text_footer']       = 'Wenn Sie Fragen haben, antworten Sie bitte auf diese Nachricht.';

