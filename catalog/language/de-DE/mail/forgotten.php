<?php
// Text
$_['text_subject']  = '%s - Neues Passwort';
$_['text_greeting'] = 'Sie haben ein neues Passwort in %s angefordert.';
$_['text_change']   = 'Um Ihr Passwort zurückzusetzen, klicken Sie auf den Link:';
$_['text_ip']       = 'Ein neues Passwort wurde von dieser IP angefordert: %s';

