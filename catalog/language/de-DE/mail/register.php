<?php
// Text
$_['text_subject']        = '%s - Danke für die Registrierung';
$_['text_welcome']        = 'Willkommen bei %s und danke für die Registrierung !!';
$_['text_login']          = 'Ihr Account wurde erstellt und Sie können sich mit Ihrer E-Mail und Ihrem Passwort unter dem folgenden Link einloggen:';
$_['text_approval']       = 'Ihr Konto wurde erstellt und wartet auf Bestätigung. Nach der Bestätigung können Sie den Laden per E-Mail und Passwort über den folgenden Link betreten: ';
$_['text_service']        = 'Nachdem Sie sich beim System angemeldet haben, können Sie auf verschiedene Services des Shops zugreifen, wie zum Beispiel aktuelle Bestellungen einsehen, eine Rechnung drucken und Informationen über Ihr Konto bearbeiten.';
$_['text_thanks']         = 'Danke,';
$_['text_new_customer']   = 'Neuer Kunde';
$_['text_signup']         = 'Ein neuer Kunde wurde registriert:';
$_['text_customer_group'] = 'Kundengruppe';
$_['text_firstname']      = 'Vorname, patronymic:';
$_['text_lastname']       = 'Nachname:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telefon:';
