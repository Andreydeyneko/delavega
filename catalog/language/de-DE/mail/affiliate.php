<?php
// Text
$_['text_subject']        = '%s - Partnerprogramm';
$_['text_welcome']        = 'Herzlichen Glückwunsch. Sie sind eine Filiale unseres %s Store geworden! ';
$_['text_login']          = 'Ihr Account wurde erstellt und Sie können sich mit Ihrer E-Mail und Ihrem Passwort unter dem folgenden Link einloggen:';
$_['text_approval']       = 'Ihr Konto wartet auf Bestätigung. Nach der Bestätigung können Sie den Partnerabschnitt unserer Website unter dem folgenden Link eingeben: ';
$_['text_service']        = 'Nach dem Einloggen können Sie Tracking-Codes generieren, Gebühren verfolgen und Kontoinformationen bearbeiten.';
$_['text_thanks']         = 'Danke';
$_['text_new_affiliate']  = 'Neuer Partner';
$_['text_signup']         = 'Ein neuer Partner wurde registriert:';
$_['text_website']        = 'Webseite:';
$_['text_customer_group'] = 'Gruppe:';
$_['text_firstname']      = 'Vorname, patronymic:';
$_['text_lastname']       = 'Nachname:';
$_['text_company']        = 'Firma:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telefon:';

