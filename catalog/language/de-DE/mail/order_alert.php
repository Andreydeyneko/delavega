<?php
// Text
$_['text_subject']      = '%s -  Auftrag %s';
$_['text_received']     = 'Sie haben eine Bestellung erhalten.';
$_['text_order_id']     = 'Bestellnummer:';
$_['text_date_added']   = 'Bestelldatum:';
$_['text_order_status'] = 'Auftragsstatus:';
$_['text_product']      = 'Produkte';
$_['text_total']        = 'Gesamt';
$_['text_comment']      = 'Kommentar zu Ihrer Bestellung:';
