<?php
// Heading
$_['heading_title'] = 'Geschenkgutschein verwenden';

// Text
$_['text_voucher']  = 'Geschenkgutschein (%s)';
$_['text_success']  = 'Ihr Geschenkgutschein wurde erfolgreich angewendet!';

// Entry
$_['entry_voucher'] = 'Geben Sie den Gutscheincode ein';

// Error
$_['error_voucher'] = 'Fehler. Falscher Gutscheincode oder dieses Zertifikat wurde bereits verwendet!';
$_['error_empty']   = 'Achtung: Geben Sie den Zertifikatscode ein!';

