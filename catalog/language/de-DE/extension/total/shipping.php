<?php
// Heading
$_['heading_title']        = 'Versandkosten berechnen';

// Text
$_['text_success']         = 'Die Berechnung wurde erfolgreich abgeschlossen!';
$_['text_shipping']        = 'Geben Sie Ihre Region für die Berechnung der Versandkosten an.';
$_['text_shipping_method'] = 'Bitte wählen Sie Ihre bevorzugte Versandart für den aktuellen Auftrag.';

// Entry
$_['entry_country']        = 'Land';
$_['entry_zone']           = 'Region / Staat';
$_['entry_postcode']       = 'Index';

// Error
$_['error_postcode']       = 'Der Index muss aus 2 bis 10 Zeichen bestehen!';
$_['error_country']        = 'Bitte wählen Sie ein Land!';
$_['error_zone']           = 'Bitte wählen Sie eine Region / Region!';
$_['error_shipping']       = 'Sie müssen die Liefermethode angeben!';
$_['error_no_shipping']    = 'Lieferung an diese Adresse ist nicht möglich. Bitte <a href="%s"> kontaktieren Sie das Geschäftsmanagement </a>!';

