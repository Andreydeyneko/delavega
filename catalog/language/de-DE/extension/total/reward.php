<?php
// Heading
$_['heading_title'] = 'Bonuspunkte verwenden (%s Punkte verfügbar)';

// Text
$_['text_reward']   = 'Bonuspunkte (%s)';
$_['text_order_id'] = 'Bestellnummer: %s';
$_['text_success']  = 'Punkte erfolgreich angewendet!';

// Entry
$_['entry_reward']  = 'Verfügbar (maximal %s)';

// Error
$_['error_reward']  = 'Bitte geben Sie die Anzahl der Bonuspunkte ein, die Sie für diese Bestellung bezahlen müssen!';
$_['error_points']  = 'Sie haben keine %s Bonuspunkte!';
$_['error_maximum'] = 'Die maximal verfügbaren Punkte, die angewendet werden können: %s!';

