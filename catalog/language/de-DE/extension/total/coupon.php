<?php
// Heading
$_['heading_title'] = 'Gutschein verwenden';

// Text
$_['text_coupon']   = 'Gutschein (%s)';
$_['text_success']  = 'Rabattcoupon erfolgreich angewendet!';

// Entry
$_['entry_coupon']  = 'Gutscheincode eingeben';

// Error
$_['error_coupon']  = 'Fehler. Ungültiger Rabatt-Gutscheincode Es ist möglich, dass der Gültigkeitszeitraum abgelaufen ist oder das Nutzungslimit erreicht ist!';
$_['error_empty']   = 'Achtung! Gutschein-Code eingeben';

