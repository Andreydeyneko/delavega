<?php
// Text
$_['text_title']       = 'Um FLP zu berücksichtigen';
$_['button_confirm_order'] = 'Kaufen';
$_['text_instruction'] = 'Banküberweisungsanweisungen';
$_['text_description'] = 'Bitte überweisen Sie den Gesamtbetrag auf folgendes Konto: Details hier';
$_['text_payment']     = 'Die Bestellung wird erst verarbeitet, wenn das Geld unser Bankkonto erreicht hat.';

