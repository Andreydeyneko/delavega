<?php
// Heading
$_['express_text_title']      = 'Auftragsbestätigung';

// Text
$_['text_title']              = 'PayPal Express Zahlungen (einschließlich Visa und MasterCard)';
$_['text_cart']               = 'Einkaufswagen';
$_['text_shipping_updated']   = 'Lieferung aktualisiert';
$_['text_trial']              = 'Betrag: %s; Periodizität: %s %s; Anzahl der Zahlungen: %s, Nächster ';
$_['text_recurring']          = 'Betrag: %s Periodizität: %s %s';
$_['text_recurring_item']     = 'Zahlungen wiederholen';
$_['text_length']             = 'Anzahl der Zahlungen: %s';

// Entry
$_['express_entry_coupon']    = 'Gutscheincode eingeben:';

// Button
$_['button_express_coupon']   = 'Hinzufügen';
$_['button_express_confirm']  = 'Bestätigen';
$_['button_express_login']    = 'Anmelden bei PayPal';
$_['button_express_shipping'] = 'Lieferung aktualisieren';

// Error
$_['error_heading_title']	  = 'Ein Fehler ist aufgetreten ...';
$_['error_too_many_failures'] = 'Während des Bezahlvorgangs ist ein Fehler aufgetreten';
$_['error_unavailable'] 	  = 'Bitte verwenden Sie das vollständige Design für diese Bestellung';
$_['error_no_shipping']    	  = 'Warnung! Keine Lieferoptionen verfügbar. Bitte <a href="%s"> schreiben Sie uns </a> zur Beratung!';


