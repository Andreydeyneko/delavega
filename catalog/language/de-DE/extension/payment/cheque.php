<?php
// Text
$_['text_title']       = 'Bargeld im Büro (Kiew)';
$_['button_confirm_order'] = 'Kaufen';
$_['text_instruction'] = 'Bargeld im Büro (Kiew)';
$_['text_payable']     = 'Zahlungsempfänger:';
$_['text_address']     = 'Akzeptieren von Zahlungen bei:';
$_['text_payment']     = 'Ihre Bestellung wird erst verarbeitet, wenn wir die Zahlung erhalten haben.';

