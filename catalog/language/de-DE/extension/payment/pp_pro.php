<?php
// Text
$_['text_title']				= 'Visa und MasterCard (PayPal) Karten';
$_['text_wait']					= 'Bitte warten!';
$_['text_credit_card']			= 'Zahlungskarteninformation';

// Entry
$_['entry_cc_type']				= 'Kartentyp:';
$_['entry_cc_number']			= 'Kartennummer:';
$_['entry_cc_start_date']		= 'Startdatum der Gültigkeit der Karte:';
$_['entry_cc_expire_date']		= 'Ablaufdatum der Karte:';
$_['entry_cc_cvv2']				= 'Sicherheitscode (CVV2):';
$_['entry_cc_issue']			= 'Kartencode (Problem):';

// Help
$_['help_start_date']			= '((falls bekannt)';
$_['help_issue']				= '(nur für Maestro- und Solo-Karten)';

