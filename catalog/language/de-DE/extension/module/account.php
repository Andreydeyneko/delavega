<?php
// Heading
$_['heading_title']    = 'Mein Konto';

// Text
$_['text_register']    = 'Registrierung';
$_['text_login']       = 'Anmelden';
$_['text_logout']      = 'Abmelden';
$_['text_forgotten']   = 'Passwort vergessen?';
$_['text_account']     = 'Profil';
$_['text_edit']        = 'Kontaktinformationen bearbeiten';
$_['text_password']    = 'Passwort';
$_['text_address']     = 'Adressbuch';
$_['text_wishlist']    = 'Favoriten';
$_['text_order']       = 'Kaufhistorie';
$_['text_download']    = 'Dateien zum Herunterladen';
$_['text_reward']      = 'Bonuspunkte';
$_['text_return']      = 'Rückgabe';
$_['text_transaction'] = 'Transaktionsverlauf';
$_['text_newsletter']  = 'E-Mail verschicken';
$_['text_recurring']   = 'Wiederkehrende Zahlungen';

