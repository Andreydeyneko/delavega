<?php
// Heading
$_['heading_title']    = 'Sitemap';

// Text
$_['text_special']     = 'Aktionen';
$_['text_account']     = 'Mein Konto';
$_['text_edit']        = 'Persönliche Informationen';
$_['text_password']    = 'Passwort';
$_['text_address']     = 'Meine Adressen';
$_['text_history']     = 'Kaufhistorie';
$_['text_download']    = 'Dateien zum Herunterladen';
$_['text_cart']        = 'Einkaufswagen';
$_['text_checkout']    = 'Kasse';
$_['text_search']      = 'Suche';
$_['text_information'] = 'Informationen';
$_['text_contact']     = 'Unsere Kontakte';

