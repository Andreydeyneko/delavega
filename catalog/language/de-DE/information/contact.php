<?php
// Heading
$_['heading_title']  = 'Kontakte';

// Text
$_['text_location']  = 'Unsere Ausstellungsräume';
$_['text_store']     = 'Ausstellungsräume  Kiew';
$_['text_contact']   = 'Feedbackformular';
$_['text_address']   = 'Adresse';
$_['text_telephone'] = 'Telefon';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Abteilung für Qualitätskontrolle';
$_['text_comment']   = 'Zusätzliche Informationen';
$_['text_success']   = '<p>Ihre Nachricht wurde erfolgreich an den Eigentümer des Geschäfts gesendet!</ p>';
$_['text_agree']     = 'Ich habe <a href="%ss class="agree"> <b>%s</ b></a> gelesen und stimme den Sicherheitsbestimmungen und der persönlichen Handhabung zu Daten <input name = "personal" geprüft erforderlich type = "checkbox"> ';
$_['text_block_sale'] = 'Verkaufsabteilung';
$_['text_block_marketing'] = 'Marketing und Werbung';
$_['text_socials']   = 'Wir sind in sozialen Netzwerken';
$_['button_submit']  = 'Nachricht senden';

// Entry
$_['entry_name']     = 'Ihr Name';
$_['entry_email']    = 'Ihre E-Mail';
$_['entry_enquiry']  = 'Ihre Frage oder Nachricht';

// Email
$_['email_subject']  = 'Nachricht von %s';

// Errors
$_['error_name']     = 'Der Name muss aus 3 bis 32 Zeichen bestehen!';
$_['error_email']    = 'E-Mail Adresse ist nicht gültig!';
$_['error_enquiry']  = 'Die Textlänge muss zwischen 10 und 3000 Zeichen liegen!';

