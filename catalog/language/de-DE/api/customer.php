<?php
// Text
$_['text_success']       = 'Sie haben die Kunden erfolgreich geändert';

// Error
$_['error_permission']   = 'Warnung: Sie sind nicht berechtigt, auf die API zuzugreifen!';
$_['error_customer']     = 'Sie müssen einen Kunden auswählen!';
$_['error_firstname']    = 'Der Vorname muss zwischen 1 und 32 Zeichen lang sein!';
$_['error_lastname']     = 'Nachname muss zwischen 1 und 32 Zeichen enthalten!';
$_['error_email']        = 'E-Mail-Adresse scheint nicht gültig zu sein!';
$_['error_telephone']    = 'Telefon muss zwischen 3 und 32 Zeichen lang sein!';
$_['error_custom_field'] = '%s erforderlich!';

