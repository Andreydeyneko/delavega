<?php
// Text
$_['text_success']     = 'Erfolg: Sie haben Ihren Warenkorb geändert!';

// Error
$_['error_permission'] = 'Warnung: Sie sind nicht berechtigt, auf die API zuzugreifen!';
$_['error_stock']      = 'Mit *** markierte Produkte sind nicht verfügbar!';
$_['error_minimum']    = 'Mindestbestellmenge für %s ist %s!';
$_['error_store']      = 'Produkt kann nicht gewählt werden!';
$_['error_required']   = '%s erforderlich!';

