<?php
// Text
$_['text_address']       = 'Erfolg: Zahlungsadresse wurde gesetzt!';
$_['text_method']        = 'Erfolg: Zahlungsmethode wurde gesetzt!';

// Error
$_['error_permission']   = 'Warnung: Sie sind nicht berechtigt, auf die API zuzugreifen!';
$_['error_firstname']    = 'Der Vorname muss zwischen 1 und 32 Zeichen lang sein!';
$_['error_lastname']     = 'Nachname muss zwischen 1 und 32 Zeichen enthalten!';
$_['error_address_1']    = 'Adresse 1 muss zwischen 3 und 128 Zeichen lang sein!';
$_['error_city']         = 'Stadt muss zwischen 3 und 128 Zeichen lang sein!';
$_['error_postcode']     = 'Die Postleitzahl muss für dieses Land zwischen 2 und 10 Zeichen lang sein!';
$_['error_country']      = 'Bitte wählen Sie ein Land!';
$_['error_zone']         = 'Bitte wählen Sie eine Region / einen Bundesstaat!';
$_['error_custom_field'] = '%s  erforderlich!';
$_['error_address']      = 'Warnung: Zahlungsadresse erforderlich!';
$_['error_method']       = 'Achtung: Zahlungsart erforderlich!';
$_['error_no_payment']   = 'Achtung: Es sind keine Zahlungsoptionen verfügbar!';

