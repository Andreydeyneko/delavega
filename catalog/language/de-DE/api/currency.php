<?php
// Text
$_['text_success']     = 'Erfolg: Ihre Währung hat sich geändert!';

// Error
$_['error_permission'] = 'Warnung: Sie sind nicht berechtigt, auf die API zuzugreifen!';
$_['error_currency']   = 'Warnung: Währungscode ist ungültig!';

