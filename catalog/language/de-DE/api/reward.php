<?php
// Text
$_['text_success']     = 'Erfolg: Ihr Bonuspunkte-Rabatt wurde angewendet!';

// Error
$_['error_permission'] = 'Warnung: Sie sind nicht berechtigt, auf die API zuzugreifen!';
$_['error_reward']     = 'Warnung: Bitte geben Sie die Anzahl der zu verwendenden Bonuspunkte ein!';
$_['error_points']     = 'Achtung: Sie haben keine% s Belohnungspunkte!';
$_['error_maximum']    = 'Warnung: kann angewendet werden ist %s!';

