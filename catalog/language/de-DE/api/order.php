<?php
// Text
$_['text_success']           = 'Auftrag wurde aktualisiert';

// Error
$_['error_permission']       = 'Achtung! Zugriff verweigert für API!';
$_['error_customer']         = 'Kundendaten sind erforderlich!';
$_['error_payment_address']  = 'Die Adresse des Zahlungsempfängers ist erforderlich!';
$_['error_payment_method']   = 'НZahlungsart erforderlich!';
$_['error_no_payment']       = 'Achtung: Es gibt keine verfügbaren Zahlungsmethoden!';
$_['error_shipping_address'] = 'Lieferadresse erforderlich!';
$_['error_shipping_method']  = 'Eine Liefermethode ist erforderlich!';
$_['error_no_shipping']      = 'Achtung: Es sind keine Liefermethoden verfügbar!';
$_['error_stock']            = 'Mit *** gekennzeichnete Produkte sind nicht in der benötigten Menge verfügbar oder sind nicht auf Lager!';
$_['error_minimum']          = 'Mindestbestellmenge %s ist %s!';
$_['error_not_found']        = 'Warnung! Auftrag nicht gefunden';

