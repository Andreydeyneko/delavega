<?php
// Text
$_['text_success']     = 'Erfolg: Ihr Geschenkgutschein-Rabatt wurde angewendet!';
$_['text_cart']        = 'Erfolg: Sie haben Ihren Warenkorb geändert!';
$_['text_for']         = '%s Geschenkgutschein für %s';

// Error
$_['error_permission'] = 'Warnung: Sie sind nicht berechtigt, auf die API zuzugreifen!';
$_['error_voucher']    = 'Achtung: Geschenkgutschein ist entweder aufgebraucht!';
$_['error_to_name']    = 'Der Name des Empfängers muss zwischen 1 und 64 Zeichen lang sein!';
$_['error_from_name']  = 'Ihr Name muss zwischen 1 und 64 Zeichen lang sein!';
$_['error_email']      = 'E-Mail-Adresse scheint nicht gültig zu sein!';
$_['error_theme']      = 'Sie müssen ein Thema auswählen!';
$_['error_amount']     = 'Der Betrag muss zwischen %s und %s!';

