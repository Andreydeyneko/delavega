<?php
// Text
$_['text_upload']    = 'Datei erfolgreich hochgeladen!';

// Error
$_['error_filename'] = 'Der Dateiname muss aus 3 bis 64 Zeichen bestehen!';
$_['error_filetype'] = 'Falscher Dateityp!';
$_['error_upload']   = 'Sie müssen die Datei herunterladen!';

