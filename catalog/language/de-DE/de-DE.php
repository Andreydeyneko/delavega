<?php
// Locale
$_['code']                  = 'de';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l, d F Y';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ' ';

// Text
$_['text_home']             = 'Home';
$_['text_yes']              = 'Ja';
$_['text_no']               = 'Nein';
$_['text_none']             = ' --- Nicht ausgewählt --- ';
$_['text_select']           = ' --- Wählen --- ';
$_['text_all_zones']        = 'Alle Zonen';
$_['text_pagination']       = 'Angezeigt von %d bi s% d von %d (insgesamt %d Seiten)';
$_['text_loading']          = 'Laden ...';
$_['text_no_results']       = 'Keine Daten!';
$_['text_more']             = 'Mehr';
$_['text_close']            = 'herunterfahren';
$_['text_load_more']        = 'Mehr laden';

// Buttons
$_['button_address_add']    = 'Adresse hinzufügen';
$_['button_back']           = 'Zurück';
$_['button_continue']       = 'Weiter';
$_['button_home']           = 'Home';
$_['button_cart']           = 'In den Warenkorb';
$_['button_cancel']         = 'Abbrechen';
$_['button_compare']        = 'Zum Vergleich hinzufügen';
$_['button_wishlist']       = 'Zu den Lesezeichen hinzufügen';
$_['button_checkout']       = 'Kasse';
$_['button_confirm']        = 'Auftragsbestätigung';
$_['button_coupon']         = 'Gutschein einlösen';
$_['button_delete']         = 'Löschen';
$_['button_download']       = 'Herunterladen';
$_['button_edit']           = 'Bearbeiten';
$_['button_filter']         = 'Suchen';
$_['button_new_address']    = 'Neue Adresse';
$_['button_change_address'] = 'Bearbeiten Adresse';
$_['button_reviews']        = 'Bewertungen';
$_['button_write']          = 'Schreib eine Bewertung';
$_['button_login']          = 'Einloggen';
$_['button_update']         = 'Aktualisieren';
$_['button_remove']         = 'Löschen';
$_['button_reorder']        = 'Zusätzliche Bestellung';
$_['button_return']         = 'Zurücksenden';
$_['button_shopping']       = 'Einkauf fortsetzen';
$_['button_search']         = 'Suchen';
$_['button_shipping']       = 'Lieferung anwenden';
$_['button_submit']         = 'Übernehmen';
$_['button_guest']          = 'Einen Auftrag ohne Registrierung erteilen';
$_['button_view']           = 'Ansicht';
$_['button_voucher']        = 'Geschenkgutschein anwenden';
$_['button_upload']         = 'Datei herunterladen';
$_['button_reward']         = 'Belohnungspunkte anwenden';
$_['button_quote']          = 'Preise erhalten';
$_['button_list']           = 'Liste';
$_['button_grid']           = 'Gitter';
$_['button_map']            = 'Karte anzeigen';
$_['button_register']       = 'Registrieren';
$_['button_login']          = 'Einloggen';
$_['button_confirm_order'] = 'Kaufen';

// Contact form
$_['text_name'] = 'Name';
$_['text_phone'] = 'Telefon';
$_['text_email'] = 'Email';
$_['text_send'] = 'Senden';

// Popup
$_['text_thanks_title'] = 'Danke!';
$_['text_thanks_contact'] = 'Wir werden Sie mit den unten angegebenen Informationen kontaktieren und alle Details besprechen.';
$_['text_thanks_promo'] = 'A personal promotional code has been sent to your phone number.';

// Error
$_['error_exception']       = 'Fehlercode(%s): %s im %s Strich %s';
$_['error_upload_1']        = 'Warnung: Die Größe der Upload-Datei überschreitet den Wert von upload_max_filesize in php.ini!';
$_['error_upload_2']        = 'Warnung: Die hochgeladene Datei überschreitet den MAX_FILE_SIZE Wert, der in den Einstellungen angegeben wurde!';
$_['error_upload_3']        = 'Warnung: Die heruntergeladenen Dateien wurden nur teilweise hochgeladen!';
$_['error_upload_4']        = 'Warnung: Keine Downloads!';
$_['error_upload_6']        = 'Warnung: Temporärer Ordner!';
$_['error_upload_7']        = 'Warnung: Fehler beim Schreiben!';
$_['error_upload_8']        = 'Achtung: Es ist verboten, eine Datei mit dieser Erweiterung hochzuladen!';
$_['error_upload_999']      = 'Warnung: Unbekannter Fehler!';
$_['error_curl']            = 'CURL: Fehlercode(%s): %s';

/* Когда нужен перевод скриптов, просто добавь код языка */

// Datepicker
$_['datepicker']                    = 'de';

