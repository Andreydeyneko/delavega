<?php
// Heading
$_['heading_title'] = 'Meine Lesezeichen';

// Text
$_['text_account']  = 'Persönliches Büro';
$_['text_instock']  = 'Auf Lager';
$_['text_wishlist'] = 'Lesezeichen (%s)';
$_['text_login']    = 'Sie müssen sich <a href="%s"> anmelden </a> oder <a href="%s"> ein Konto erstellen </a>, um <a href="%s">% s </a> zu speichern zu Ihrer <a href="%s"> Lesezeichenliste </a>!';
$_['text_success']  = 'Sie haben <a href="%s">% s </a> zu <a href="%s"> Lesezeichen </a> hinzugefügt!';
$_['text_remove']   = 'Lesezeichenliste erfolgreich geändert!';
$_['text_empty']    = 'Ihre Lesezeichen sind leer.';
$_['text_empty_wishlist'] = 'Keine vorgestellten Elemente';
$_['text_remove_wishlist'] = 'Aus den Favoriten entfernen';
$_['text_before_remove'] = 'Sind Sie sicher, dass Sie dieses Produkt entfernen möchten?';

// Column
$_['column_image']  = 'Bild';
$_['column_name']   = 'Produktname';
$_['column_model']  = 'Modell';
$_['column_stock']  = 'Verfügbarkeit';
$_['column_price']  = 'Preis pro Stück';
$_['column_action'] = 'Aktion';

$_['button_remove'] = 'Löschen';
$_['button_cancel'] = 'Abbrechen';