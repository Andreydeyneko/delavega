<?php
// Heading
$_['heading_title']                = 'Autorisierung';

// Text
$_['text_account']                 = 'Persönliches Büro';
$_['text_login']                   = 'Autorisierung';
$_['text_new_customer']            = 'Neuer Kunde';
$_['text_register']                = 'Registrierung';
$_['text_register_account']        = 'Das Erstellen eines Kontos hilft Ihnen, schneller zu kaufen. Sie können den Status der Bestellung überwachen und bereits getätigte Bestellungen einsehen. Sie können Bonuspunkte akkumulieren und Rabattcoupons erhalten. <BR> Und für Stammkunden bieten wir ein flexibles Rabattsystem und persönlichen Service. <BR>';
$_['text_returning_customer']      = 'Registrierter Kunde';
$_['text_i_am_returning_customer'] = 'Melden Sie sich bei Mein Konto an';
$_['text_forgotten']               = 'Passwort vergessen?';
$_['text_go_account']              = 'Anmelden';
$_['text_enter_phone']             = 'Geben Sie die Telefonnummer ein, <br> an die das Passwort gesendet werden soll.';
$_['text_register_password']       = 'An die von Ihnen eingegebene Telefonnummer wurde ein Passwort gesendet.';
$_['text_change_phone']            = 'Telefonnummer ändern';
$_['text_resend']                  = 'Senden Sie es erneut';
$_['text_not_send']                = 'Ist das Passwort nicht gekommen?';


// Entry
$_['entry_email']                  = 'E-Mail';
$_['entry_phone']                  = 'Telefon';
$_['entry_password']               = 'Passwort';

$_['button_login_account']         = 'Autorisierung';

// Error
$_['error_login']                  = 'Falsch ausgefüllt die E-Mail und / oder das Passwort!';
$_['error_attempts']               = 'Sie haben die maximale Anzahl von Anmeldeversuchen überschritten. Wiederholen Sie die Autorisierung auf der Website nach 1 Stunde';
$_['error_approved']               = 'Sie müssen Ihr Konto vor der Autorisierung bestätigen.';
$_['error_phone_db']               = 'Dieses Telefon ist nicht registriert!';

