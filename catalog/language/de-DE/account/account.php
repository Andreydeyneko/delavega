<?php
// Heading
$_['heading_title']       = 'Persönliches Büro';

// Text
$_['text_account']        = 'Persönliches Büro';
$_['text_my_account']     = 'Mein Konto';
$_['text_my_orders']      = 'Meine Bestellungen';
$_['text_my_affiliate']   = 'Mein Partnerkonto';
$_['text_my_newsletter']  = 'Abonnement';
$_['text_edit']           = 'Kontaktinformationen ändern';
$_['text_password']       = 'Ändern Sie Ihr Passwort';
$_['text_address']        = 'Ändern Sie meine Adressen';
$_['text_credit_card']    = 'Kreditkarten-Management';
$_['text_wishlist']       = 'Lesezeichen anzeigen';
$_['text_order']          = 'Bestellhistorie';
$_['text_download']       = 'Dateien zum Herunterladen';
$_['text_reward']         = 'Bonuspunkte';
$_['text_return']         = 'Anfragen zurücksenden';
$_['text_transaction']    = 'Transaktionsverlauf';
$_['text_newsletter']     = 'Newsletter abonnieren oder abbestellen';
$_['text_recurring']      = 'Wiederkehrende Zahlungen';
$_['text_transactions']   = 'Transaktionen';
$_['text_affiliate_add']  = 'Registrierung des Partnerkontos';
$_['text_affiliate_edit'] = 'Partnerinformationen ändern';
$_['text_tracking']       = 'Partnerverfolgungscode';
$_['text_not_send_email'] = 'Erhalten Sie keine Mailings';
$_['text_save']           = 'Speichern';
$_['text_no_special']     = 'Für nicht-Werbeartikel.';
$_['text_to_catalog']     = 'Katalogisieren';
$_['text_discount']       = 'Persönlicher Rabatt';

$_['entry_firstname'] = 'Vorname:';
$_['entry_lastname'] = 'Nachname:';
$_['entry_telephone'] = 'Telefon:';
$_['entry_email'] = 'Email:';
$_['entry_shipping'] = 'Versandadresse:';
$_['entry_birthday'] = 'Geburtsdatum:';