<?php
// Heading
$_['heading_title']         = 'Bestellhistorie';

// Text
$_['text_account']          = 'Persönliches Konto';
$_['text_order']            = 'Bestellung';
$_['text_order_detail']     = 'Bestelldetails';
$_['text_invoice_no']       = 'Kontonummer';
$_['text_order_id']         = 'Auftragsnummer';
$_['text_order_number']     = 'Auftragsnummer';
$_['text_date_added']       = 'Bestelldatum';
$_['text_order_price']      = 'Bestellmenge';
$_['text_order_status']     = 'Auftragsstatus';
$_['text_shipping_address'] = 'Lieferadresse';
$_['text_shipping_method']  = 'Versandart';
$_['text_payment_address']  = 'Rechnungsadresse';
$_['text_payment_method']   = 'Zahlungsmethode';
$_['text_comment']          = 'Auftragskommentar';
$_['text_history']          = 'Bestellhistorie';
$_['text_success']          = 'Produkte aus der Bestellung <a href="%s">% s </a> wurden <a href="%s"> erfolgreich in Ihren Einkaufswagen gelegt </a>';
$_['text_empty']            = 'Sie haben keine Einkäufe getätigt!';
$_['text_no_products']      = 'Keine Aufträge';
$_['text_error']            = 'Die angeforderte Bestellung konnte nicht gefunden werden!';
$_['text_table_list']       = 'Um die Tabelle korrekt anzuzeigen, blättern Sie nach rechts';
$_['text_qnt']              = 'Betrag:';
$_['text_qnt_product']      = 'pcs';
$_['text_service_lift']     = 'Das Vorhandensein eines Lastenaufzugs';
$_['text_lift_yes']         = 'Ja';
$_['text_lift_no']          = 'Nein';

// Column
$_['column_order_id']       = 'Bestellnummer';
$_['column_customer']       = 'Kunde';
$_['column_product']        = 'Nummer';
$_['column_name']           = 'Produktname';
$_['column_model']          = 'Modell';
$_['column_quantity']       = 'Nummer';
$_['column_price']          = 'Bestellmenge';
$_['column_total']          = 'Gesamt';
$_['column_action']         = 'Aktion';
$_['column_date_added']     = 'Hinzugefügt';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Auftragskommentar';

// Error
$_['error_reorder']         = '%s es ist momentan nicht verfügbar ...';

