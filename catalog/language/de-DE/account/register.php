<?php
// Heading
$_['heading_title']        = 'Registrierung';

// Text
$_['text_account']         = 'Persönliches Konto';
$_['text_register']        = 'Registrierung';
$_['text_account_already'] = 'Wenn Sie bereits registriert sind, gehen Sie zur <a href="%s"> Anmeldeseite </a>.';
$_['text_your_details']    = 'Stammdaten';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Ihr passwort';
$_['text_agree']           = 'Ich habe <a href="%ss class="agree"> <b>% s </ b> </a> gelesen und stimme den Sicherheitsbestimmungen und der persönlichen Handhabung zu Daten';
$_['text_register_note']   = 'Geben Sie die E-Mail-Adresse und die Telefonnummer ein, an die das Passwort gesendet werden soll.';
$_['text_register_password'] = 'An die eingegebene Telefonnummer wurde ein Passwort gesendet.';
$_['text_change_phone'] = 'Telefonnummer ändern';
$_['text_resend'] = 'Erneut senden';
$_['text_not_send'] = 'Das Passwort ist nicht gekommen?';
$_['text_resend_popup'] = 'Passwort erneut gesendet';

// Entry
$_['entry_customer_group'] = 'Geschäftsrichtung';
$_['entry_firstname']      = 'Name';
$_['entry_lastname']       = 'Nachname';
$_['entry_email']          = 'Email';
$_['entry_telephone']      = 'Telefon';
$_['entry_newsletter']     = 'Newsletter abonnieren';
$_['entry_password']       = 'Passwort von SMS';
$_['entry_confirm']        = 'Passwort bestätigen';

// Error
$_['error_exists']         = 'Diese E-Mail ist bereits registriert!';
$_['error_firstname']      = 'Der Name muss aus 1 bis 32 Zeichen bestehen!';
$_['error_lastname']       = 'Der Nachname muss zwischen 1 und 32 Zeichen lang sein!';
$_['error_email']          = 'E-Mail wurde falsch eingegeben!';
$_['error_telephone']      = 'Das Telefon muss aus 3 bis 32 Ziffern bestehen!';
$_['error_custom_field']   = '%s muss ausgefüllt sein!';
$_['error_password']       = 'Ungültiges passwort!';
$_['error_confirm']        = 'Passwörter und Bestätigungs-Passwort stimmen nicht überein!';
$_['error_agree']          = 'Sie müssen lesen und zustimmen %s!';

