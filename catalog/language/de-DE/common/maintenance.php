<?php
// Heading
$_['heading_title']    = 'Wartungsmodus';

// Text
$_['text_maintenance'] = 'Der Laden vorübergehend geschlossen';
$_['text_message']     = '<h1 style="text-align:center;">Der Laden vorübergehend geschlossen. Wir machen Wartungsarbeiten. <br /> Bald wird der Laden verfügbar sein. Bitte komm später wieder.</h1>';

