<?php
// Text
$_['text_home']          = 'Zuhause';
$_['text_wishlist']      = '%s';
$_['text_shopping_cart'] = 'Warenkorb';
$_['text_category']      = 'Katalog';
$_['text_contant']       = 'Kontaktieren Sie uns';
$_['text_company']       = 'Unternehmen';
$_['text_account']       = 'Persönliches Büro';
$_['text_register']      = 'Registrierung';
$_['text_login']         = 'Autorisierung';
$_['text_order']         = 'Bestellhistorie';
$_['text_transaction']   = 'Transaktionen';
$_['text_download']      = 'Herunterladen';
$_['text_logout']        = 'Beenden';
$_['text_checkout']      = 'Kasse';
$_['text_search']        = 'Suche';
$_['text_all']           = 'Alle ansehen';

