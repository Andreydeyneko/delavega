<?php
// Text
$_['text_items']     = 'Produkte %s (%s)';
$_['text_empty']     = 'Ihr Warenkorb ist leer!';
$_['text_cart']      = 'Zum Warenkorb';
$_['text_checkout']  = 'Bestellung aufgeben';
$_['text_checkout']  = 'Bestellung aufgeben';
$_['text_recurring'] = 'Abrechnungsprofil';
$_['text_cart_quantity'] = 'Betrag:';
$_['text_cart_sht'] = 'pcs';
$_['text_cart_price'] = 'Preis:';
$_['text_continue_shopping'] = 'Einkauf fortsetzen';
$_['text_cart_title'] = 'Produkt wurde dem Warenkorb hinzugefügt';