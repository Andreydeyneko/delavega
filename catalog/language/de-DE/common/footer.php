<?php
// Text
$_['text_information']  = 'Informationen';
$_['text_service']      = 'Unterstützungsdienst';
$_['text_extra']        = 'Erweitert';
$_['text_contact']      = 'Kontakte';
$_['text_return']       = 'Warenrückgabe';
$_['text_sitemap']      = 'Sitemap';
$_['text_manufacturer'] = 'Hersteller';
$_['text_voucher']      = 'Geschenkgutscheine';
$_['text_affiliate']    = 'Partnerprogramm';
$_['text_special']      = 'Anteile';
$_['text_account']      = 'Persönliches Konto';
$_['text_order']        = 'Bestellhistorie';
$_['text_wishlist']     = 'Lesezeichen';
$_['text_newsletter']   = 'Verschicken';
$_['text_getdiscounts']       = 'Get a discount';

$_['text_quality']      = 'Qualitätssicherungsabteilung:';
$_['text_question']     = 'Technische Probleme';
$_['text_room']         = 'Raum zeigen';
$_['text_create']       = 'Eine Site erstellen';
$_['text_powered']      = '&copy; %s %s';
$_['text_subscribe']    = 'Nachrichten abonnieren';
$_['text_subscribe_button']    = 'Abonnieren';
$_['text_enter_account']    = 'Melden Sie sich bei Ihrem Konto an';
$_['text_enter']    = 'EIBLOGGEN';
$_['text_register']    = 'Registrieren';
$_['text_subscribe_success']   = 'Sie haben den Nachrichten abonnieren';
$_['text_password_resend_success'] = 'Passwort erneut gesendet';
$_['text_instruction']    = 'Sie müssen sich <a href="%s" class="popup__link">anmelden </a> oder <a href="%s" class="popup__link">ein Konto erstellen</a>, Elemente zu Favoriten hinzufügen. ';
