<?php
// Heading
$_['heading_title']     = 'Suchen';
$_['heading_search']    = 'Suchergebnisse';
$_['heading_tag']       = 'Nach Tag - ';

// Text
$_['text_search']       = 'Suchergebnisse';
$_['text_search_found'] = 'Produkte, die den Suchkriterien entsprechen';
$_['text_keyword']      = 'Schlüsselwörter';
$_['text_category']     = 'Alle Kategorien';
$_['text_sub_category'] = 'Suche in Unterkategorien';
$_['text_empty']        = 'Es gibt keine Produkte, die den Suchkriterien entsprechen.';
$_['text_quantity']     = 'Betrag:';
$_['text_manufacturer'] = 'Hersteller:';
$_['text_model']        = 'Modell:';
$_['text_points']       = 'Bonuspunkte:';
$_['text_price']        = 'Preis:';
$_['text_tax']          = 'Ohne MwSt:';
$_['text_reviews']      = 'Gesamtzahl der Bewertungen: %s';
$_['text_compare']      = 'Produkte vergleichen (%s)';
$_['text_sort']         = 'Sortieren:';
$_['text_default']      = 'Standard';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Preis (niedrig &gt; hoch)';
$_['text_price_desc']   = 'Preis (hoch &gt; niedrig)';
$_['text_rating_asc']   = 'Bewertung (von niedrig ausgehend)';
$_['text_rating_desc']  = 'Bewertung (beginnend mit hoch)';
$_['text_model_asc']    = 'Modell (A - Z)';
$_['text_model_desc']   = 'Modell (Z - A)';
$_['text_limit']        = 'Zeigen:';

// Entry
$_['entry_search']      = 'Suchen';
$_['text_no_results']   = 'Es wurden keine Ergebnisse für die Abfrage "%s" gefunden. Versuchen Sie es nochmal.';
$_['text_no_results_found']   = 'Auf Anfrage wurde nichts gefunden. Versuchen Sie es nochmal.';
$_['entry_description'] = 'Suche in der Produktbeschreibung';

