<?php
// Heading
$_['heading_title']     = 'Produkte vergleichen';

// Text
$_['text_product']      = 'Beschreibung';
$_['text_name']         = 'Produkt';
$_['text_image']        = 'Bild';
$_['text_price']        = 'Preis';
$_['text_model']        = 'Modell';
$_['text_manufacturer'] = 'Hersteller';
$_['text_availability'] = 'Auf Lager';
$_['text_instock']      = 'Verfügbar auf Lager';
$_['text_rating']       = 'Bewertung';
$_['text_reviews']      = 'Gesamtzahl der Bewertungen: %s';
$_['text_summary']      = 'Kurzbeschreibung';
$_['text_weight']       = 'Gewicht';
$_['text_dimension']    = 'Abmessungen (L x B x H)';
$_['text_compare']      = 'Produkte vergleichen (%s)';
$_['text_success']      = 'Das Produkt <a href="%s">%s</a> wurde zu Ihrer <a href="%s">Vergleichsliste hinzugefügt!</a>!';
$_['text_remove']       = 'Aus Vergleichsliste entfernt';
$_['text_empty']        = 'Sie haben keine Produkte zum Vergleichen ausgewählt.';

