<?php
// Heading
$_['heading_title']     = 'Hersteller';

// Text
$_['text_brand']        = 'Hersteller';
$_['text_index']        = 'Alphabetischer Index:';
$_['text_error']        = 'Hersteller wurde nicht gefunden!';
$_['text_empty']        = 'Es gibt keine Produkte von diesem Hersteller.';
$_['text_quantity']     = 'Betrag:';
$_['text_manufacturer'] = 'Hersteller:';
$_['text_model']        = 'Modell:';
$_['text_points']       = 'Bonuspunkte:';
$_['text_price']        = 'Preis:';
$_['text_tax']          = 'Ohne MwSt:';
$_['text_compare']      = 'Produkte vergleichen (%s)';
$_['text_sort']         = 'Sortieren:';
$_['text_default']      = 'Standard';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Preis (niedrig &gt; hoch)';
$_['text_price_desc']   = 'Preis (hoch &gt; niedrig)';
$_['text_rating_asc']   = 'Bewertung (von niedrig ausgehend)';
$_['text_rating_desc']  = 'Bewertung (beginnend mit hoch)';
$_['text_model_asc']    = 'Modell (A - Z)';
$_['text_model_desc']   = 'Modell (Z - A)';
$_['text_limit']        = 'Zeigen:';

