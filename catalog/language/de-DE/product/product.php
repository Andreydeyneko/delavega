<?php
// Text
$_['text_search']              = 'Suche';
$_['text_brand']               = 'Hersteller';
$_['text_size']                = 'Dimensionen PDF';
$_['text_manufacturer']        = 'Hersteller:';
$_['text_model']               = 'Modell:';
$_['text_reward']              = 'Bonuspunkte:';
$_['text_points']              = 'ЦPreis in Bonuspunkten:';
$_['text_stock']               = 'Verfügbarkeit:';
$_['text_instock']             = 'Auf Lager';
$_['text_tax']                 = 'Ohne MwSt:';
$_['text_discount']            = ' oder mehr: ';
$_['text_option']              = 'Verfügbare Optionen';
$_['text_minimum']             = 'Mindestbestellmenge: %s';
$_['text_reviews']             = '%s Bewertungen';
$_['text_write']               = 'Feedback hinterlassen';
$_['text_login']               = 'Bitte <a href="%s"> anmelden </a> oder <a href="%s"> ein Konto erstellen </a>, bevor Sie eine Rezension schreiben';
$_['text_no_reviews']          = 'Es gibt keine Bewertungen für dieses Produkt.';
$_['text_note']                = '<span style="color:#FF0000;"> Hinweis: </span> HTML-Markup wird nicht unterstützt! Verwenden Sie Nur-Text.';
$_['text_success']             = 'Vielen Dank für Ihr Feedback. Er hat den Administrator angemeldet, um nach Spam zu suchen, und wird demnächst veröffentlicht. ';
$_['text_related']             = 'Ausgewählte Produkte';
$_['text_tags']                = '<i class="fa fa-tags"></i>';
$_['text_error']               = 'Produkt nicht gefunden!';
$_['text_payment_recurring']   = 'Abrechnungsprofil';
$_['text_trial_description']   = 'Gesamt: %s; Periodizität: %d %s; Anzahl der Zahlungen: %d, Next  ';
$_['text_payment_description'] = 'Gesamt: %s; Periodizität:  %d %s; Anzahl der Zahlungen:  %d ';
$_['text_payment_cancel']      = 'Gesamt: %s; Periodizität:  %d %s ; Anzahl der Zahlungen: bis auf Widerruf ';
$_['text_day']                 = 'Tag';
$_['text_week']                = 'Wochen';
$_['text_semi_month']          = 'zwei Wochen';
$_['text_month']               = 'Monat';
$_['text_year']                = 'Jahr';
$_['text_individual_size']     = 'Benutzerdefinierte Größe';
$_['text_final_price']         = 'Gesamt:';
$_['text_also_buy']            = 'Leute kaufen mit diesem Produkt';
$_['text_description_product'] = 'Produktbeschreibung';
$_['text_review_title']        = 'Hinterlasse eine Bewertung';
$_['text_review_send']         = 'Feedback senden';
$_['text_own_size_title']      = 'Kundenauftrag';
$_['text_own_size_text']       = 'Wir werden Sie unter Verwendung der folgenden Informationen kontaktieren, um alle Details zu besprechen.';
$_['text_own_size_send']       = 'Zu senden';
$_['text_video']               = 'Video';
$_['text_3d_model']            = '3D Modell';
$_['text_3d_form_title']       = '3D für Designer herunterladen';
$_['text_mechanism_tip']       = 'Wenn Sie dieses Element auswählen, fügen Sie dem Produkt einen speziellen <strong> Hebemechanismus </strong> hinzu.';
$_['text_download_size']       = 'Größe herunterladen';
$_['text_3d_overview']         = 'Sehen Sie sich 3d review an';
$_['text_video_overview']      = 'Videobewertung des Mechanismus';
$_['text_review_thanks']       = 'Danke für das Feedback';
$_['text_review_after']        = 'Es wird nach Überprüfung des Moderators auf der Website angezeigt.';
$_['text_about_discounts']     = 'Über Rabatte';
$_['text_location']  = 'Unsere Ausstellungsräume';
$_['text_store']     = 'Ausstellungsräume  Kiew';
$_['text_contact']   = 'Feedbackformular';
$_['text_address']   = 'Adresse';
$_['text_telephone'] = 'Telefon';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Abteilung für Qualitätskontrolle';
$_['text_comment']   = 'Zusätzliche Informationen';
$_['text_block_sale'] = 'Verkaufsabteilung';
$_['text_block_marketing'] = 'Marketing und Werbung';
$_['text_socials']   = 'Wir sind in sozialen Netzwerken';
$_['text_where_buy'] = 'Wo kaufen?';
$_['text_about_size'] = 'Erfahren Sie mehr';
$_['button_submit']  = 'Nachricht senden';
$_['text_select_color'] = 'Farbe';
$_['text_select_color_type'] = '-- Stofffarbe auswählen --';

$_['button_download3d']       = 'Datei herunterladen';

// Entry
$_['entry_qty']                = 'Betrag';
$_['entry_name']               = 'Name';
$_['entry_lastname']           = 'Nachname';
$_['entry_company']            = 'Firma';
$_['entry_city']               = 'Stadt';
$_['entry_site']               = 'Webseite';
$_['entry_email']              = 'Email';
$_['entry_review']             = 'Dein Feedback';
$_['entry_phone']              = 'Telefon';
$_['entry_rating']             = 'Bewertung:';
$_['entry_good']               = 'Gut';
$_['entry_bad']                = 'Schlecht';

// Tabs
$_['tab_description']          = 'Beschreibung';
$_['tab_attribute']            = 'Eigenschaften';
$_['tab_review']               = 'Produktbewertungen';

// Error
$_['error_name']               = 'Der Name muss aus 3 bis 25 Zeichen bestehen!';
$_['error_text']               = 'Der Antworttext muss zwischen 25 und 1000 Zeichen lang sein!';
$_['error_rating']             = 'Bitte wählen Sie eine Bewertung!';

