<?php
class ControllerInformationProject extends Controller {
    public function index() {

        $this->load->language('information/project');
        $this->load->model('catalog/project');
        $this->load->model('tool/image');

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
            $data['limit'] = (int)$this->request->get['limit'];
        } else {
            $limit = 6;
            $data['limit'] = 6;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/project')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $project_page_info = $this->model_catalog_project->getProjectPageDescriptions();

        if ($project_page_info) {
            $this->document->setTitle($project_page_info['meta_title']);
            $this->document->setDescription($project_page_info['meta_description']);
            $this->document->setKeywords($project_page_info['meta_keyword']);
        }

        $data['breadcrumbs_total'] = count($data['breadcrumbs']);

        $data['total_projects'] = $this->model_catalog_project->getTotalProjects();

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('information/projects', $data));
    }

    public function getProjectsAjax() {
        $this->load->language('information/project');
        $this->load->model('catalog/project');
        $this->load->model('tool/image');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
            $data['limit'] = (int)$this->request->get['limit'];
        } else {
            $limit = 6;
            $data['limit'] = 6;
        }

        $data['gallerys'] = array();

        $data['projects'] = array();

        $filter_data = array(
            'start' => ($page - 1) * $limit,
            'limit' => 100,
        );

        $data['total_projects'] = $this->model_catalog_project->getTotalProjects();
        $all_projects = $this->model_catalog_project->getProjects($filter_data);

        if ($all_projects) {

            foreach ($all_projects as $project) {

                $data['models'] = array();
                $data['images'] = array();

                $images = $this->model_catalog_project->getProjectImages($project['project_id']);

                if ($images) {
                    foreach ($images as $result) {
                        if (isset($result['image']) && is_file(DIR_IMAGE . $result['image'])) {
                            $thumb = $this->model_tool_image->cropsize($result['image'], 510, 360);
                            $popup = HTTPS_SERVER . 'image/' . $result['image'];
                        } else {
                            $thumb = $this->model_tool_image->cropsize("placeholder.png", 510, 360);
                            $popup = $this->model_tool_image->cropsize("placeholder.png", 510, 360);
                        }
                        $data['images'][] = array(
                            'title' => $result['title'],
                            'thumb' => $thumb,
                            'popup' => $popup,
                        );
                    }
                }

                $models = $this->model_catalog_project->getProductRelated($project['project_id']);

                foreach ($models as $result) {
                    $filter = array(
                        'product' => $result,
                        'width'   => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'),
                        'height'  => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height')
                    );

                    $data['models'][] = $this->product->getProduct($filter);
                }

                $data['projects'][] = array(
                    'id'          => $project['project_id'],
                    'name'        => $project['title'],
                    'designer'    => $project['designer'],
                    'models'      => $data['models'],
                    'images'      => $data['images']
                );
            }

            $template = $this->load->view('information/projects_ajax', $data);

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($template));
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/project')
            );
            $data['breadcrumbs_total'] = count($data['breadcrumbs']);

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($this->load->view('error/not_found', $data)));
        }
    }
}