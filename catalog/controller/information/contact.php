<?php
class ControllerInformationContact extends Controller {
    private $error = array();

    public function index() {

        $this->load->language('information/contact');

        //$this->document->setTitle($this->language->get('heading_title'));
        $this->document->setTitle($this->config->get('config_contacts_title')[$this->config->get('config_language_id')]);
        $this->document->setDescription($this->config->get('config_contacts_keywords')[$this->config->get('config_language_id')]);

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/contact')
        );

        $data['button_submit'] = $this->language->get('button_submit');

        // Socials
        $data['pinterest'] = $this->config->get('config_pint');
        $data['facebook'] = $this->config->get('config_fb');
        $data['instagram'] = $this->config->get('config_inst');

        $data['action'] = $this->url->link('information/contact', '', true);

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), 1843, 600);
        } else {
            $data['image'] = false;
        }


        $data['store'] = $this->config->get('config_name');
        $data['quality'] = $this->config->get('config_quality');
        $data['address'] = $this->config->get('config_kiev_address')[$this->config->get('config_language_id')];
        $data['phone'] = $this->config->get('config_kiev_phone');
        $data['phone1'] = $this->config->get('config_telephone2');
        $data['email'] = $this->config->get('config_kiev_email');
        $data['wortime'] = $this->config->get('config_kiev_worktime')[$this->config->get('config_language_id')];
        $data['sale_phone'] = $this->config->get('config_kiev_sale_phone');
        $data['sale_email'] = $this->config->get('config_kiev_sale_email');
        $data['sale_viber'] = $this->config->get('config_kiev_sale_viber');
        $data['sale_telegram'] = $this->config->get('config_kiev_sale_telegram');
        $data['sale_whatsapp'] = $this->config->get('config_kiev_sale_whatsapp');
        $data['design_phone'] = $this->config->get('config_kiev_design_phone');
        $data['design_email'] = $this->config->get('config_kiev_design_email');
        $data['design_viber'] = $this->config->get('config_kiev_design_viber');
        $data['design_telegram'] = $this->config->get('config_kiev_design_telegram');
        $data['design_whatsapp'] = $this->config->get('config_kiev_design_whatsapp');
        $data['suppliers_phone'] = $this->config->get('config_kiev_suppliers_phone');
        $data['suppliers_email'] = $this->config->get('config_kiev_suppliers_email');
        $data['suppliers_viber'] = $this->config->get('config_kiev_suppliers_viber');
        $data['suppliers_telegram'] = $this->config->get('config_kiev_suppliers_telegram');
        $data['suppliers_whatsapp'] = $this->config->get('config_kiev_suppliers_whatsapp');
        $data['marketing_phone'] = $this->config->get('config_kiev_marketing_phone');
        $data['marketing_email'] = $this->config->get('config_kiev_marketing_email');
        $data['marketing_viber'] = $this->config->get('config_kiev_marketing_viber');
        $data['marketing_telegram'] = $this->config->get('config_kiev_marketing_telegram');
        $data['marketing_whatsapp'] = $this->config->get('config_kiev_marketing_whatsapp');
        $data['control_viber'] = $this->config->get('config_kiev_control_viber');
        $data['control_telegram'] = $this->config->get('config_kiev_control_telegram');
        $data['control_whatsapp'] = $this->config->get('config_kiev_control_whatsapp');
        $data['control_email'] = $this->config->get('config_kiev_control_email');
        $data['control_phone'] = $this->config->get('config_kiev_control_phone');
        $data['control_phone_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_control_phone'));
        $data['suppliers_phone_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_suppliers_phone'));
        $data['marketing_phone_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_marketing_phone'));
        $data['design_phone_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_design_phone'));
        $data['sale_phone_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_sale_phone'));

        $data['control_whatsapp_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_control_whatsapp'));
        $data['suppliers_whatsapp_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_suppliers_whatsapp'));
        $data['marketing_whatsapp_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_marketing_whatsapp'));
        $data['design_whatsapp_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_design_whatsapp'));
        $data['sale_whatsapp_format'] =  preg_replace('/[^0-9.]+/', '', $this->config->get('config_kiev_sale_whatsapp'));

        $data['locations'] = array();

        $this->load->model('localisation/location');
        $this->load->model('setting/setting');

        $all_locations = $this->model_localisation_location->getLocations([]);

        foreach($all_locations as $location_info) {

            if ($location_info) {
                if ($location_info['image']) {
                    $image = $this->model_tool_image->resize($location_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_height'));
                } else {
                    $image = false;
                }
                $for_maps = '<div class="for_maps" style="min-width: 200px;position: relative;"><svg width="36" height="49" style="position: absolute;    fill: #ab7225;" ><use xlink:href="/front/dist/images/nav.svg#for_maps" /></svg><p class="contacts-map__city" style="margin-left: 50px">'.addcslashes($location_info['name']).'</p><p style="margin-left: 50px">'.addcslashes($location_info['comment']).'</p><p style="margin-left: 50px">'.addcslashes($location_info['address']).'</p><p style="margin-left: 50px"><a class="contacts-map__note" href="tel:'.$location_info['telephone'].'">'.$location_info['telephone'].'</a></p><p  style="margin-left: 50px"><a class="contacts-map__note" href="tel:'.$location_info['telephone2'].'">'.$location_info['telephone2'].'</a></p><p class="contacts-map__note" style="margin-left: 50px"><a class="contacts-map__note" href="mailto:'.$location_info['email'].'">'.$location_info['email'].'</a></p></div>';

                $data['locations'][] = array(
                    'for_maps' => $for_maps,
                    'location_id' => $location_info['location_id'],
                    'name'        => $location_info['name'],
                    'address'     => $location_info['address'],
                    'geocode'     => $location_info['geocode'],
                    'phone'       => $location_info['telephone'],
                    'phone2'      => $location_info['telephone2'],
                    'email'       => $location_info['email'],
                    'fax'         => $location_info['fax'],
                    'image'       => $image,
                    'open'        => $location_info['open'],
                    'comment'     => $location_info['comment']
                );
            }
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('information/contact', $data));
    }
}