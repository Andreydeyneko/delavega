<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {
		    $order_id = $this->session->data['order_id'];

            $data['text_message'] = sprintf($this->language->get('text_customer'), $order_id, $this->url->link('account/account', '', true));

			$this->cart->clear();

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['firstname']);
			unset($this->session->data['email']);
			unset($this->session->data['telephone']);
			unset($this->session->data['delivery_address']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		} else {
            $order_id = '';
        }

		$this->document->setTitle($this->language->get('heading_title'));

        $data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $order_id);

		$data['continue'] = $this->url->link('common/home');
		$data['account'] = $this->url->link('account/account');

		$data['pinterest'] = $this->config->get('config_pint');
		$data['facebook'] = $this->config->get('config_fb');
		$data['instagram'] = $this->config->get('config_inst');

		if (isset($this->session->data['language']) && $this->session->data['language'] == 'ua-uk') {
            $data['news'] = 'news-ua';
        } elseif (isset($this->session->data['language']) && $this->session->data['language'] == 'en-gb') {
            $data['news'] = 'news-en';
        } elseif (isset($this->session->data['language']) && $this->session->data['language'] == 'de-DE') {
            $data['news'] = 'news-de';
        } else {
            $data['news'] = 'news';
        }
        $this->load->model('checkout/order');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $data_ads = '';
        $data_ads_pr = array();
        $data_products = array();
        $order = $this->model_checkout_order->getOrder($order_id);
        $coupon = $this->model_checkout_order->getOrderCoupon($order_id);
        foreach ($this->model_checkout_order->getOrderProducts($order_id) as $product){
            $variant = '';
            foreach ($this->model_checkout_order->getOrderOptions($order_id, $product['order_product_id']) as $option){
                if ($option["option_id"] == "16" ){
                    $variant = $option["value"];
                }
            }
            $category='';
            foreach ($this->model_catalog_product->getCategories($product['product_id']) as $categorye){
                $category .= $this->model_catalog_category->getCategory($categorye['category_id'])['name'].'/';
            }
            $data_ads .= "'".$product['product_id']."',";
            $data_ads_pr[] = ($coupon)?($coupon['type']=='P')?$product['price'] - ($product['price']/100 * $coupon['discount']):$product['price'] - $coupon['discount'] : $product['price'];

            $pixel['ids'][] = $product['product_id'];

            $data_products[] = array(
                'name'=> $product['name'],
                'id'=> $product['product_id'],
                'price'=> ($coupon)?($coupon['type']=='P')?$product['price'] - ($product['price']/100 * $coupon['discount']):$product['price'] - $coupon['discount'] : $product['price'],
                'brand'=> 'Delavega',
                'category'=> substr($category, 0, -1),
                'variant'=> $variant,
                'quantity'=> $product['quantity'],
                'coupon'=> ($coupon)?$coupon["name"]:''
            );

        }

        $datalayer = "<script>
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'currencyCode': '".$order['currency_code']."',
                    'purchase': {
                        'actionField': {
                            'id': '".$order['order_id']."',
      	                    'revenue': '".$order['total']."',
                        },
                        'products': '".json_encode($data_products,JSON_UNESCAPED_UNICODE )."'
                    }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Purchase',
                'gtm-ee-event-non-interaction': 'True',
            });
            </script>";

        $this->document->setdataLayer($datalayer);
        $ads = "<script>
                        var google_tag_params = {
                        dynx_itemid: [".trim($data_ads,',')."], // id товара
                        dynx_pagetype: 'conversion',
                        dynx_totalvalue:".$order['total']." // цена товара
                        };
                    </script>";

        $this->document->setAds($ads);

        if (!empty($order_id)) {
            $data['questionScript'] = "<script>
                            window.renderOptIn = function() {
                                window.gapi.load('surveyoptin', function() {
                                    window.gapi.surveyoptin.render(
                                        {
                                            'merchant_id': 121413698,
                                            'order_id': '" . $order['order_id'] . "',
                                            'email': '" . $order['email'] . "',
                                            'delivery_country': 'ua-uk',
                                            'estimated_delivery_date': '" . date('Y-m-d', strtotime('+5 day')) . "',
                                        });
                                });
                            }
                        </script>
                        <!-- НАЧАЛО кода языка опроса -->
                        <script>
                            window.___gcfg = {
                                lang: '" . $this->session->data['language'] . "'
                            };
                        </script>";
            $data['pixel'] = "<script>
                          fbq('track', 'Purchase', {
                            value: '" . $order['total'] ."',
                            currency: 'UAH',
                            content_ids: '". json_encode($pixel['ids'],JSON_UNESCAPED_UNICODE ) ."',
                            content_type: 'product',
                          
                          });
                        </script>
                        <!-- КОНЕЦ кода языка опроса -->";
        }

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}