<?php
class ControllerCheckoutConfirmst1ch extends Controller {
    public function index() {
        $redirect = '';

        // Validate if payment method has been set.
        if (!isset($this->session->data['payment_method'])) {
            $redirect = $this->url->link('checkout/step1checkout', '', true);
        }

        if (!$redirect) {
            $data['payment'] = $this->load->controller('extension/payment/' . $this->session->data['payment_method']['code']);
        } else {
            $data['redirect'] = $redirect;
        }

        $this->response->setOutput($this->load->view('checkout/confirmst1ch', $data));
    }
}
