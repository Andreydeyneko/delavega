<?php

class ControllerCheckoutStep1checkout extends Controller
{

    public function index()
    {
        // Validate cart has products and has stock.
        if (!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) {
            $this->response->redirect($this->url->link('checkout/cart'));
        }

        $this->load->language('checkout/step1checkout');
        $this->load->model('checkout/step1checkout');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('checkout/step1checkout', '', true)
        );

        $data['breadcrumbs_total'] = count($data['breadcrumbs']);
        $data['text_accept'] = sprintf($this->language->get('text_accept'), $this->url->link('information/information', 'information_id=12', true));

        //Данные для первого шага
        if ($this->customer->isLogged()) {
            $data['firstname'] = $this->customer->getFirstName();
        } else if (isset($this->session->data['firstname'])) {
            $data['firstname'] = $this->session->data['firstname'];
        } else {
            $data['firstname'] = '';
        }

        if ($this->customer->isLogged()) {
            $data['lastname'] = $this->customer->getLastName();
        } else if (isset($this->session->data['lastname'])) {
            $data['lastname'] = $this->session->data['lastname'];
        } else {
            $data['lastname'] = '';
        }

        if ($this->customer->isLogged()) {
            $data['telephone'] = $this->customer->getTelephone();
        } else if (isset($this->session->data['telephone'])) {
            $data['telephone'] = $this->session->data['telephone'];
        } else {
            $data['telephone'] = '';
        }

        if ($this->customer->isLogged()) {
            $data['email'] = $this->customer->getEmail();
        } else if (isset($this->session->data['email'])) {
            $data['email'] = $this->session->data['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->session->data['accept'])) {
            $data['accept'] = $this->session->data['accept'];
        } else {
            $data['accept'] = 1;
        }
        if (isset($this->session->data['service_lift'])) {
            $data['service_lift'] = $this->session->data['service_lift'];
        } else {
            $data['service_lift'] = 0;
        }

        $data['zone_id'] = 3491;

        if (isset($this->session->data['delivery_address'])) {
            $data['delivery_address'] = $this->session->data['delivery_address'];
        } else if ($this->customer->isLogged()) {
            $data['delivery_address'] = $this->customer->getDeliveryAddress();
        } else {
            $data['delivery_address'] = '';
        }
        $data_ads = '';
        $data_ads_total = '';
        foreach ($this->cart->getProducts() as $product) {
            $data_ads .= "'".$product['product_id']."',";
            $data_ads_total .= $product['quantity'] * $product['price'] .',' ;
        }
        $ads = "<script>
                        var google_tag_params = {
                        dynx_itemid: [".trim($data_ads,',')."], // id товара
                        dynx_pagetype: 'conversionintent',
                        dynx_totalvalue: ".trim($data_ads_total,',')." // цена товара
                        };
                    </script>";

        $this->document->setAds($ads);
        //Получим перечень областей для заданного региона
        $data['country_id'] = 220; /* Захардкодили Украину */
        $data['zones'] = $this->model_checkout_step1checkout->getZones($data['country_id']);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('checkout/step1checkout', $data));
    }

    public function getCartProducts()
    {

        $this->load->model('tool/image');
        $this->load->model('tool/upload');
        $this->load->language('checkout/step1checkout');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $data = array();
        $data_products = array();
        $my_total = 0;
        $data_ads_total = 0;
        $data_ads = array();
        foreach ($this->cart->getProducts() as $product) {

            $variant = '';

            $relateds = $this->model_catalog_product->getProductRelated($product["product_id"]);

            foreach ($relateds as $related) {
                $filter = array(
                    'product' => $related,
                    'width' => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'),
                    'height' => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height')
                );

                if ($this->product->getProduct($filter)) {
                    $data['related'][] = $this->product->getProduct($filter);
                }

            }


            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], 480, 342);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 480, 342);
            }
            $option_data = array();

            $product_price = $product['price'];
            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option['price']) {
                    if ($option['price_prefix'] != 'd' && $option['price_prefix'] != 'u') {
                        $price = $this->currency->format($this->tax->calculate($option['price'], $product['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                    } else {
                        $price = (int)$option['price'] . ' %';
                    }
                } else {
                    $price = false;
                }
                if ($option["option_id"] == "16" ){
                    $variant = $option["value"];
                }
                $option_data[] = array(
                    'id' => $option['option_id'],
                    'name' => $option['name'],
                    'price_prefix' => $option['price_prefix'],
                    'value' => $value,
                    'price' => $price,
                    'type' => $option['type']
                );
            }
            $my_total += $product_price * $product['quantity'];
            // Display prices
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $unit_price = $this->tax->calculate($product_price, $product['tax_class_id'], $this->config->get('config_tax'));

                $price = $this->currency->format($unit_price, $this->session->data['currency']);
                $total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
            } else {
                $price = false;
                $total = false;
            }
            $data['products'][] = array(
                'cart_id' => $product['cart_id'],
                'thumb' => $image,
                'name' => $product['name'],
                'model' => $product['model'],
                'option' => $option_data,
                'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
                'quantity' => $product['quantity'],
                'price' => $price,
                'total' => $total,
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );



            $category='';
            foreach ($this->model_catalog_product->getCategories($product['product_id']) as $categorye){
                $category .= $this->model_catalog_category->getCategory($categorye['category_id'])['name'].'/';
            }

            $data_products[] = array(
                'name'=> $product['name'],
                'id'=> $product['product_id'],
                'price'=> $product['price'],
                'brand'=> 'Delavega',
                'category'=> substr($category, 0, -1),
                'variant'=> $variant,
                'quantity'=> $product['quantity']
            );

            $pixel['ids'][] = $product['product_id'];

        }
        $order_data = array();

        $data['slpages'] = round(count($data['related']) / 3)-1;

        $totals = array();
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = array(
            'totals' => &$totals,
            'taxes' => &$taxes,
            'total' => &$total
        );

        $this->load->model('setting/extension');

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {

            if ($this->config->get('total_' . $result['code'] . '_status')) {
                $this->load->model('extension/total/' . $result['code']);

                // We have to put the totals in an array so that they pass by reference.
                $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
            }
        }

        $sort_order = array();

        foreach ($totals as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $totals);

        foreach ($totals as $total) {
            if($total['code'] == 'total'){
                $pixel['total'] = $total['value'];
            }
            $data['totals'][] = array(
                'title' => $total['title'],
                'code' => $total['code'],
                'text' => ($total['code'] != 'procent') ? $this->currency->format($total['value'], $this->session->data['currency']) : (int)$total['value'] . '%'
            );
        }

        $data['datalayer'] = "<script>
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'currencyCode': '". $this->session->data['currency']."',
                    'checkout': {
                        'actionField': {'step': 1},
                        'products': '".json_encode($data_products,JSON_UNESCAPED_UNICODE )."'
                    }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Checkout',
                'gtm-ee-event-non-interaction':'False',
            });
            </script>";

        $data['pixel'] = "<script>
            fbq('track', 'InitiateCheckout', {
                value: '".$pixel['total']."',
                currency: 'UAH',
                content_ids: '" . json_encode($pixel['ids'],JSON_UNESCAPED_UNICODE ) ."',
                content_type: 'product',
            });
            </script>";

        $this->response->setOutput($this->load->view('checkout/st1chcart', $data));
    }

    public function updateCheckoutInfo()
    {

        $json = array();
        $order_data = array();
        $this->load->model('checkout/step1checkout');
        $this->load->model('setting/extension');
        $this->load->model('account/customer');
        $this->load->model('checkout/order');

        if (!isset($json['error'])) {

            $totals = array();
            $taxes = $this->cart->getTaxes();
            $total = 0;

            // Because __call can not keep var references so we put them into an array.
            $total_data = array(
                'totals' => &$totals,
                'taxes' => &$taxes,
                'total' => &$total
            );

            $sort_order = array();

            $results = $this->model_setting_extension->getExtensions('total');
            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get('total_' . $result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);
                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = array();
            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);

            $order_data['totals'] = $totals;
            $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            $order_data['store_id'] = $this->config->get('config_store_id');
            $order_data['store_name'] = $this->config->get('config_name');
            $order_data['affiliate_id'] = 0;
            $order_data['commission'] = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking'] = '';
            if ($order_data['store_id']) {
                $order_data['store_url'] = $this->config->get('config_url');
            } else {
                if ($this->request->server['HTTPS']) {
                    $order_data['store_url'] = HTTPS_SERVER;
                } else {
                    $order_data['store_url'] = HTTP_SERVER;
                }
            }

            if ($this->customer->isLogged()) {
                $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
                $order_data['customer_id'] = $this->customer->getId();
                $order_data['customer_group_id'] = $customer_info['customer_group_id'];
            } else {
                $order_data['customer_id'] = 0;
                $order_data['customer_group_id'] = 1;
            }

            /* Получим Название региона по id */
            $zone_info = $this->model_checkout_step1checkout->getZoneById($this->request->post['zone_id']);
            $zone_name = $zone_info['name'];
            $zone_code = $zone_info['code'];
            $country_info = $this->model_checkout_step1checkout->getCountryNameById($this->request->post['country_id']);
            $country_name = $country_info['name'];
            $iso_code_2 = $country_info['iso_code_2'];
            $iso_code_3 = $country_info['iso_code_3'];

            $order_data['firstname'] = $this->request->post['firstname'];
            $order_data['lastname'] = '';
            $order_data['email'] = $this->request->post['email'];
            $order_data['telephone'] = $this->request->post['telephone'];
            $order_data['service_lift'] = $this->request->post['service_lift'];
            $order_data['delivery_address'] = $this->request->post['delivery_address'];
            $order_data['comment'] = $this->request->post['message'];

            $order_data['payment_firstname'] = $this->request->post['firstname'];
            $order_data['payment_lastname'] = '';
            $order_data['payment_company'] = '';
            $order_data['payment_address_1'] = '';
            $order_data['payment_address_2'] = '';
            $order_data['payment_city'] = '';
            $order_data['payment_postcode'] = '';
            $order_data['payment_zone'] = $zone_name;
            $order_data['payment_zone_id'] = $this->request->post['zone_id'];
            $order_data['payment_country'] = $country_name;
            $order_data['payment_country_id'] = $this->request->post['country_id'];
            $order_data['payment_address_format'] = '';
            $order_data['payment_custom_field'] = array();
            $order_data['payment_method'] = (isset($this->session->data["payment_method"]["title"])) ? $this->session->data["payment_method"]["title"] : '';//$this->getPaymentMethodName($this->request->post['payment_method']);
            $order_data['payment_code'] = $this->request->post['payment_method'];

            $this->session->data['payment_address'] = array(
                'firstname' => $this->request->post['firstname'],
                'lastname' => '',
                'company' => '',
                'address_1' => '',
                'address_2' => '',
                'postcode' => '',
                'city' => '',
                'zone_id' => $this->request->post['zone_id'],
                'zone' => $zone_name,
                'zone_code' => $zone_code,
                'country_id' => $this->request->post['country_id'],
                'country' => $country_name,
                'iso_code_2' => $iso_code_2,
                'iso_code_3' => $iso_code_3,
                'address_format' => '',
                'custom_field' => array()
            );

            $order_data['shipping_company'] = '';
            $order_data['shipping_address_1'] = '';
            $order_data['shipping_address_2'] = '';
            $order_data['shipping_city'] = '';
            $order_data['shipping_postcode'] = '';
            $order_data['shipping_address_format'] = '';
            $order_data['shipping_custom_field'] = array();
            $order_data['shipping_method'] = (isset($this->session->data["payment_method"]["title"])) ? $this->session->data["shipping_method"]["title"] : '';//$this->getShippingMethodName($this->request->post['shipping_method']);
            $order_data['shipping_code'] = $this->request->post['shipping_method'];
            if ($this->cart->hasShipping()) {
                $order_data['shipping_firstname'] = $this->request->post['firstname'];
                $order_data['shipping_lastname'] = '';
                $order_data['shipping_zone'] = $zone_name;
                $order_data['shipping_zone_id'] = $this->request->post['zone_id'];
                $order_data['shipping_country'] = $country_name;
                $order_data['shipping_country_id'] = $this->request->post['country_id'];
            } else {
                $order_data['shipping_firstname'] = '';
                $order_data['shipping_lastname'] = '';
                $order_data['shipping_zone'] = '';
                $order_data['shipping_zone_id'] = '';
                $order_data['shipping_country'] = '';
                $order_data['shipping_address_1'] = '';
            }
            $order_data['shipping_address_1'] = $this->request->post['delivery_address'];

            $this->session->data['shipping_address'] = array(
                'firstname' => $this->request->post['firstname'],
                'lastname' => '',
                'company' => '',
                'address_1' => $this->request->post['delivery_address'],
                'address_2' => '',
                'postcode' => '',
                'city' => '',
                'zone_id' => $this->request->post['zone_id'],
                'zone' => $zone_name,
                'zone_code' => $zone_code,
                'country_id' => $this->request->post['country_id'],
                'country' => $country_name,
                'iso_code_2' => $iso_code_2,
                'iso_code_3' => $iso_code_3,
                'address_format' => '',
                'custom_field' => array()
            );

            $order_data['products'] = array();
            foreach ($this->cart->getProducts() as $product) {

                $option_data = array();
                foreach ($product['option'] as $option) {
                    if ((float)$option['price'] != 0) {
                        if ($option['price_prefix'] == 'd') {
                            $option['value'] = $option['value'] . ' (-' . (float)$option['price'] . '%)';
                        } elseif ($option['price_prefix'] == 'u') {
                            $option['value'] = $option['value'] . ' (+' . (float)$option['price'] . '%)';
                        } else {
                            $option['value'] = $option['value'] . ' (' . $option['price_prefix'] . $this->currency->format($option['price'], $this->session->data['currency']) . ')';
                        }

                    }
                    $option_data[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'option_id' => $option['option_id'],
                        'option_value_id' => $option['option_value_id'],
                        'name' => $option['name'],
                        'value' => $option['value'],
                        'type' => $option['type']
                    );
                }
                $order_data['products'][] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'download' => $product['download'],
                    'quantity' => $product['quantity'],
                    'subtract' => $product['subtract'],
                    'price' => $product['price'],
                    'total' => $product['total'],
                    'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward' => $product['reward']
                );
            }

            // Gift Voucher
            $order_data['vouchers'] = array();
            if (isset($this->session->data['vouchers']) && !empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $voucher) {
                    $order_data['vouchers'][] = array(
                        'description' => $voucher['description'],
                        'code' => token(10),
                        'to_name' => $voucher['to_name'],
                        'to_email' => $voucher['to_email'],
                        'from_name' => $voucher['from_name'],
                        'from_email' => $voucher['from_email'],
                        'voucher_theme_id' => $voucher['voucher_theme_id'],
                        'message' => $voucher['message'],
                        'amount' => $voucher['amount']
                    );
                }
            }

            $order_data['total'] = $total_data['total'];
            $method_data = array();

            $this->load->model('setting/extension');

            $results = $this->model_setting_extension->getExtensions('shipping');

            foreach ($results as $result) {
                if ($this->config->get('shipping_' . $result['code'] . '_status')) {
                    $this->load->model('extension/shipping/' . $result['code']);

                    $quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

                    if ($quote) {
                        $method_data[$result['code']] = array(
                            'title' => $quote['title'],
                            'quote' => $quote['quote'],
                            'sort_order' => $quote['sort_order'],
                            'error' => $quote['error']
                        );
                    }
                }
            }

            $sort_order = array();

            foreach ($method_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_data);

            $this->session->data['shipping_methods'] = $method_data;

            $method_payment_data = array();

            $this->load->model('setting/extension');

            $results = $this->model_setting_extension->getExtensions('payment');

            $recurring = $this->cart->hasRecurringProducts();

            foreach ($results as $result) {
                if ($this->config->get('payment_' . $result['code'] . '_status')) {
                    $this->load->model('extension/payment/' . $result['code']);

                    $method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], 0);

                    if ($method) {
                        if ($recurring) {
                            if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
                                $method_payment_data[$result['code']] = $method;
                            }
                        } else {
                            $method_payment_data[$result['code']] = $method;
                        }
                    }
                }
            }

            $sort_order = array();

            foreach ($method_payment_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_payment_data);

            $this->session->data['payment_methods'] = $method_payment_data;

            $order_data['language_id'] = $this->config->get('config_language_id');
            $order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
            $order_data['currency_code'] = $this->session->data['currency'];
            $order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
            $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $order_data['forwarded_ip'] = '';
            }
            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $order_data['user_agent'] = '';
            }
            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $order_data['accept_language'] = '';
            }
            $order_data['custom_field'] = array();

            $this->session->data['zone_id'] = $this->request->post['zone_id'];
            $this->session->data['country_id'] = $this->request->post['country_id'];
            $this->session->data['customer_id'] = $order_data['customer_id'];
            $this->session->data['customer_group_id'] = $order_data['customer_group_id'];
            $this->session->data['firstname'] = $order_data['firstname'];
            $this->session->data['email'] = $order_data['email'];
            $this->session->data['telephone'] = $order_data['telephone'];
            $this->session->data['service_lift'] = $order_data['service_lift'];
            $this->session->data['delivery_address'] = $order_data['delivery_address'];

            if (!isset($this->session->data['order_id'])) {
                $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
            } else {
                $this->model_checkout_order->editOrder($this->session->data['order_id'], $order_data);
            }

            $json['success'] = 'success';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

        //this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
    }

    public function getPaymentMethodName($code)
    {
        switch ($code) {
            case 'cheque':
                return 'Наличными в офисе (Киев)';
                break;
            case 'bank_transfer':
                return 'На расчетный счёт ФЛП';
                break;
            case 'liqpay':
                return 'Картой On-line';
                break;
            case 'cod':
                return 'На карту Приват Банка';
                break;
            default:
                return $code;
        }
    }

    public function getShippingMethodName($code)
    {

        switch ($code) {
            case 'pickup.pickup':
                return 'Собственная Служба доставки по Киеву';
                break;
            case 'flat.flat':
                return 'Любой удобной службой доставки по Украине';
                break;
            default:
                return $code;
        }
    }

    // не только валидируем, но и создаем часть массива для заказа
    public function validateFields()
    {
        $json = array();
        $json['error'] = false;

        $this->load->model('checkout/step1checkout');
        $this->load->model('setting/extension');
        $this->load->model('account/customer');
        $this->load->model('checkout/order');

        $json['success'] = false;

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 2) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $json['error']['firstname'] = 'error';
        }

        if ((utf8_strlen(trim($this->request->post['delivery_address'])) < 3) || (utf8_strlen(trim($this->request->post['delivery_address'])) > 100)) {
            $json['error']['delivery_address'] = 'error';
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $json['error']['email'] = 'error';
        }

        if (strlen(preg_replace('/[^0-9]/', '', $this->request->post['telephone'])) < 12) {
            $json['error']['telephone'] = 'error';
        }

        if (!$this->request->post['zone_id']) {
            $json['error']['zone_id'] = 'error';
        }
        if (!$this->request->post['country_id']) {
            $json['error']['country_id'] = 'error';
        }

        if (!$json['error']) {
            $json['success'] = true;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function updateOrder()
    {

        $json = array();
        $this->load->model('checkout/step1checkout');

        if (isset($this->session->data['order_id']) && $this->session->data['order_id']) {
            $this->model_checkout_step1checkout->editOrderMethods($this->session->data);
        } else {
            $json['redirect'] = $this->url->link('checkout/step1checkout', '', true);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function saveMessage()
    {

        $json = array();
        $this->load->model('checkout/step1checkout');

        if (isset($this->session->data['order_id']) && $this->session->data['order_id']) {
            if (isset($this->request->post['message']) && $this->request->post['message']) {
                $this->model_checkout_step1checkout->editOrderMessage($this->session->data['order_id'], $this->request->post['message']);
                $json['success'] = 'success';
            } else {
                $json['error'] = 'message';
            }
        } else {
            $json['error'] = 'order_id';
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function shippingMethodsFeelds()
    {

        $this->load->language('checkout/checkout');
        $this->load->model('localisation/location');
        $this->load->model('checkout/step1checkout');
        $data = array();
        $json = array();

        //echo '<pre>'; print_r($this->session->data); echo '</pre>';

        //прилетел выбранный город
        if (isset($this->request->post['cityref'])) {
            if ($this->request->post['cityref']) {
                $this->session->data['shipping_address']['city_ref'] = $this->request->post['cityref'];
                unset($this->session->data['shipping_address']['warehouse_ref']);
            } else {
                unset($this->session->data['shipping_address']['city_ref']);
                unset($this->session->data['shipping_address']['warehouse_ref']);
            }
            $this->model_checkout_step1checkout->editOrderNPost($this->session->data);
        }
        //прилетел выбранный склад
        if (isset($this->request->post['warehouseref'])) {
            if ($this->request->post['warehouseref']) {
                $this->session->data['shipping_address']['warehouse_ref'] = $this->request->post['warehouseref'];
            } else {
                unset($this->session->data['shipping_address']['warehouse_ref']);
            }
            $this->model_checkout_step1checkout->editOrderNPost($this->session->data);
        }

        if (isset($this->session->data['shipping_method']['code']) && $this->session->data['shipping_method']['code'] == 'NovaPoshta.NovaPoshta') {
            $tpl = 'npost';
            //получим список городов
            $zone_ref = $this->model_localisation_location->getZoneRefById($this->session->data['zone_id']);
            $data['cities'] = $this->model_localisation_location->getCities($zone_ref);

            if (isset($this->session->data['shipping_address']['city_ref']) && $this->session->data['shipping_address']['city_ref']) {
                $data['city_ref'] = $this->session->data['shipping_address']['city_ref'];
                //уже выбран город, значит выберем склады
                $data['warehousies'] = $this->model_localisation_location->getWarehouseByCity($data['city_ref']);
                if (isset($this->session->data['shipping_address']['warehouse_ref']) && $this->session->data['shipping_address']['warehouse_ref']) {
                    $data['warehouse_ref'] = $this->session->data['shipping_address']['warehouse_ref'];
                }
            }
        } else {
            $tpl = 'all';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
