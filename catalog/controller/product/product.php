<?php

class ControllerProductProduct extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('product/product');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $this->load->model('catalog/category');
        $datalayer_category = '';
        if (isset($this->request->get['path'])) {
            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);

                if ($category_info) {
                    $datalayer_category .= $category_info['name'].'/';
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/category', 'path=' . $path)
                    );
                }
            }

            // Set the last category breadcrumb
            $category_info = $this->model_catalog_category->getCategory($category_id);

            $data['category_info'] = $category_info;
            $data['category_name'] = $category_info['name'];
            
            if ($category_info) {
                $url = '';

                if (isset($this->request->get['sort'])) {
                    $url .= '&sort=' . $this->request->get['sort'];
                }

                if (isset($this->request->get['order'])) {
                    $url .= '&order=' . $this->request->get['order'];
                }

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

                if (isset($this->request->get['limit'])) {
                    $url .= '&limit=' . $this->request->get['limit'];
                }
                $datalayer_category .= $category_info['name'];
                $data['breadcrumbs'][] = array(
                    'text' => $category_info['name'],
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
                );
            }
        }

        $this->load->model('catalog/manufacturer');

        if (isset($this->request->get['manufacturer_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_brand'),
                'href' => $this->url->link('product/manufacturer')
            );

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

            if ($manufacturer_info) {
                $data['breadcrumbs'][] = array(
                    'text' => $manufacturer_info['name'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
                );
            }
        }

        if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
            $url = '';

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_search'),
                'href' => $this->url->link('product/search', $url)
            );
        }

        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {

            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $product_info['name'],
                'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
            );

            $this->document->setTitle($product_info['meta_title']);
            $this->document->setDescription($product_info['meta_description']);
            $this->document->setKeywords($product_info['meta_keyword']);
            $this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');

            $data['heading_title'] = $product_info['name'];

            $data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
            $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));

            $this->load->model('catalog/review');

            $data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

            $data['product_name'] = $product_info['name'];
            $data['h1'] = $product_info['h1'];

            $data['product_id'] = (int)$this->request->get['product_id'];
            $data['manufacturer'] = $product_info['manufacturer'];
            $data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
            $data['model'] = $product_info['model'];
            $data['size'] = $product_info['size'];
            $data['video'] = $product_info['video'];
            $data['overview_3d'] = $product_info['overview_3d'];
            $data['designer_3d_file'] = $product_info['designer_3d_file'];
            $data['video_gear'] = $product_info['video_gear'];
            $data['reward'] = $product_info['reward'];
            $data['points'] = $product_info['points'];
            $data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

            $current_language = $this->config->get('config_language_id');

            if (!empty($product_info['small_description'])) {
                $data['small_description'] = html_entity_decode($product_info['small_description'], ENT_QUOTES, 'UTF-8');
            } else {
                $data['small_description'] = html_entity_decode($this->config->get('config_product_text')[$current_language], ENT_QUOTES, 'UTF-8');
            }

            $data['mechanism_tip'] = html_entity_decode($this->language->get('text_mechanism_tip'), ENT_QUOTES, 'UTF-8');

            // Furniture type

            $data['furniture_type'] = (int)$product_info['furniture'];
            $data['furniture_name1'] = $product_info['name1'];
            $data['furniture_name2'] = $product_info['name2'];
            $data['furniture_name3'] = $product_info['name3'];
            $data['furniture_name4'] = $product_info['name4'];
            $data['furniture_name5'] = $product_info['name5'];

            // Href
            $data['furniture_href1'] = $product_info['href1'];
            $data['furniture_href2'] = $product_info['href2'];
            $data['furniture_href3'] = $product_info['href3'];
            $data['furniture_href4'] = $product_info['href4'];
            $data['furniture_href5'] = $product_info['href5'];

            $data['furniture_description1'] = html_entity_decode($product_info['description1'], ENT_QUOTES, 'UTF-8');
            $data['furniture_description2'] = html_entity_decode($product_info['description2'], ENT_QUOTES, 'UTF-8');
            $data['furniture_description3'] = html_entity_decode($product_info['description3'], ENT_QUOTES, 'UTF-8');
            $data['furniture_description4'] = html_entity_decode($product_info['description4'], ENT_QUOTES, 'UTF-8');
            $data['furniture_description5'] = html_entity_decode($product_info['description5'], ENT_QUOTES, 'UTF-8');

            // Sofa title
            $data['sofa_title_1'] = $this->config->get('config_sofa_title_1')[$current_language];
            $data['sofa_title_2'] = $this->config->get('config_sofa_title_2')[$current_language];
            $data['sofa_title_3'] = $this->config->get('config_sofa_title_3')[$current_language];
            $data['sofa_title_4'] = $this->config->get('config_sofa_title_4')[$current_language];
            $data['sofa_title_5'] = $this->config->get('config_sofa_title_5')[$current_language];

            // Sofa href
            $data['sofa_href_1'] = $this->config->get('config_sofa_href_1')[$current_language];
            $data['sofa_href_2'] = $this->config->get('config_sofa_href_2')[$current_language];
            $data['sofa_href_3'] = $this->config->get('config_sofa_href_3')[$current_language];
            $data['sofa_href_4'] = $this->config->get('config_sofa_href_4')[$current_language];
            $data['sofa_href_5'] = $this->config->get('config_sofa_href_5')[$current_language];

            // Sofa description
            $data['sofa_1'] = html_entity_decode($this->config->get('config_sofa_1')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['sofa_2'] = html_entity_decode($this->config->get('config_sofa_2')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['sofa_3'] = html_entity_decode($this->config->get('config_sofa_3')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['sofa_4'] = html_entity_decode($this->config->get('config_sofa_4')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['sofa_5'] = html_entity_decode($this->config->get('config_sofa_5')[$current_language], ENT_QUOTES, 'UTF-8');

            // Chairs title
            $data['chairs_title_1'] = $this->config->get('config_chairs_title_1')[$current_language];
            $data['chairs_title_2'] = $this->config->get('config_chairs_title_2')[$current_language];
            $data['chairs_title_3'] = $this->config->get('config_chairs_title_3')[$current_language];
            $data['chairs_title_4'] = $this->config->get('config_chairs_title_4')[$current_language];
            $data['chairs_title_5'] = $this->config->get('config_chairs_title_5')[$current_language];

            // Chairs href
            $data['chairs_href_1'] = $this->config->get('config_chairs_href_1')[$current_language];
            $data['chairs_href_2'] = $this->config->get('config_chairs_href_2')[$current_language];
            $data['chairs_href_3'] = $this->config->get('config_chairs_href_3')[$current_language];
            $data['chairs_href_4'] = $this->config->get('config_chairs_href_4')[$current_language];
            $data['chairs_href_5'] = $this->config->get('config_chairs_href_5')[$current_language];

            // Chairs description
            $data['chairs_1'] = html_entity_decode($this->config->get('config_chairs_1')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['chairs_2'] = html_entity_decode($this->config->get('config_chairs_2')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['chairs_3'] = html_entity_decode($this->config->get('config_chairs_3')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['chairs_4'] = html_entity_decode($this->config->get('config_chairs_4')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['chairs_5'] = html_entity_decode($this->config->get('config_chairs_5')[$current_language], ENT_QUOTES, 'UTF-8');

            // Beds title
            $data['beds_title_1'] = $this->config->get('config_beds_title_1')[$current_language];
            $data['beds_title_2'] = $this->config->get('config_beds_title_2')[$current_language];
            $data['beds_title_3'] = $this->config->get('config_beds_title_3')[$current_language];
            $data['beds_title_4'] = $this->config->get('config_beds_title_4')[$current_language];
            $data['beds_title_5'] = $this->config->get('config_beds_title_5')[$current_language];

            // Beds href
            $data['beds_href_1'] = $this->config->get('config_beds_href_1')[$current_language];
            $data['beds_href_2'] = $this->config->get('config_beds_href_2')[$current_language];
            $data['beds_href_3'] = $this->config->get('config_beds_href_3')[$current_language];
            $data['beds_href_4'] = $this->config->get('config_beds_href_4')[$current_language];
            $data['beds_href_5'] = $this->config->get('config_beds_href_5')[$current_language];

            // Beds description
            $data['beds_1'] = html_entity_decode($this->config->get('config_beds_1')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['beds_2'] = html_entity_decode($this->config->get('config_beds_2')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['beds_3'] = html_entity_decode($this->config->get('config_beds_3')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['beds_4'] = html_entity_decode($this->config->get('config_beds_4')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['beds_5'] = html_entity_decode($this->config->get('config_beds_5')[$current_language], ENT_QUOTES, 'UTF-8');

            // Accessories title
            $data['accessories_title_1'] = $this->config->get('config_accessories_title_1')[$current_language];
            $data['accessories_title_2'] = $this->config->get('config_accessories_title_2')[$current_language];
            $data['accessories_title_3'] = $this->config->get('config_accessories_title_3')[$current_language];
            $data['accessories_title_4'] = $this->config->get('config_accessories_title_4')[$current_language];
            $data['accessories_title_5'] = $this->config->get('config_accessories_title_5')[$current_language];

            // Accessories href
            $data['accessories_href_1'] = $this->config->get('config_accessories_href_1')[$current_language];
            $data['accessories_href_2'] = $this->config->get('config_accessories_href_2')[$current_language];
            $data['accessories_href_3'] = $this->config->get('config_accessories_href_3')[$current_language];
            $data['accessories_href_4'] = $this->config->get('config_accessories_href_4')[$current_language];
            $data['accessories_href_5'] = $this->config->get('config_accessories_href_5')[$current_language];

            // Accessories description
            $data['accessories_1'] = html_entity_decode($this->config->get('config_accessories_1')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['accessories_2'] = html_entity_decode($this->config->get('config_accessories_2')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['accessories_3'] = html_entity_decode($this->config->get('config_accessories_3')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['accessories_4'] = html_entity_decode($this->config->get('config_accessories_4')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['accessories_5'] = html_entity_decode($this->config->get('config_accessories_5')[$current_language], ENT_QUOTES, 'UTF-8');

            // Accessories title (table)
            $data['accessories_title_table_1'] = $this->config->get('config_accessories_title_table_1')[$current_language];
            $data['accessories_title_table_2'] = $this->config->get('config_accessories_title_table_2')[$current_language];

            // Accessories href (table)
            $data['accessories_href_table_1'] = $this->config->get('config_accessories_href_table_1')[$current_language];
            $data['accessories_href_table_2'] = $this->config->get('config_accessories_href_table_2')[$current_language];

            // Accessories description (table)
            $data['accessories_table_1'] = html_entity_decode($this->config->get('config_accessories_table_1')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['accessories_table_2'] = html_entity_decode($this->config->get('config_accessories_table_2')[$current_language], ENT_QUOTES, 'UTF-8');

            // Office title
            $data['office_title_1'] = $this->config->get('config_office_title_1')[$current_language];
            $data['office_title_2'] = $this->config->get('config_office_title_2')[$current_language];
            $data['office_title_3'] = $this->config->get('config_office_title_3')[$current_language];
            $data['office_title_4'] = $this->config->get('config_office_title_4')[$current_language];
            $data['office_title_5'] = $this->config->get('config_office_title_5')[$current_language];

            // Office href
            $data['office_href_1'] = $this->config->get('config_office_href_1')[$current_language];
            $data['office_href_2'] = $this->config->get('config_office_href_2')[$current_language];
            $data['office_href_3'] = $this->config->get('config_office_href_3')[$current_language];
            $data['office_href_4'] = $this->config->get('config_office_href_4')[$current_language];
            $data['office_href_5'] = $this->config->get('config_office_href_5')[$current_language];

            // Office description
            $data['office_1'] = html_entity_decode($this->config->get('config_office_1')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['office_2'] = html_entity_decode($this->config->get('config_office_2')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['office_3'] = html_entity_decode($this->config->get('config_office_3')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['office_4'] = html_entity_decode($this->config->get('config_office_4')[$current_language], ENT_QUOTES, 'UTF-8');
            $data['office_5'] = html_entity_decode($this->config->get('config_office_5')[$current_language], ENT_QUOTES, 'UTF-8');

            if ($product_info['quantity'] <= 0) {
                $data['stock'] = $product_info['stock_status'];
            } elseif ($this->config->get('config_stock_display')) {
                $data['stock'] = $product_info['quantity'];
            } else {
                $data['stock'] = $this->language->get('text_instock');
            }

            $promoProduct = $this->model_catalog_product->getPromoProduct($this->request->get['product_id']);

            $data['stickers'] = array();

            if ($promoProduct) {
                foreach ($promoProduct as $item) {
                    if ((strtotime(date('Y-m-d')) >= strtotime($item['date_start'])) && (strtotime(date('Y-m-d')) <= strtotime($item['date_end'])) || (($item['date_start'] == '0000-00-00') && ($item['date_end'] == '0000-00-00'))) {
                        $data['stickers'][] = array(
                            'title' => $item['promo_text']
                        );
                    }
                }
            }

            $data['is_wishlist'] = false;

            $this->load->model('account/wishlist');

            $customer_wishlist = $this->model_account_wishlist->getWishlistProductId($this->request->get['product_id']);

            if (in_array($this->request->get['product_id'], $customer_wishlist)) {
                $data['is_wishlist'] = true;
            } else {
                $data['is_wishlist'] = false;
            }

            $this->load->model('tool/image');

            if ($product_info['image']) {
                $data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
                //$data['popup'] = 'image/' . $product_info['image'];
                $data['original_image'] = $product_info['image'];
            } else {
                $data['popup'] = $this->model_tool_image->cropsize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
                $data['original_image'] = 'placeholder.png';
            }

            if ($product_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            } else {
                $data['thumb'] = '';
            }

            if ($product_info['image_size']) {
                $data['image_size'] = 'image/' . $product_info['image_size'];
            } else {
                $data['image_size'] = '';
            }

            $data['images'] = array();

            $results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
            if ($results) {
                foreach ($results as $result) {
                    $data['images'][] = array(
                        //'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')),
                        'popup' => 'image/' . $result['image'],
                        'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_height'))
                    );
                }
            } else {
                $data['images'][] = array(
                    //'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')),
                    'popup' => 'image/placeholder.png',
                    'thumb' => $this->model_tool_image->cropsize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_height'))
                );
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                $data['mpn'] = $this->currency->format($this->tax->calculate($product_info['mpn'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                $datalayer_price = $this->tax->calculate($product_info['mpn'], $product_info['tax_class_id'], $this->config->get('config_tax'));
            } else {
                $data['price'] = false;
                $data['mpn'] = false;
                $datalayer_price = false;
            }

            if ((float)$product_info['special']) {
                $data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                $datalayer_price = $this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'));
            } else {
                $data['special'] = false;
            }

            if ($this->config->get('config_tax')) {
                $data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
            } else {
                $data['tax'] = false;
            }

            $discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

            $data['discounts'] = array();

            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
                );
            }

            $data['raw_price'] = $data['price'];

            $data['raw_special'] = $data['special'];

            if ($data['price']) {
                $data['final_price'] = '<span class=\'autocalc-product-price\'>' . $data['price'] . '</span>';
            }

            if ($data['special']) {
                $data['final_special'] = '<span class=\'autocalc-product-special\'>' . $data['special'] . '</span>';
            }
            if ($data['points']) {
                $data['points'] = '<span class=\'autocalc-product-points\'>' . $data['points'] . '</span>';
            }

            $data['price_value'] = $product_info['price'];
            $data['special_value'] = $product_info['special'];
            $data['tax_value'] = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'];
            $data['points_value'] = $product_info['points'];

            $var_currency = array();
            $currency_code = !empty($this->session->data['currency']) ? $this->session->data['currency'] : $this->config->get('config_currency');
            $var_currency['value'] = $this->currency->getValue($currency_code);
            $var_currency['symbol_left'] = $this->currency->getSymbolLeft($currency_code);
            $var_currency['symbol_right'] = $this->currency->getSymbolRight($currency_code);
            $var_currency['decimals'] = $this->currency->getDecimalPlace($currency_code);
            $var_currency['decimal_point'] = $this->language->get('decimal_point');
            $var_currency['thousand_point'] = $this->language->get('thousand_point');
            $data['autocalc_currency'] = $var_currency;

            $currency2_code = $this->config->get('config_currency2');
            if ($this->currency->has($currency2_code) && $currency2_code != $currency_code) {
                $var_currency = array();
                $currency_code = $currency2_code;
                $var_currency['value'] = $this->currency->getValue($currency_code);
                $var_currency['symbol_left'] = $this->currency->getSymbolLeft($currency_code);
                $var_currency['symbol_right'] = $this->currency->getSymbolRight($currency_code);
                $var_currency['decimals'] = $this->currency->getDecimalPlace($currency_code);
                $var_currency['decimal_point'] = $this->language->get('decimal_point');
                $var_currency['thousand_point'] = $this->language->get('thousand_point');
                $data['autocalc_currency2'] = $var_currency;
            }

            $data['dicounts_unf'] = $discounts;

            $data['tax_class_id'] = $product_info['tax_class_id'];
            $data['tax_rates'] = $this->tax->getRates(0, $product_info['tax_class_id']);

            $data['autocalc_option_special'] = $this->config->get('config_autocalc_option_special');
            $data['autocalc_option_discount'] = $this->config->get('config_autocalc_option_discount');
            $data['autocalc_not_mul_qty'] = $this->config->get('config_autocalc_not_mul_qty');
            $data['autocalc_select_first'] = $this->config->get('config_autocalc_select_first');

            $data['options'] = array();

            foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
                $product_option_value_data = array();
                $cloth_colors = array();
                foreach ($option['product_option_value'] as $option_value) {
                    $cloth_colors = array();
                    if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            if ($option_value['price_prefix'] != 'd' && $option_value['price_prefix'] != 'u') {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $price = (int)$option_value['price'] . ' %';
                            }


                        } else {
                            $price = false;
                        }

                        if ($option['option_id'] == 13) {
                            $cloth_all = $this->model_catalog_product->getProductOptionCloth($this->request->get['product_id'], 14);

                            foreach ($cloth_all as $option_cloth) {
                                $data['color_option_id'] = $option_cloth['product_option_id'];
                                foreach ($option_cloth['product_option_value'] as $cloth) {
                                    if ($option_value['option_value_id'] == $cloth['cloth_type']) {
                                        $cloth_colors[$cloth['cloth_type']][] = array(
                                            'price_value' => $cloth['price'],
                                            'color_option_id' => $option_cloth['product_option_id'],
                                            'product_option_value_id' => $cloth['product_option_value_id'],
                                            'option_value_id' => $cloth['option_value_id'],
                                            'name' => $cloth['name'],
                                            'cloth_type' => $cloth['cloth_type'],
                                            'option_text' => $cloth['option_text'],
                                            'count_colors' => count($option_cloth['product_option_value']),
                                            'show_popup' => $cloth['show_popup'],
                                            'image' => $this->model_tool_image->cropsize($cloth['image'], 60, 60),
                                            'original_image' => $cloth['product_image'],
                                            'product_image' => $this->model_tool_image->resize($cloth['product_image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')),
                                            'price' => $price,
                                            'price_prefix' => $cloth['price_prefix']
                                        );
                                    }
                                }
                            }
                        }
                        if ($option['option_id'] == 18 && $option_value['option_text'] != '') {

                            // $option_value['option_text'] = $this->currency->format($this->tax->calculate( $option_value['option_text'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        }
                        $product_option_value_data[] = array(
                            'price_value' => $option_value['price'],
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'cloth_type' => $option_value['cloth_type'],
                            'option_text' => $option_value['option_text'],
                            'cloth_colors' => $cloth_colors,
                            'show_popup' => $option_value['show_popup'],
                            'image' => $this->model_tool_image->cropsize($option_value['image'], 60, 60),
                            'product_image' => $this->model_tool_image->resize($option_value['product_image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')),
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                $data['options'][] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'count_option' => count($product_option_value_data),
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'choose_type' => $option['choose_type'],
                    'type' => $option['type'],
                    'sizes' => $option['sizes'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }
            $product_related_options_value = $this->model_catalog_product->getProductRelatedOptions($this->request->get['product_id']);
            if ($product_info['minimum']) {
                $data['minimum'] = $product_info['minimum'];
            } else {
                $data['minimum'] = 1;
            }
            $data['review_status'] = $this->config->get('config_review_status');

            if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
                $data['review_guest'] = true;
            } else {
                $data['review_guest'] = false;
            }

            if ($this->customer->isLogged()) {
                $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
            } else {
                $data['customer_name'] = '';
            }

            $data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
            $data['rating'] = (int)$product_info['rating'];

            // Captcha
            if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
                $data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
            } else {
                $data['captcha'] = '';
            }

            $this->load->model('catalog/review');
            $data['reviews'] = array();

            $review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

            $results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id']);

            foreach ($results as $result) {
                $data['reviews'][] = array(
                    'author' => $result['author'] . ' ' . $result['lastname'],
                    'text' => nl2br($result['text']),
                    'rating' => (int)$result['rating'],
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
                );
            }

            $data['downloads_3d'] = array();

            $download_file = $this->model_catalog_product->getDownloads3D($this->request->get['product_id']);

            foreach ($download_file as $result) {
                $data['downloads_3d'][] = array(
                    'name' => $result['name'],
                    'href' => DIR_DOWNLOAD . $result['mask']
                );
            }

            $data['download'] = array();

            $download_files = $this->model_catalog_product->getDownload($this->request->get['product_id']);
            if ($download_files->num_rows){
                $data['download'] = array(
                    'name' => $download_files['name'],
                    'href' => DIR_DOWNLOAD . $download_files['mask']
                );
            }

            $data['downloads_size'] = array();

            $download_size = $this->model_catalog_product->getDownloadsSize($this->request->get['product_id']);

            foreach ($download_size as $result) {
                $data['downloads_size'][] = array(
                    'name' => $result['name'],
                    'href' => DIR_DOWNLOAD . $result['mask']
                );
            }

            $data['share'] = $this->url->link('product/product', 'product_id=' . (int)$this->request->get['product_id']);

            $data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

            $data['products'] = array();

            $results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

            foreach ($results as $result) {
                $filter = array(
                    'product' => $result,
                    'width' => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'),
                    'height' => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height')
                );

                $data['products'][] = $this->product->getProduct($filter);
            }

            $data['tags'] = array();

            if ($product_info['tag']) {
                $tags = explode(',', $product_info['tag']);

                foreach ($tags as $tag) {
                    $data['tags'][] = array(
                        'tag' => trim($tag),
                        'href' => $this->url->link('product/search', 'tag=' . trim($tag))
                    );
                }
            }

            $data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

            $this->model_catalog_product->updateViewed($this->request->get['product_id']);

            $this->load->model('design/layout');
            if (isset ($this->request->get ['route'])) {
                $route = (string)$this->request->get ['route'];
            } else {
                $route = 'common/home';
            }
            $layout_template = $this->model_design_layout->getLayoutTemplate($route);
            $isLayoutRoute = true;
            if (!$layout_template) {
                $layout_template = 'product';
                $isLayoutRoute = false;
            }
            // get general layout template
            if (!$isLayoutRoute) {
                $layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
                if ($layout_id) {
                    $tmp_layout_template = $this->model_design_layout->getGeneralLayoutTemplate($layout_id);
                    if ($tmp_layout_template)
                        $layout_template = $tmp_layout_template;
                }
            }
            $data_products[] = array(
                'name'=>$data['product_name'],
                'id'=> $data['product_id'],
                'price'=> $datalayer_price,
                'category'=> $datalayer_category
            );

            $datalayer = "<script>
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                        'currencyCode': '".$this->session->data['currency']."',
                        'detail':{ 
                        'products': ".json_encode($data_products,JSON_UNESCAPED_UNICODE )."
                }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Details',
                'gtm-ee-event-non-interaction': 'True',
            });
            </script>";
            $ads = "<script>
                        var google_tag_params = {
                        dynx_itemid: '".$data['product_id']."', // id товара
                        dynx_pagetype: 'offerdetail',
                        dynx_totalvalue:".$datalayer_price." // цена товара
                        };
                    </script>";

            $this->document->setAds($ads);
            $this->document->setdataLayer($datalayer);
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $data['cart'] = $this->load->controller('common/cart');


            $this->response->setOutput($this->load->view('product/' . $layout_template, $data));
        } else {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    public function write()
    {
        $this->load->language('product/product');

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if ((utf8_strlen($this->request->post['review_name']) < 3) || (utf8_strlen($this->request->post['review_name']) > 25)) {
                $json['error']['review_name'] = 'error';
            }

            if ((utf8_strlen($this->request->post['review_lastname']) < 3) || (utf8_strlen($this->request->post['review_lastname']) > 25)) {
                $json['error']['review_lastname'] = 'error';
            }

            if ((utf8_strlen($this->request->post['review_text']) < 15) || (utf8_strlen($this->request->post['review_text']) > 1000)) {
                $json['error']['review_text'] = 'error';
            }

            if (!isset($json['error'])) {
                $this->load->model('catalog/review');

                $this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

                $json['success'] = true;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function ownOrder()
    {
        $this->load->language('product/product');

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if ((utf8_strlen($this->request->post['own_name']) < 3) || (utf8_strlen($this->request->post['own_name']) > 25)) {
                $json['error']['own_name'] = $this->language->get('error_name');
            }

            if (strlen(preg_replace('/[^0-9]/', '', $this->request->post['own_phone'])) < 12) {
                $json['error']['own_phone'] = $this->language->get('error_phone');
            }

            if (!isset($json['error'])) {
                $this->load->model('catalog/product');

                $this->model_catalog_product->addOwnOrder($this->request->get['product_id'], $this->request->post);

                $json['success'] = true;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function download3D()
    {

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateDownload3D($this->request->post)) {

            if (isset($this->request->post['product'])) {
                $data['product'] = $this->request->post['product'];
            }
            if (isset($this->request->post['download_lastname'])) {
                $data['download_lastname'] = $this->request->post['download_lastname'];
            }

            if (isset($this->request->post['download_company'])) {
                $data['download_company'] = $this->request->post['download_company'];
            }

            if (isset($this->request->post['download_city'])) {
                $data['download_city'] = $this->request->post['download_city'];
            }

            if (isset($this->request->post['download_site'])) {
                $data['download_site'] = $this->request->post['download_site'];
            }

            if (isset($this->request->post['download_email'])) {
                $data['download_email'] = $this->request->post['download_email'];
            }

            if (isset($this->request->post['download_phone'])) {
                $data['download_phone'] = $this->request->post['download_phone'];
            }

            $json['success'] = true;

            $this->load->model('catalog/product');

            $this->model_catalog_product->addDownload3D($data);
        }

        if (isset($this->error['download_lastname'])) {
            $json['error']['download_lastname'] = $this->error['download_lastname'];
        }

        if (isset($this->error['download_company'])) {
            $json['error']['download_company'] = $this->error['download_company'];
        }

        if (isset($this->error['download_city'])) {
            $json['error']['download_city'] = $this->error['download_city'];
        }

        if (isset($this->error['download_site'])) {
            $json['error']['download_site'] = $this->error['download_site'];
        }

        if (isset($this->error['download_email'])) {
            $json['error']['download_email'] = $this->error['download_email'];
        }

        if (isset($this->error['download_phone'])) {
            $json['error']['download_phone'] = $this->error['download_phone'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateDownload3D()
    {

        if ((utf8_strlen(trim($this->request->post['download_lastname'])) < 2) || (utf8_strlen(trim($this->request->post['download_lastname'])) > 32)) {
            $this->error['download_lastname'] = $this->language->get('error_name');
        }

        if ((utf8_strlen(trim($this->request->post['download_company'])) < 3) || (utf8_strlen(trim($this->request->post['download_company'])) > 32)) {
            $this->error['download_company'] = $this->language->get('error_name');
        }

        if ((utf8_strlen(trim($this->request->post['download_city'])) < 2) || (utf8_strlen(trim($this->request->post['download_city'])) > 255)) {
            $this->error['download_city'] = $this->language->get('error_name');
        }

        if ((utf8_strlen(trim($this->request->post['download_site'])) < 5) || (utf8_strlen(trim($this->request->post['download_site'])) > 100)) {
            $this->error['download_site'] = $this->language->get('error_name');
        }

        if (strlen(preg_replace('/[^0-9]/', '', $this->request->post['download_phone'])) < 12) {
            $this->error['download_phone'] = $this->language->get('error_phone');
        }

        if ((utf8_strlen($this->request->post['download_email']) > 96) || !filter_var($this->request->post['download_email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['download_email'] = $this->language->get('error_email');
        }

        return !$this->error;

    }

    public function getRelatedOptionsValue()
    {
        if (isset($this->request->get['product_id'])) {
            $this->load->model('catalog/product');
            $p = $this->model_catalog_product->getRelatedOptionsValue($this->request->get['product_id'], $this->request->get['val_id']);

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($p));

        }
    }

    public function getRecurringDescription()
    {
        $this->load->language('product/product');
        $this->load->model('catalog/product');

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        if (isset($this->request->post['recurring_id'])) {
            $recurring_id = $this->request->post['recurring_id'];
        } else {
            $recurring_id = 0;
        }

        if (isset($this->request->post['quantity'])) {
            $quantity = $this->request->post['quantity'];
        } else {
            $quantity = 1;
        }

        $product_info = $this->model_catalog_product->getProduct($product_id);

        $recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

        $json = array();

        if ($product_info && $recurring_info) {
            if (!$json) {
                $frequencies = array(
                    'day' => $this->language->get('text_day'),
                    'week' => $this->language->get('text_week'),
                    'semi_month' => $this->language->get('text_semi_month'),
                    'month' => $this->language->get('text_month'),
                    'year' => $this->language->get('text_year'),
                );

                if ($recurring_info['trial_status'] == 1) {
                    $price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
                } else {
                    $trial_text = '';
                }

                $price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

                if ($recurring_info['duration']) {
                    $text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
                } else {
                    $text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
                }

                $json['success'] = $text;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function contacts()
    {

        $this->load->language('product/product');

        // Socials
        $data['pinterest'] = $this->config->get('config_pint');
        $data['quality'] = $this->config->get('config_quality');
        $data['facebook'] = $this->config->get('config_fb');
        $data['instagram'] = $this->config->get('config_inst');

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_height'));
        } else {
            $data['image'] = false;
        }

        $data['store'] = $this->config->get('config_name');
        $data['address'] = $this->config->get('config_kiev_address')[$this->config->get('config_language_id')];
        $data['phone'] = $this->config->get('config_kiev_phone');
        $data['email'] = $this->config->get('config_kiev_email');
        $data['wortime'] = $this->config->get('config_kiev_worktime')[$this->config->get('config_language_id')];
        $data['sale_phone'] = $this->config->get('config_kiev_sale_phone');
        $data['sale_email'] = $this->config->get('config_kiev_sale_email');
        $data['marketing_phone'] = $this->config->get('config_kiev_marketing_phone');
        $data['marketing_email'] = $this->config->get('config_kiev_marketing_email');

        $data['locations'] = array();

        $this->load->model('localisation/location');

        $all_locations = $this->model_localisation_location->getLocations();

        foreach ($all_locations as $location_info) {

            if ($location_info) {
                if ($location_info['image']) {
                    $image = $this->model_tool_image->resize($location_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_height'));
                } else {
                    $image = false;
                }
                $for_maps = '<div class="for_maps" style="min-width: 200px;position: relative;"><svg width="36" height="49" style="position: absolute;    fill: #ab7225;" ><use xlink:href="/front/dist/images/nav.svg#for_maps" /></svg><p class="contacts-map__city" style="margin-left: 50px">' . addcslashes($location_info['name']) . '</p><p style="margin-left: 50px">' . addcslashes($location_info['comment']) . '</p><p style="margin-left: 50px">' . addcslashes($location_info['address']) . '</p><p style="margin-left: 50px"><a class="contacts-map__note" href="tel:' . $location_info['telephone'] . '">' . $location_info['telephone'] . '</a></p><p  style="margin-left: 50px"><a class="contacts-map__note" href="tel:' . $location_info['telephone2'] . '">' . $location_info['telephone2'] . '</a></p><p class="contacts-map__note" style="margin-left: 50px"><a class="contacts-map__note" href="mailto:' . $location_info['email'] . '">' . $location_info['email'] . '</a></p></div>';

                $data['locations'][] = array(
                    'for_maps' => $for_maps,
                    'location_id' => $location_info['location_id'],
                    'name' => $location_info['name'],
                    'address' => $location_info['address'],
                    'geocode' => $location_info['geocode'],
                    'phone' => $location_info['telephone'],
                    'phone2' => $location_info['telephone2'],
                    'email' => $location_info['email'],
                    'fax' => $location_info['fax'],
                    'image' => $image,
                    'open' => $location_info['open'],
                    'comment' => $location_info['comment']
                );
            }
        }

        $this->response->setOutput($this->load->view('product/product_contacts', $data));
    }
}
