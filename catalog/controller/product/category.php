<?php
class ControllerProductCategory extends Controller {
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

        if (isset($this->request->get['price_min'])) {
            $price_min = $this->request->get['price_min'];
            $data['price_min'] = $this->request->get['price_min'];
        } else {
            $price_min = 0;
            $data['price_min'] = 0;
        }

        if (isset($this->request->get['price_max'])) {
            $price_max = $this->request->get['price_max'];
            $data['price_max'] = $this->request->get['price_max'];
        } else {
            $price_max = 0;
            $data['price_max'] = 0;
        }

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
            $data['page'] = $this->request->get['page'];
		} else {
			$page = 1;
            $data['page'] = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
            $data['limit'] = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
            $data['limit'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

            if (isset($this->request->get['price_min'])) {
                $url .= '&price_min=' . $this->request->get['price_min'];
            }

            if (isset($this->request->get['price_max'])) {
                $url .= '&price_max=' . $this->request->get['price_max'];
            }

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);


				}
			}
		} else {
			$category_id = 0;
		}

		$this->session->data['category_current'] = $category_id;

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$this->document->setTitle($category_info['meta_title']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);

			$data['heading_title'] = $category_info['name'];

			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);

            if (isset($_GET['price_min'])) {
                $data['active_filter'] = true;
            } else {
                $data['active_filter'] = false;
            }

            $data['current_category_href'] = $this->url->link('product/category', 'path=' . $this->request->get['path']);

			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['h1'] = $category_info['h1'];
			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');

			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

            if (isset($this->request->get['price_min'])) {
                $url .= '&price_min=' . $this->request->get['price_min'];
            }

            if (isset($this->request->get['price_max'])) {
                $url .= '&price_max=' . $this->request->get['price_max'];
            }

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

            if (isset($category_info['image_bg']) && is_file(DIR_IMAGE . $category_info['image_bg'])) {
                $data['background'] = $this->model_tool_image->cropsize($category_info['image_bg'], 1843, 600);
            } else {
                $data['background'] = $this->model_tool_image->cropsize('placehoder.png', 1843, 600);
            }

            if (isset($this->request->get['category_id'])) {
                $data['category_active'] = $this->request->get['category_id'];
            } else {
                $data['category_active'] = false;
            }

            if (isset($this->request->get['path'])) {
                $parts = explode('_', (string)$this->request->get['path']);
            } else {
                $parts = array();
            }

            if (isset($parts[0])) {
                $data['category_id'] = $parts[0];
            } else {
                $data['category_id'] = 0;
            }

            if (isset($parts[1])) {
                $data['category_active'] = $parts[1];
            } else {
                $data['category_active'] = 0;
            }

			$data['categories'] = array();
			$data['products'] = array();
            $data['limit'] = $limit;
			$results = $this->model_catalog_category->getCategories($data['category_id']);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				$data['categories'][] = array(
					'id'    => $result['category_id'],
					'name'  => $result['name'],
					'image' => 'image/' . $result['image'],
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'])
				);
			}

            ////////////////banner
            if (isset($category_info['banner_id']) && $category_info['banner_id'] != '0'){
                $this->load->model('design/banner');
                $data['banners'] = array();

                $results = $this->model_design_banner->getBanner($category_info['banner_id']);
                foreach ($results as $result) {
                    if (is_file(DIR_IMAGE . $result['image'])) {
                        $data['banners'][$result['sort_order']] = array(
                            'banner_id' => $result['banner_id'],
                            'title' => html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'),
                            'alt' => strip_tags(html_entity_decode($result['alt'], ENT_QUOTES, 'UTF-8')),
                            'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                            'link' => $result['link'],
                            'image' => $this->model_tool_image->resize($result['image'], 510, 600)
                        );
                        if ( $limit >= $result['sort_order']){

                            $limit--;
                        }
                    }
                }
            }
////////////////banner

            $filter_data = array(
                'filter_category_id' =>  isset($this->session->data['category_current']) ? $this->session->data['category_current'] : 0,
                'filter_filter'      => $filter,
                'sort'               => $sort,
                'order'              => $order,
                'price_min'          => $price_min,
                'price_max'          => $price_max,
                'start'              => ($page - 1) * $limit,
                'limit'              => 200,
            );
            $data['total_products'] = $this->model_catalog_product->getTotalProducts($filter_data);


            $products = $this->model_catalog_product->getProducts($filter_data);
            $i = 0;
            foreach ($products as $product) {
                $filter = array(
                    'product' => $product,
                    'width'   => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'),
                    'height'  => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height')
                );

               if (!isset($data['banners'][$i])){

                $data['products'][] = $this->product->getProduct($filter);

               } else{
                   $data['products'][] = $data['banners'][$i];
                   $data['products'][] = $this->product->getProduct($filter);

               }

                $i++;
            }
//            echo '<pre style="display:none">';
//            var_dump($filter_data);
//            echo '</pre>';
            if (isset($this->request->get['price_min'])) {
                $data['price_min'] = $this->request->get['price_min'];
            } else {
                $data['price_min'] = round($this->currency->convert($this->model_catalog_product->getMinPrice($category_id),$this->config->get('config_currency'), $this->session->data['currency']),0,PHP_ROUND_HALF_DOWN);
            }
            $data['default_min_price'] = round($this->currency->convert($this->model_catalog_product->getMinPrice($category_id),$this->config->get('config_currency'), $this->session->data['currency']),0,PHP_ROUND_HALF_DOWN);
            if (isset($this->request->get['price_max'])) {
                $data['price_max'] = $this->request->get['price_max'];
            } else {
                $data['price_max'] = round($this->currency->convert($this->model_catalog_product->getMaxPrice($category_id),$this->config->get('config_currency'), $this->session->data['currency']),0,PHP_ROUND_HALF_UP);
            }

            $data['currency_code'] = $this->session->data['currency'];

            $data['default_max_price'] = round($this->currency->convert($this->model_catalog_product->getMaxPrice($category_id),$this->config->get('config_currency'), $this->session->data['currency']),0,PHP_ROUND_HALF_UP);

            $data['session_category'] = isset($this->session->data['category_current']) ? $this->session->data['category_current'] : 0;

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_viewed'),
                'value' => 'p.viewed-DESC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.viewed&order=DESC' . $url)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_date_desc'),
                'value' => 'p.date_added-DESC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.date_added&order=DESC' . $url)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_special_asc'),
                'value' => 'ps.price-DESC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=ps.price&order=DESC' . $url)
            );

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

            if (isset($this->request->get['price_min'])) {
                $url .= '&price_min=' . $this->request->get['price_min'];
            }

            if (isset($this->request->get['price_max'])) {
                $url .= '&price_max=' . $this->request->get['price_max'];
            }

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

            if (isset($this->request->get['price_min'])) {
                $url .= '&price_min=' . $this->request->get['price_min'];
            }

            if (isset($this->request->get['price_max'])) {
                $url .= '&price_max=' . $this->request->get['price_max'];
            }

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sort'] = $sort;
			$data['order'] = $order;

			$data['continue'] = $this->url->link('common/home');

			$this->load->model ( 'design/layout' );
			if (isset ( $this->request->get ['route'] )) {
				$route = ( string ) $this->request->get ['route'];
			} else {
				$route = 'common/home';
			}
			$layout_template = $this->model_design_layout->getLayoutTemplate($route);
			$isLayoutRoute = true;
			if(!$layout_template){
				$layout_template = 'category';
				$isLayoutRoute = false;
			}
			// get general layout template
			if(!$isLayoutRoute){
				$layout_id = $this->model_catalog_category->getCategoryLayoutId($category_id);
				if($layout_id){
					$tmp_layout_template = $this->model_design_layout->getGeneralLayoutTemplate($layout_id);
					if($tmp_layout_template)
						$layout_template = $tmp_layout_template;
				}
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('product/'.$layout_template, $data));
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

    public function getProductsAjax() {

        $this->load->language('product/category');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['price_min'])) {
            $price_min = $this->request->get['price_min'];
            $data['price_min'] = $this->request->get['price_min'];
        } else {
            $price_min = 0;
            $data['price_min'] = 0;
        }

        if (isset($this->request->get['price_max'])) {
            $price_max = $this->request->get['price_max'];
            $data['price_max'] = $this->request->get['price_max'];
        } else {
            $price_max = 0;
            $data['price_max'] = 0;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
            $data['page'] = $this->request->get['page'];
        } else {
            $page = 1;
            $data['page'] = 1;
        }

//        if (isset($this->request->get['limit'])) {
//            $limit = (int)$this->request->get['limit'];
//            $data['limit'] = (int)$this->request->get['limit'];
//        } else {
            $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
            $data['limit'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
//        }

        $default_limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
        ////////////////banner


       $category_info = $this->model_catalog_category->getCategory(isset($this->session->data['category_current']) ? $this->session->data['category_current'] : 0);
        if (isset($category_info['banner_id']) && $category_info['banner_id'] != '0'){
            $this->load->model('design/banner');
            $data['banners'] = array();

            $results = $this->model_design_banner->getBanner($category_info['banner_id']);
            foreach ($results as $result) {
                if (is_file(DIR_IMAGE . $result['image']) && ($page - 1) * $default_limit <= (int)$result['sort_order'] && $limit  + ($page - 1) * $default_limit > (int)$result['sort_order'] ){
                    $data['banners'][$result['sort_order']] = array(
                        'banner_id' => $result['banner_id'],
                        'title' => html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'),
                        'alt' => strip_tags(html_entity_decode($result['alt'], ENT_QUOTES, 'UTF-8')),
                        'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                        'link' => $result['link'],
                        'image' => $this->model_tool_image->resize($result['image'], 510, 600)
                    );
                    $limit--;
                }
                if ( $limit <= (int)$result['sort_order'] &&  $default_limit >= (int)$result['sort_order'] ){
                    $default_limit--;

                }

            }
        }

////////////////banner
        $data['products'] = array();

        $filter_data = array(
            'filter_category_id' => isset($this->session->data['category_current']) ? $this->session->data['category_current'] : 0,
            'filter_filter'      => $filter,
            'sort'               => $sort,
            'order'              => $order,
            'price_min'          => $price_min,
            'price_max'          => $price_max,
            'start'              => ($page - 1) * $default_limit,
            'limit'              => $limit
        );

        $data['total_products'] = $this->model_catalog_product->getTotalProducts($filter_data);
        $results = $this->model_catalog_product->getProducts($filter_data);
        $i = ($page - 1) * $default_limit;

        foreach ($results as $product) {

            $filter = array(
                'product' => $product,
                'width'   => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'),
                'height'  => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height')
            );

            if (!isset($data['banners'][$i])){
                $data['products'][] = $this->product->getProduct($filter);

            } else{
                $data['products'][] = $data['banners'][$i];
                $data['products'][] = $this->product->getProduct($filter);

            }
            $i++;
        }
//        echo '<pre style="display:none">';
//        var_dump($data['products']);
//        echo '</pre>';
        $template = $this->load->view('product/products_ajax', $data);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($template));
    }
}
