<?php
class ControllerExtensionPaymentLiqPay extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$data['action'] = 'https://liqpay.ua/?do=clickNbuy';

		$xml  = '<request>';
		$xml .= '	<version>1.2</version>';
		$xml .= '	<result_url>' . $this->url->link('checkout/success', '', true) . '</result_url>';
		$xml .= '	<server_url>' . $this->url->link('extension/payment/liqpay/callback', '', true) . '</server_url>';
		$xml .= '	<merchant_id>' . $this->config->get('payment_liqpay_merchant') . '</merchant_id>';
		$xml .= '	<order_id>' . $this->session->data['order_id'] . '</order_id>';
		$xml .= '	<amount>' . $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false) . '</amount>';
		$xml .= '	<currency>' . $order_info['currency_code'] . '</currency>';
		$xml .= '	<description>' . $this->config->get('config_name') . ' ' . $order_info['payment_firstname'] . ' ' . $order_info['payment_address_1'] . ' ' . $order_info['payment_address_2'] . ' ' . $order_info['payment_city'] . ' ' . $order_info['email'] . '</description>';
		$xml .= '	<default_phone></default_phone>';
		$xml .= '	<pay_way>' . $this->config->get('payment_liqpay_type') . '</pay_way>';
		$xml .= '</request>';

		$data['xml'] = base64_encode($xml);
		$data['signature'] = base64_encode(sha1($this->config->get('payment_liqpay_signature') . $xml . $this->config->get('payment_liqpay_signature'), true));

		return $this->load->view('extension/payment/liqpay', $data);
	}

	public function callback() {
		$xml = base64_decode($this->request->post['operation_xml']);
		$signature = base64_encode(sha1($this->config->get('payment_liqpay_signature') . $xml . $this->config->get('payment_liqpay_signature'), true));

		$posleft = strpos($xml, 'order_id');
		$posright = strpos($xml, '/order_id');

		$order_id = substr($xml, $posleft + 9, $posright - $posleft - 10);

		if ($signature == $this->request->post['signature']) {
			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'));
		}
	}

    public function confirm() {
        $json = array();
        $json['error'] = false;
        $json['success'] = false;

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 2) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $json['error']['firstname'] = 'error';
        }

        if ($this->session->data['shipping_method']['code'] != 'cs.shipping_cs_0'){
            if ((utf8_strlen(trim($this->request->post['delivery_address'])) < 3) || (utf8_strlen(trim($this->request->post['delivery_address'])) > 100)) {
                $json['error']['delivery_address'] = 'error';
            }
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $json['error']['email'] = 'error';
        }

        if (strlen(preg_replace('/[^0-9]/', '', $this->request->post['telephone'])) < 12) {
            $json['error']['telephone'] = 'error';
        }


        if(!$this->session->data['shipping_method']){
            $json['error']['shipping_method'] = 'error';
        }


        if(!$this->request->post['zone_id']){
            $json['error']['zone_id'] = 'error';
        }
        if(!$this->request->post['country_id']){
            $json['error']['country_id'] = 'error';
        }
        if(!$this->request->post['accept']){
            $json['error']['accept'] = 'error';
        }
        if (!$json['error'] && $this->session->data['payment_method']['code'] == 'liqpay') {

            $json['success'] = true;

            $this->load->model('checkout/order');

            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_liqpay_order_status_id'));

            $json['redirect'] = $this->url->link('checkout/success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
