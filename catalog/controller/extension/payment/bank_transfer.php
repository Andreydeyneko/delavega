<?php
class ControllerExtensionPaymentBankTransfer extends Controller {
	public function index() {
		$this->load->language('extension/payment/bank_transfer');

		$data['bank'] = nl2br($this->config->get('payment_bank_transfer_bank' . $this->config->get('config_language_id')));

		return $this->load->view('extension/payment/bank_transfer', $data);
	}

	public function confirm() {
        $json = array();
        $json['error'] = false;
        $json['success'] = false;

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 2) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $json['error']['firstname'] = 'error';
        }

        if ($this->session->data['shipping_method']['code'] != 'cs.shipping_cs_0'){
            if ((utf8_strlen(trim($this->request->post['delivery_address'])) < 3) || (utf8_strlen(trim($this->request->post['delivery_address'])) > 100)) {
                $json['error']['delivery_address'] = 'error';
            }
        }

        if(!$this->session->data['shipping_method']){
            $json['error']['shipping_method'] = 'error';
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $json['error']['email'] = 'error';
        }

        if (strlen(preg_replace('/[^0-9]/', '', $this->request->post['telephone'])) < 12) {
            $json['error']['telephone'] = 'error';
        }

        if(!$this->request->post['zone_id']){
            $json['error']['zone_id'] = 'error';
        }
        if(!$this->request->post['country_id']){
            $json['error']['country_id'] = 'error';
        }

        if(!$this->request->post['accept']){
            $json['error']['accept'] = 'error';
        }

        if (!$json['error'] && $this->session->data['payment_method']['code'] == 'bank_transfer') {

            $json['success'] = true;

            $this->load->model('checkout/order');

            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_bank_transfer_order_status_id'));

            $json['redirect'] = $this->url->link('checkout/success');
        }
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
	}
}