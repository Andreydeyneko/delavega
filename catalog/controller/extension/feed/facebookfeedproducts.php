<?php 
class ControllerExtensionFeedFacebookFeedProducts extends Controller {

	public function index() {

		if ($this->config->get('facebookfeedproducts_status')) {
            $output = '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
            $output .= '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">' . "\n";
            $output .= '<channel>' . "\n";
            $output .= '<title>' . $this->config->get('config_name') . '</title>' . "\n";
            $output .= '<description>' . $this->config->get('config_meta_description') . '</description>' . "\n";
            $output .= '<link>' . HTTP_SERVER . '</link>' . "\n";

            $this->load->model('catalog/category');
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $this->load->model('extension/feed/facebookfeedproducts');

            $data=array();

			if (isset($this->request->get['language']) && $this->request->get['language'] != $this->session->data['language']) {
                $this->session->data['language'] = $this->request->get['language'];
                $this->response->redirect($_SERVER['REQUEST_URI']);
            } else if (($this->config->get('facebookfeedproducts_language') != $this->session->data['language']) && !isset($this->request->get['language'])) {
                $this->session->data['language'] = $this->config->get('facebookfeedproducts_language');
                $this->response->redirect($_SERVER['REQUEST_URI']);
            }
            $currency= $this->config->get('facebookfeedproducts_currency');
            $color= $this->config->get('facebookfeedproducts_color');
            $size= $this->config->get('facebookfeedproducts_size');
            $pattern=  $this->config->get('facebookfeedproducts_pattern');
            $material=  $this->config->get('facebookfeedproducts_material');

			$products = $this->model_extension_feed_facebookfeedproducts->getProducts();
            if ($products) {
			foreach ($products as $product) {
                if (isset($product['product_id'])) {
                    $price = isset($product['price']) ? $product['price'] : '';
                    $product_name = isset($product['name']) ? $product['name'] : '';
                    $product_id = isset($product['product_id']) ? $product['product_id'] : '';
                    $product_link = isset($product['product_id']) ? $this->url->link('product/product', 'product_id=' . $product['product_id']) : '';
                    $product_quantity = 0;
                    $categorys = '';

                    $categories = $this->model_catalog_product->getCategories($product['product_id']);

                    foreach ($categories as $category) {
                        $path = $this->getPath($category['category_id']);

                        if ($path) {

                            foreach (explode('_', $path) as $path_id) {
                                $category_info = $this->model_catalog_category->getCategory($path_id);

                                if ($category_info) {
                                    if (!$categorys) {
                                        $categorys = $category_info['name'];
                                    } else {
                                        $categorys .= ' &gt; ' . $category_info['name'];
                                    }
                                }
                            }

                        }
                    }




                    if ($color!='' || $size!='' || $pattern!='' || $material!='') {
                        $optionss = $this->model_catalog_product->getProductOptions($product['product_id']);
                        $option = array();
                        $options = array();

                        for ($i = 0; $i < count($optionss); $i++) {

                            if ($optionss[$i]['name'] == $color || $optionss[$i]['name'] == $size || $optionss[$i]['name'] == $pattern || $optionss[$i]['name'] == $material) {
                                $options[] = $optionss[$i];
                            }

                        }
                    }
                    if (isset($options) && $options!='') {
                        if (isset($options[0])) {
                            foreach ($options[0]['product_option_value'] as $option0) {
                                if ($option0['price_prefix'] == "+") {
                                    $price0 = $price + $option0['price'];
                                } else {
                                    $price0 = $price - $option0['price'];
                                }

                                $product_name0 = $product_name . " " . $option0['name'];
                                $product_id0 = $product_id . '-' . $option0['product_option_value_id'];
                                $product_link0 = $product_link . '&amp;' . $option0['name'];
                                $product_quantity = $product_quantity + $option0['quantity'];
                                $option[0] = array($options[0]['name'] => $option0['name']);

                                if (isset($options[1])) {
                                    foreach ($options[1]['product_option_value'] as $option1) {
                                        if ($option1['price_prefix'] == "+") {
                                            $price1 = $price0 + $option1['price'];
                                        } else {
                                            $price1 = $price0 - $option1['price'];
                                        }

                                        $product_name1 = $product_name0 . " " . $option1['name'];
                                        $product_id1 = $product_id0 . '-' . $option1['product_option_value_id'];
                                        $product_link1 = $product_link0 . '&amp;' . $option1['name'];
                                        $product_quantity = $product_quantity + $option1['quantity'];
                                        $option[1] = array($options[1]['name'] => $option1['name']);


                                        if (isset($options[2])) {

                                            foreach ($options[2]['product_option_value'] as $option2) {
                                                if ($option2['price_prefix'] == "+") {
                                                    $price2 = $price1 + $option2['price'];
                                                } else {
                                                    $price2 = $price1 - $option2['price'];
                                                }

                                                $product_name2 = $product_name1 . " " . $option2['name'];
                                                $product_id2 = $product_id1 . '-' . $option2['product_option_value_id'];
                                                $product_link2 = $product_link1 . '&amp;' . $option2['name'];
                                                $product_quantity = $product_quantity + $option2['quantity'];
                                                $option[2] = array($options[2]['name'] => $option2['name']);


                                                if (isset($options[3])) {

                                                    foreach ($options[3]['product_option_value'] as $option3) {

                                                        if ($option3['price_prefix'] == "+") {
                                                            $price3 = $price2 + $option3['price'];
                                                        } else {
                                                            $price3 = $price2 - $option3['price'];
                                                        }
                                                        $product_name3 = $product_name2 . " " . $option3['name'];
                                                        $product_id3 = $product_id2 . '-' . $option3['product_option_value_id'];
                                                        $product_link3 = $product_link2 . '&amp;' . $option3['name'];
                                                        $product_quantity = $product_quantity + $option3['quantity'];
                                                        $option[3] = array($options[3]['name'] => $option3['name']);

                                                        $data = array(
                                                            'title' => $product_name3,
                                                            'link' => $product_link3,
                                                            'id' => $product_id3,
                                                            'product_id' => $product['product_id'],
                                                            'description' => (isset($product['description']) && strlen(strip_tags(html_entity_decode($product['description'])))>0) ? html_entity_decode($product['description'])  : '',
                                                            'brand' => html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
                                                            'image' => $product['image'],
                                                            'mpn' => $product['model'],
                                                            'special' => $product['special'],
                                                            'price' => $price,
                                                            'currency' => $currency,
                                                            'tax_class_id' => $product['tax_class_id'],
                                                            'option' => $option,
                                                            'google_category' => $product['google_category'],
                                                            'categorys' => $categorys,
                                                            'quantity' => $product_quantity / 4
                                                        );

                                                        $output .= $this->getItem($data);


                                                    }

                                                } else {

                                                    $data = array(
                                                        'title' => $product_name2,
                                                        'link' => $product_link2,
                                                        'id' => $product_id2,
                                                        'product_id' => $product['product_id'],
                                                        'description' => (isset($product['description']) && strlen(strip_tags(html_entity_decode($product['description'])))>0) ? html_entity_decode($product['description'])  : '',
                                                        'brand' => html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
                                                        'image' => $product['image'],
                                                        'mpn' => $product['model'],
                                                        'special' => $product['special'],
                                                        'price' => $price,
                                                        'currency' => $currency,
                                                        'tax_class_id' => $product['tax_class_id'],
                                                        'option' => $option,
                                                        'google_category' => $product['google_category'],
                                                        'categorys' => $categorys,
                                                        'quantity' => $product_quantity / 3
                                                    );

                                                    $output .= $this->getItem($data);

                                                }
                                            }
                                        } else {
                                            $data = array(
                                                'title' => $product_name1,
                                                'link' => $product_link1,
                                                'id' => $product_id1,
                                                'product_id' => $product['product_id'],
                                                'description' => (isset($product['description']) && strlen(strip_tags(html_entity_decode($product['description'])))>0) ? html_entity_decode($product['description'])  : '',
                                                'brand' => html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
                                                'image' => $product['image'],
                                                'mpn' => $product['model'],
                                                'special' => $product['special'],
                                                'price' => $price,
                                                'currency' => $currency,
                                                'tax_class_id' => $product['tax_class_id'],
                                                'option' => $option,
                                                'google_category' => $product['google_category'],
                                                'categorys' => $categorys,
                                                'quantity' => $product_quantity / 2
                                            );

                                            $output .= $this->getItem($data);

                                        }

                                    }
                                } else {
                                    $data = array(
                                        'title' => $product_name0,
                                        'link' => $product_link0,
                                        'id' => $product_id0,
                                        'product_id' => $product['product_id'],
                                        'description' => (isset($product['description']) && strlen(strip_tags(html_entity_decode($product['description'])))>0) ? html_entity_decode($product['description'])  : '',
                                        'brand' => html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8'),
                                        'image' => $product['image'],
                                        'mpn' => $product['model'],
                                        'special' => $product['special'],
                                        'price' => $price,
                                        'currency' => $currency,
                                        'tax_class_id' => $product['tax_class_id'],
                                        'option' => $option,
                                        'google_category' => $product['google_category'],
                                        'categorys' => $categorys,
                                        'quantity' => $product_quantity
                                    );

                                    $output .= $this->getItem($data);

                                }
                            }
                        }

                    } else {
                        $data = array(
                            'title' => str_replace('&', '&amp;', $product_name),
                            'link' => $product_link,
                            'id' => $product_id,
                            'product_id' => $product_id,
                            'description' => (isset($product['description']) && strlen(strip_tags(html_entity_decode($product['description'])))>0) ? html_entity_decode($product['description'])  : '',
                            'brand' => isset($product['manufacturer']) ? str_replace('&', '&amp;', html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8')) : '',
                            'image' => isset($product['image']) ? $product['image'] : '',
                            'mpn' => isset($product['model']) ? $product['model'] : '',
                            'special' => isset($product['special']) ? $product['special'] : '',
                            'price' => isset($price) ? $price : '',
                            'currency' => $currency,
                            'tax_class_id' => isset($product['tax_class_id']) ? $product['tax_class_id'] : '',
                            'google_category' => isset($product['google_category']) ? $product['google_category'] : '',
                            'categorys' => $categorys,
                            'quantity' => isset($product['quantity']) ? $product['quantity'] : ''
                        );

                        $output .= $this->getItem($data);

                    }
                }
            }
            }

			
			$output .= '</channel>'. "\n"; 
			$output .= '</rss>'. "\n";
            $file = 'feed.xml';
            $json['success'] = false;
            @unlink($file);
            if(file_put_contents($file, $output)){
                $json['success'] = true;
            };

           // $this->response->addHeader('Content-Type: application/json');
            //$this->response->setOutput(json_encode($json));


			$this->response->addHeader('Content-type: text/rss');
			$this->response->setOutput($output);


        }
	}

	protected function getItem($data) {

		$output_ = '';
		$output_ .= '<item>'. "\n";
		$output_ .= '<g:title>' . $data['title'] . '</g:title>'. "\n";
		$output_ .= '<g:link>' . $data['link']  . '</g:link>'. "\n";
		$output_ .= '<g:description>' .$data['description'] . '</g:description>'. "\n";
		$output_ .= '<g:brand>' . $data['brand'] . '</g:brand>'. "\n";
		$output_ .= '<g:condition>new</g:condition>'. "\n";
		$output_ .= '<g:id>' . $data['id'] . '</g:id>'. "\n";
															
		if ($data['image']) {
			$output_ .= '<g:image_link>' . $this->model_tool_image->resize($data['image'], 500, 500) . '</g:image_link>'. "\n";
		} else {
			$output_ .= '<g:image_link>' . $this->model_tool_image->resize('no_image.jpg', 500, 500) . '</g:image_link>'. "\n";
		}
															
			$additional_images = $this->model_catalog_product->getProductImages($data['product_id']);
																				
			if($additional_images) {
				foreach (array_slice($additional_images,0,9) as $additional_image) {
					$output_ .= '<g:additional_image_link>'. $this->model_tool_image->resize($additional_image['image'], 500, 500) .'</g:additional_image_link>'. "\n";
				}
			}				

		$output_ .= '<g:mpn>' . $data['mpn'] . '</g:mpn>'. "\n";

		if ((float)$data['special']) {
			$output_ .= '<g:sale_price>' .  $this->currency->format($data['special'], $data['currency'], false, false).' '. $data['currency'] . '</g:sale_price>'. "\n";
			$output_ .= '<g:tax_calc_for_sale_price>' .$this->currency->format( $this->tax->getTax($data['special'],$data['tax_class_id']),$data['currency'], false, false) . '</g:tax_calc_for_sale_price>'. "\n";
			$output_ .= '<g:taxed_sale_price>' .  $this->currency->format($this->tax->calculate($data['special'], $data['tax_class_id']), $data['currency'], false, false) . '</g:taxed_sale_price>'. "\n";
		}

		$output_ .= '<g:price>' . $this->currency->format($data['price'], $data['currency'], false, false).' '. $data['currency']. '</g:price>'. "\n";
		$output_ .= '<g:tax_calc_for_price>' . $this->currency->format($this->tax->getTax($data['price'],$data['tax_class_id']),$data['currency'], false, false) . '</g:tax_calc_for_price>'. "\n";
		$output_ .= '<g:taxed_price>' . $this->currency->format($this->tax->calculate($data['price'], $data['tax_class_id']), $data['currency'], false, false) . '</g:taxed_price>'. "\n";
        $output_ .= '<g:google_product_category>' . $data['google_category'] . '</g:google_product_category>'. "\n";											
        $output_ .= '<g:product_type>' . $data['categorys'] . '</g:product_type>'. "\n";
        $output_ .= '<g:availability>' . ($data['quantity'] ? 'in stock' : 'out of stock') . '</g:availability>'. "\n";
			
			if (isset($data['option'])){
				foreach ($data['option'] as $optio){
					foreach ($optio as $key=>$opt){
						$output_ .= '<g:'.$key.'>' . $opt . '</g:'.$key.'>'. "\n"; 
					}
				}
			}
			
			$output_ .= '</item>'. "\n";
		
		
		return $output_;
	}		

	protected function getPath($parent_id, $current_path = '') {
		$category_info = $this->model_catalog_category->getCategory($parent_id);

		if ($category_info) {
			if (!$current_path) {
				$new_path = $category_info['category_id'];
			} else {
				$new_path = $category_info['category_id'] . '_' . $current_path;
			}

			$path = $this->getPath($category_info['parent_id'], $new_path);

			if ($path) {
				return $path;
			} else {
				return $new_path;
			}
		}
	}	
}
?>