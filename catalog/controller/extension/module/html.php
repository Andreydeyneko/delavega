<?php
class ControllerExtensionModuleHTML extends Controller {
	public function index($setting) { 
		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
			$data['layout'] = html_entity_decode($setting['layout'], ENT_QUOTES, 'UTF-8');

			if(isset($setting['product_desc_img']) && $setting['product_desc_img']){
				$data['triggers'] = $setting['product_desc_img'][$this->config->get('config_language_id')];
				array_walk($data['triggers'], function (&$item) {
				    $item['description'] = html_entity_decode($item['description'], ENT_QUOTES, 'UTF-8');
                });
			} else {
				$data['triggers'] = array();
			}

			if($data['layout']){
				$tpl = $data['layout'];
			} else {
				$tpl = 'html';
			}

			return $this->load->view('extension/module/'.$tpl, $data);
		}
	}
}