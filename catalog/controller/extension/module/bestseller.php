<?php
class ControllerExtensionModuleBestSeller extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/bestseller');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {

				$filter = array(
					'product' => $result,
					'width'   => $setting['width'],
					'height'  => $setting['height']
				);

				$data['products'][] = $this->product->getProduct($filter);
			}

			return $this->load->view('extension/module/bestseller', $data);
		}
	}
}
