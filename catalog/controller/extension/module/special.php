<?php
class ControllerExtensionModuleSpecial extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/special');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_catalog_product->getProductSpecials($filter_data);

		if ($results) {
			foreach ($results as $result) {
				$filter = array(
					'product' => $result,
					'width'   => $setting['width'],
					'height'  => $setting['height']
				);

				$data['products'][] = $this->product->getProduct($filter);
			}

			return $this->load->view('extension/module/special', $data);
		}
	}
}