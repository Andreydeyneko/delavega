<?php
class ControllerExtensionModuleInstagram extends Controller {
    public function index($setting) {

        $this->load->model('tool/image');

        $this->load->language('extension/module/instagram');

        $data['heading_title'] = $setting['title'][$this->config->get('config_language_id')];

        $data['bIndexBot'] = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false);
        $data['instagram'] = $this->config->get('config_inst');

        $data['user_id'] = $setting['user_id'];
        $data['token'] = $setting['token'];
        $data['width'] = $setting['width'];
        $data['height'] = $setting['height'];
        $data['hashtag'] = $setting['hashtag'];
        $data['count'] = $setting['qty'];

        return $this->load->view('extension/module/instagram', $data);
    }
}