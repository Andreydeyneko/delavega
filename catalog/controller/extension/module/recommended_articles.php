<?php
class ControllerExtensionModuleRecommendedArticles extends Controller {
    public function index($setting) {
        $this->load->language('extension/module/popular_articles');

        $this->load->model('tool/image');

        $data['tltblogs'] = array();

        if (!$setting['limit']) {
            $setting['limit'] = 3;
        }

        if (!empty($setting['article'])) {
            $all_articles = array_slice($setting['article'], 0, (int)$setting['limit']);

            foreach ($all_articles as $article_id) {
                $article_info = $this->model_extension_tltblog_tltblog->getTltBlog($article_id);

                if ($article_info) {
                    if ($article_info['image']) {
                        $image = $this->model_tool_image->cropsize($article_info['image'], $setting['width'], $setting['height']);
                    } else {
                        $image = $this->model_tool_image->cropsize('placeholder.png', $setting['width'], $setting['height']);
                    }
                    $this->load->model('extension/tltblog/url_alias');
                    $pr1= explode('/',$this->request->get['_route_']);
                    $querys = $this->model_extension_tltblog_url_alias->getUrlAliasById('tltblog_id='.$article_info['tltblog_id'],$this->config->get('config_language_id'));
                    $blog_id = $article_id;

//                    echo '<pre style="display: none">';
//                    print_r($blog_id.' --');
//                    echo '</pre>';

                    $url_data = array();
                    if (isset($blog_id)&&$blog_id>0) {
                        $blog_ids = (int)$blog_id;

                        $url_to = $this->url->link('extension/tltblog/tltblog_seo', http_build_query($url_data), true).'/'.$querys['keyword'];


                    }else{
                        $url_to = $this->url->link('extension/tltblog/tltblog_seo', http_build_query($url_data), true);

                    }

                    $data['tltblogs'][] = array(
                        'tltblog_id'  		=> $article_info['tltblog_id'],
                        'thumb'       		=> $image,
                        'title'       		=> $article_info['title'],
                        'intro'       		=> utf8_substr(strip_tags(html_entity_decode($article_info['intro'], ENT_QUOTES, 'UTF-8')), 0, 60) . ' ...',
                        'date' 	            => date('d.m.Y', strtotime($article_info['date_added'])),
                        'href2'        		=> $this->url->link('extension/tltblog/tltblog', 'tltblog_id=' . $article_info['tltblog_id']),
                        'href'        		=> $url_to
                    );
                }
            }
        }

        return $this->load->view('extension/module/recommended_articles', $data);
    }
}