<?php
class ControllerExtensionModuleLastview extends Controller {
    public function index($setting)
    {
        if (isset($this->session->data['products_id'])) {
            if (!in_array($this->request->get['product_id'], $this->session->data['products_id'])) {
                $this->session->data['products_id'][] = $this->request->get['product_id'];
            }
        } else {
            $this->session->data['products_id'][] = $this->request->get['product_id'];
        }

        $this->load->language('extension/module/lastview');

        $data['cart'] = $this->url->link('checkout/cart');
        $data['wishlist'] = $this->url->link('account/wishlist');
        $data['compare'] = $this->url->link('product/compare');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if(isset($this->session->data['products_id']))
            $this->session->data['products_id'] = array_slice($this->session->data['products_id'], -$setting['limit']);

        if (isset($this->session->data['products_id'])) {
            foreach ($this->session->data['products_id'] as $result_id) {
                $result = $this->model_catalog_product->getProduct($result_id);

                $filter = array(
                    'product' => $result,
                    'width'   => $setting['width'],
                    'height'  => $setting['height']
                );

                $data['products'][] = $this->product->getProduct($filter);
            }
        }

        return $this->load->view('extension/module/lastview', $data);
    }
}