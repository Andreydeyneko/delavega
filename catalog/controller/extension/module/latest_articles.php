<?php
class ControllerExtensionModuleLatestArticles extends Controller {
    public function index($setting) {
        $this->load->language('extension/module/latest_articles');

        $data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->session->data['language']) && $this->session->data['language'] == 'ua-uk') {
            $data['news'] = 'news-ua';
        } elseif (isset($this->session->data['language']) && $this->session->data['language'] == 'en-gb') {
            $data['news'] = 'news-en';
        } elseif (isset($this->session->data['language']) && $this->session->data['language'] == 'de-DE') {
            $data['news'] = 'news-de';
        } elseif (isset($this->session->data['language']) && $this->session->data['language'] == 'ru-ru') {
            $data['news'] = 'news-ru';
        } else {
            $data['news'] = 'news';
        }

        if (isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') {
            $data['class'] = ' section--e';
        } else {
            $data['class'] = '';
        }

        $this->load->model('extension/tltblog/tltblog');

        $this->load->model('tool/image');

        $data['articles'] = array();

        $results = $this->model_extension_tltblog_tltblog->getTltBlogsLatest($setting['limit']);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height']);
                } else {
                    $image = $this->model_tool_image->cropsize('placeholder.png', $setting['width'], $setting['height']);
                }
                $data['articles'][] = array(
                    'tltblog_id'  		=> $result['tltblog_id'],
                    'thumb'       		=> $image,
                    'title'       		=> $result['title'],
                    'intro'       		=> html_entity_decode($result['intro'], ENT_QUOTES, 'UTF-8'),
                    'date' 	            => date('d.m.Y', strtotime($result['date_added'])),
                    'href'        		=> $this->url->link('extension/tltblog/tltblog', 'tltpath=' . $data['news'] . '&tltblog_id=' . $result['tltblog_id'])
                );
            }

            return $this->load->view('extension/module/latest_articles', $data);
        }
    }
}