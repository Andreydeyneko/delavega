<?php
class ControllerExtensionModulePopularArticles extends Controller {
    public function index($setting) {
        $this->load->language('extension/module/popular_articles');

        $data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('extension/tltblog/tltblog');

        $this->load->model('tool/image');

        $data['tltblogs'] = array();

        $results = $this->model_extension_tltblog_tltblog->getTltBlogsPopular($setting['limit']);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height']);
                } else {
                    $image = $this->model_tool_image->cropsize('placeholder.png', $setting['width'], $setting['height']);
                }
                $this->load->model('extension/tltblog/url_alias');
                $pr1= explode('/',$this->request->get['_route_']);
                $querys = $this->model_extension_tltblog_url_alias->getUrlAliasById('tltblog_id='.$result['tltblog_id'],$this->config->get('config_language_id'));
                $blog_id = $result['tltblog_id'];
                $url_data = array();
                if (isset($blog_id)&&$blog_id>0) {
                    $blog_ids = (int)$blog_id;

                    $url_to = $this->url->link('extension/tltblog/tltblog_seo', http_build_query($url_data), true).'/'.$querys['keyword'];


                }else{
                    $url_to = $this->url->link('extension/tltblog/tltblog_seo', http_build_query($url_data), true);

                }


                    $data['tltblogs'][] = array(
                        'tltblog_id'  		=> $result['tltblog_id'],
                        'thumb'       		=> $image,
                        'href'       		=> $url_to,
                        'test2'       		=> $pr1,
                        'test3'       		=> $this->url->link('extension/tltblog/tltblog_seo', http_build_query($url_data), true),
                        'title'       		=> $result['title'],
                        'intro'       		=> utf8_substr(strip_tags(html_entity_decode($result['intro'], ENT_QUOTES, 'UTF-8')), 0, 60) . ' ...',
                        'date' 	            => date('d.m.Y', strtotime($result['date_added'])),
                        'href2'        		=> $this->url->link('extension/tltblog/tltblog', 'tltblog_id=' . $result['tltblog_id'])
                    );
            }

//            echo '<pre style="display: none">';
//            print_r($querys);
//            echo '</pre>';
//            echo '<pre style="display: none">';
//            print_r($this->request->get);
//            echo '</pre>';
//            echo '<pre style="display: none">';
//            print_r( $data['tltblogs']);
//            echo '</pre>';

            return $this->load->view('extension/module/popular_articles', $data);
        }
    }
}