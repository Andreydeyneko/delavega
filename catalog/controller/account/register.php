<?php
class ControllerAccountRegister extends Controller {
	private $error = array();

	public function index() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

        $data['action'] = $this->url->link('account/register', '', true);

		$this->load->language('account/register');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');

		$data['heading_title'] = $this->language->get('heading_title');

        $data['login'] = $this->url->link('account/login', '', true);

		if ($this->request->post && $this->validate()) {

            $password = $this->generatePassword();

            $this->session->data['register_data'] = array(
                'firstname' => '',
                'lastname'  => '',
                'telephone' => $this->request->post['telephone'],
                'email'     => $this->request->post['email'],
                'password'  => $password
            );

            $this->turbosms->send($this->request->post['telephone'], $password);

			$this->response->redirect($this->url->link('account/register_confirm'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

//		$data['breadcrumbs'][] = array(
//			'text' => $this->language->get('text_account'),
//			'href' => $this->url->link('account/account', '', true)
//		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_register'),
			'href' => $this->url->link('account/register', '', true)
		);
		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (isset($this->session->data['register_data']['email'])) {
			$data['email'] = $this->session->data['register_data']['email'];
		} else {
            $data['email'] = '';
        }

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
        } elseif (isset($this->session->data['register_data']['telephone'])) {
            $data['telephone'] = $this->session->data['register_data']['telephone'];
		} else {
			$data['telephone'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/register', $data));
	}

	private function validate() {

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

        if (preg_match('/_/i', $this->request->post['telephone'])) {
            $this->error['telephone'] = $this->language->get('error_phone');
        }

        if ((utf8_strlen(trim($this->request->post['telephone'])) < 8)) {
            $this->error['telephone'] = $this->language->get('error_phone');
        }
		
		return !$this->error;
	}

    public function generatePassword() {

        $chars = "1234567890";
        $max = 4;

        $size = StrLen($chars) - 1;

        $password = null;

        while($max--)
            $password .= $chars[rand(0,$size)];

        return $password;
    }
}