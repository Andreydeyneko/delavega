<?php
class ControllerAccountLogin extends Controller {
	private $error = array();

	public function index() {
		$this->load->model('account/customer');

		// Login override for admin users
		if (!empty($this->request->get['token'])) {
			$this->customer->logout();
			$this->cart->clear();

			unset($this->session->data['order_id']);
			unset($this->session->data['payment_address']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['comment']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);

			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

			if ($customer_info && $this->customer->login($customer_info['email'], '', true)) {
				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}

		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/login');

		$this->document->setTitle($this->language->get('heading_title'));

		if ($this->request->post && $this->validate()) {

            $password = $this->generatePassword();

            $this->session->data['login_data'] = array(
                'telephone' => $this->request->post['telephone'],
                'password'  => $password
            );

            $this->turbosms->send($this->request->post['telephone'], $password);

            $this->response->redirect($this->url->link('account/login_confirm'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

//		$data['breadcrumbs'][] = array(
//			'text' => $this->language->get('text_account'),
//			'href' => $this->url->link('account/account', '', true)
//		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

		$data['action'] = $this->url->link('account/login', '', true);
		$data['register'] = $this->url->link('account/register', '', true);

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}
        $this->document->setrobots('noindex, follow');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/login', $data));
	}

	protected function validate() {
        if (isset($this->request->post['telephone'])) {
            $customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['telephone']);
            if ($customer_info) {
                if ($this->request->post['telephone'] != $customer_info['telephone']) {
                    $this->error['warning'] = $this->language->get('error_phone_db');
                }
            } else {
                $this->error['warning'] = $this->language->get('error_phone_db');
            }
        } else {
            $this->error['warning'] = $this->language->get('error_phone_db');
        }
		return !$this->error;
	}

    public function generatePassword() {

        $chars = "1234567890";
        $max = 4;

        $size = StrLen($chars) - 1;

        $password = null;

        while($max--)
            $password .= $chars[rand(0,$size)];

        return $password;
    }
}
