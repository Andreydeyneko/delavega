<?php
class ControllerAccountRegisterConfirm extends Controller {
    private $error = array();

    public function index() {
        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('account/account', '', true));
        }

        $this->load->language('account/register');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/customer');

        $data['text_resend_popup'] = $this->language->get('text_resend_popup');

        $data['register_link'] = $this->url->link('account/register', '', true);

        if ($this->request->post && $this->validate()) {

            $this->model_account_customer->addCustomer($this->session->data['register_data']);

            $this->customer->login($this->session->data['register_data']['email'], $this->session->data['register_data']['password']);

            unset($this->session->data['guest']);
            unset($this->session->data['register_data']);

            $this->response->redirect($this->url->link('account/account'));
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

//        $data['breadcrumbs'][] = array(
//            'text' => $this->language->get('text_account'),
//            'href' => $this->url->link('account/account', '', true)
//        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_register'),
            'href' => $this->url->link('account/register', '', true)
        );
        $data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }

        $data['action'] = $this->url->link('account/register_confirm', '', true);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/register_confirm', $data));
    }

    private function validate() {

        if ($this->request->post['password'] != $this->session->data['register_data']['password']) {
            $this->error['password'] = $this->language->get('error_password');
        }

        return !$this->error;
    }

    public function resendPassword() {
        $json = array();

        $new_password = $this->generatePassword();

        if (isset($this->session->data['register_data']['telephone']) && isset($this->session->data['register_data']['password'])) {
            $this->session->data['register_data']['password'] = $new_password;
            $this->turbosms->send($this->session->data['register_data']['telephone'], $new_password);
            $json['success'] = true;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function generatePassword() {

        $chars = "1234567890";
        $max = 4;

        $size = StrLen($chars) - 1;

        $password = null;

        while($max--)
            $password .= $chars[rand(0,$size)];

        return $password;
    }
}