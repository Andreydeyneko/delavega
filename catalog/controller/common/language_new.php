<?php
class ControllerCommonLanguageNew extends Controller {
    public function index() {
        $this->load->language('common/language');

        if ($this->config->get('tltblog_seo')) {
            require_once(DIR_APPLICATION . 'controller/extension/tltblog/tltblog_seo.php');
            $tltblog_seo = new ControllerExtensionTltBlogTltBlogSeo($this->registry);
            $this->url->addRewrite($tltblog_seo);
        }

        $data['action'] = $this->url->link('common/language/language', '', $this->request->server['HTTPS']);

        $data['code'] = $this->session->data['language'];

        $this->load->model('localisation/language');

        $data['languages'] = array();

        $results = $this->model_localisation_language->getLanguages();

        foreach ($results as $result) {
            if ($result['status']) {
                $data['languages'][] = array(
                    'name' => $result['name'],
                    'code' => $result['code']
                );
            }
        }

        if (!isset($this->request->get['route'])) {
            $data['redirect'] = $this->url->link('common/home');
        } else {
            $url_data = $this->request->get;

            unset($url_data['_route_']);

            $route = $url_data['route'];

            unset($url_data['route']);

            $url = '';

            if ($url_data) {
                $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            }

            $data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
            $data['tltblog_id'] = $url_data['tltblog_id'];
        }

        return $this->load->view('common/language_new', $data);
    }

    public function language() {
        if (isset($this->request->post['code'])) {
            $this->session->data['language'] = $this->request->post['code'];

            if ($this->request->post['code'] == 'en-gb' || $this->request->post['code'] == 'de-DE') {
                $this->session->data['currency'] = 'EUR';
            } else {
                $this->session->data['currency'] = 'UAH';
            }

            $this->load->model('localisation/language');

            $this->config->set('config_language_id', $this->model_localisation_language->getLanguageByCode($this->request->post['code'])['language_id']);
        }

        if (isset($this->request->post['redirect'])) {

            $parseUrl = parse_url(str_replace('&amp;', '&', $this->request->post['redirect']));

            if (isset($parseUrl['query'])  && !isset($this->request->post['tltblog_id'])) {
                $this->response->redirect($this->request->post['redirect']);
            } elseif ($parseUrl['path'] == '/ru/' || $parseUrl['path'] == '/en/' || $parseUrl['path'] == '/') {
                $langCodeMain = substr($this->request->post['code'],0, 2);
                if($langCodeMain == 'ua'){
                    $this->response->redirect(str_replace('ua'. $parseUrl['path'], 'ua/', $this->request->post['redirect']));
                } else {
                    $this->response->redirect(str_replace('ua'. $parseUrl['path'], 'ua/' . $langCodeMain . '/', $this->request->post['redirect']));
                }
            } else {
                if($this->request->post['tltblog_id']){
                    $langBlog = substr($this->request->post['code'],0, 2);
                    $linkTltblog = $this->url->link('extension/tltblog/tltblog', 'tltpath=news-' . $langBlog . '&tltblog_id=' . $this->request->post['tltblog_id'], true);

                    $linkTltblog = $this->getLangUrlForBlog($linkTltblog, $langBlog);

                    if ($linkTltblog) {
                        $this->response->redirect($linkTltblog);
                    } else {
                        $this->response->redirect($this->request->post['redirect']);
                    }
                } else {
                    $parts = explode('/', $parseUrl['path']);

                    if (utf8_strlen(reset($parts)) == 0) {
                        array_shift($parts);
                    }

                    // remove any empty arrays from trailing
                    if (utf8_strlen(end($parts)) == 0) {
                        array_pop($parts);
                    }

                    $urlRedirect = '';

                    foreach ($parts as $part) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE keyword = '" . $this->db->escape($part) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

                        if ($query->num_rows) {
                            if ($this->config->get('config_language_id') != $query->row['language_id']) {
                                $url = explode('=', $query->row['query']);
                                if (isset($url[0]) && isset($url[1])) {
                                    $query2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = '" . $this->db->escape($url[0] . '=' . $url[1]) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                                } else {
                                    $query2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = '" . $this->db->escape($url[0]) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                                }
                                if ($query2->num_rows) {
                                    $urlRedirect .= '/' . $query2->row['keyword'];
                                }
                            }

                        }
                    }

                    if ($urlRedirect) {
                        $this->response->redirect(getUrl() . $urlRedirect);
                    } else {
                        $this->response->redirect($this->request->post['redirect']);
                    }
                }
            }

        } else {
            $this->response->redirect($this->url->link('common/home'));
        }
    }

    protected function getLangUrlForBlog($url, $lan) {
        if($lan != 'ua') {
            return $url = str_replace('/news-'.$lan, '/'.$lan.'/news-'.$lan, $url);
        } else {
            return $url;
        }
    }
}