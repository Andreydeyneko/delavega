<?php
class ControllerCommonCart extends Controller {
	public function index() {
		$this->load->language('common/cart');

		// Totals
		$this->load->model('setting/extension');

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);

		// Display prices
		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_setting_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get('total_' . $result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);
		}

		$data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));

		$this->load->model('tool/image');
		$this->load->model('tool/upload');
		$this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $data_products = array();
		$data['products'] = array();
		foreach ($this->cart->getProduct() as $product) {

            $variant = '';

            if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], 425, 290);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 425, 290);
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option['price']) {

                    if ($option['price_prefix'] !='d' && $option['price_prefix']!='u'){
                        $price = $this->currency->format($this->tax->calculate($option['price'], $product['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                    }else{
                        $price = (int)$option['price'].' %';
                    }

                } else {
                    $price = false;
                }
                if ($option["option_id"] == "16" ){
                    $variant = $option["value"];
                }
				$option_data[] = array(
                    'id'    => $option['option_id'],
					'name'  => $option['name'],
					'option_text'  => '',
					'prefix'=> $option['price_prefix'],
					'value' => $value,
                    'price' => $price,
					'type'  => $option['type']
				);
			}

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));

				$price = $this->currency->format($unit_price, $this->session->data['currency']);
				$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
			} else {
				$price = false;
				$total = false;
			}

			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'price'     => $price,
				'total'     => $total,
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
            $category='';
            foreach ($this->model_catalog_product->getCategories($product['product_id']) as $categorye){
                $category .= $this->model_catalog_category->getCategory($categorye['category_id'])['name'].'/';
            }

            $data_products[] = array(
                'name'=> $product['name'],
                'id'=> $product['product_id'],
                'price'=> $product['price'],
                'brand'=> 'Delavega',
                'category'=> substr($category, 0, -1),
                'variant'=> $variant,
                'quantity'=> $product['quantity']
            );


		}

		// Gift Voucher
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency'])
				);
			}
		}

		$data['totals'] = array();

		foreach ($totals as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
			);
		}


        $data['datalayer'] = "<script>
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                        'currencyCode': '".$this->session->data['currency']."',
                        'detail':{ 
                        'products': ".json_encode($data_products,JSON_UNESCAPED_UNICODE )."
                }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Details',
                'gtm-ee-event-non-interaction': 'True',
            });
            </script>";


		$data['cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/step1checkout', '', true);
		return $this->load->view('common/cart', $data);
	}

	public function info() {
		$this->response->setOutput($this->index());
	}
}
