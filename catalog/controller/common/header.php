<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('setting/extension');

        $data['bIndexBot'] = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false);

		$data['analytics'] = array();

		$analytics = $this->model_setting_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
			}
		}
		/*new js and css for header*/
        $this->document->addStyle('catalog/view/theme/default/stylesheet/new_stylesheet.css');
        $this->document->addScript('catalog/view/javascript/script_new.js');
        /*new js and css for header*/

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
        $this->load->model('localisation/language');
        $results = $this->model_localisation_language->getLanguageByCode($this->session->data['language']);
        $data['alt_logo'] = $this->config->get('config_alt_logo')[$results["language_id"]];

        $data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
        if ($this->document->getrobots()){
            $data['robots'] = $this->document->getrobots();
        }else{
            $data['robots'] = '';
        }
		$data['datalayer'] = $this->document->getdataLayer();
        //var_dump($data['datalayer']);
        if($data['datalayer'] == NULL){
            $data['datalayer'] = '<script> window.dataLayer = window.dataLayer || [] </script>';
        }
		if ($this->document->getAds()){
            $data['ads'] = $this->document->getAds();
        }else{
            $data['ads'] = "<script>
                                var google_tag_params = {
                                dynx_itemid: '',
                                dynx_pagetype: 'other',
                                dynx_totalvalue:''
                                };
                            </script>";
        }

//		$data['links'] = $this->document->getLinks();
        $data['link'] = 'https://' . $this->request->server['SERVER_NAME'] . $this->request->server['SCRIPT_URL'];
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts('header');
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = $this->model_account_wishlist->getTotalWishlist();
		} else {
			$data['text_wishlist'] = (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0);
		}

		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/step1checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['company'] = $this->url->link('information/information', '&information_id=4', true);
		$data['telephone'] = $this->config->get('config_telephone');
		$data['telephone_href'] = str_replace([' ', '(', ')', '-'], '', $this->config->get('config_telephone'));
		$data['telephone2_href'] = str_replace([' ', '(', ')', '-'], '', $this->config->get('config_telephone2'));
		$data['telephone2'] = $this->config->get('config_telephone2');
		$data['telephone3'] = $this->config->get('config_telephone3');
        $data['facebook'] = $this->config->get('config_fb');
        $data['instagram'] = $this->config->get('config_inst');
        $data['pinterest'] = $this->config->get('config_pint');

        $data['telegram'] = $this->config->get('config_telegram_bot');
        $data['viber'] = $this->config->get('config_viber_bot');
		$data['count_products'] = $this->cart->countProducts();

		$data['language'] = $this->load->controller('common/language');
		$data['language_new'] = $this->load->controller('common/language_new');
		$data['currency'] = $this->load->controller('common/currency');
		$data['currency_new'] = $this->load->controller('common/currency_new');
		$data['search'] = $this->load->controller('common/search');
		//$data['cart'] = $this->load->controller('common/cart');
		$data['menu'] = $this->load->controller('common/menu');

		//var_dump($data['menu']);

        $this->load->model('catalog/menu');

        $data['menu_header_parent'] = array();

        $menu_header_parent_id = $this->model_catalog_menu->getMenuHeaderParentId(0);

        foreach ($menu_header_parent_id as $parent_id) {
            if ($parent_id['status']) {
                $data['menus'] = array();

                $menu_header = $this->model_catalog_menu->getMenuHeader($parent_id['menu_id']);

                if ($menu_header) {
                    foreach ($menu_header as $menu) {
                        if ($menu['status']) {
                            $data['menus'][] = array(
                                'name' => $menu['name'],
                                'href' => $menu['href']
                            );
                        }
                    }
                }

                $data['menu_header_parent'][] = array(
                    'name' => $parent_id['name'],
                    'href' => $parent_id['href'],
                    'child' => $data['menus']
                );
            }
        }

        if ($this->request->get['route'] == 'error/not_found' ||
            $this->request->get['route'] == 'product/search' ||
            $this->request->get['route'] == 'product/product' ||
            $this->request->get['route'] == 'account/account' ||
            $this->request->get['route'] == 'account/wishlist' ||
            $this->request->get['route'] == 'information/project' ||
            $this->request->get['route'] == 'account/register' ||
            $this->request->get['route'] == 'account/register_confirm' ||
            $this->request->get['route'] == 'account/login_confirm' ||
            $this->request->get['route'] == 'extension/tltblog/tltblog_seo' ||
            $this->request->get['route'] == 'extension/tltblog/tltblog' ||
            $this->request->get['route'] == 'extension/tltblog/tlttag' ||
            $this->request->get['route'] == 'checkout/success' ||
            $this->request->get['route'] == 'checkout/step1checkout' ||
            $this->request->get['route'] == 'account/login' ||
            $this->request->get['route'] == 'account/order'
        ) {
            $data['class'] = ' wrapper__content--white';
        } else {
            $data['class'] = '';
        }

        if ($this->request->get['route'] == 'common/home') {
            $data['home_class'] = 'home';
        } else {
            $data['home_class'] = '';
        }

		return $this->load->view('common/header', $data);
	}
}
