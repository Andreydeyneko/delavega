<?php
class ControllerCommonMenu extends Controller {
	public function index() {
		$this->load->language('common/menu');

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);



					$children_data[] = array(
						'name'      => $child['name'],
						'image'     => 'image/' . $child['image'],
						'image_menu' => 'image/' . $child['image_menu'],
						'href'      => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		$data = $this->dataTreatment($data);

		return $this->load->view('common/menu', $data);
	}
	/*
	функция для корректировки урлов в гдавном меню
	заменяет:
	vsi-lizhka на lizhka
	vsi-divany на divany
	*/
	public function dataTreatment($data){
        foreach ($data['categories'] as $key => $cat){
            if(!empty($cat['children'])){
                if($cat['name'] == 'Accessories' || $cat['name'] == 'Аксессуары' || $cat['name'] == 'Аксесуари' || $cat['name'] == 'Кровати' || $cat['name'] == 'Диваны' || $cat['name'] == 'Ліжка' || $cat['name'] == 'Дивани' || $cat['name'] == 'Beds' || $cat['name'] == 'Sofas'){
                    $data['categories'][$key]['dropdown_only'] = true;
                }else{
                    $data['categories'][$key]['dropdown_only'] = false;
                }
                foreach ($cat['children'] as $key1 => $child){
                    if(strpos($child['href'], 'lizhka/vsi-lizhka') !== false){
                        $new_href = str_replace('lizhka/vsi-lizhka', 'lizhka', $child['href']);
                        $data['categories'][$key]['children'][$key1]['href'] = $new_href;
                    }elseif(strpos($child['href'], 'divani/vsi-divany') !== false){
                        $new_href = str_replace('divani/vsi-divany', 'divani', $child['href']);
                        $data['categories'][$key]['children'][$key1]['href'] = $new_href;
                    }elseif(strpos($child['href'], 'krovati/vse-krovati') !== false){
                        $new_href = str_replace('krovati/vse-krovati', 'krovati', $child['href']);
                        $data['categories'][$key]['children'][$key1]['href'] = $new_href;
                    }elseif(strpos($child['href'], 'divany/vse-divany') !== false){
                        $new_href = str_replace('divany/vse-divany', 'divany', $child['href']);
                        $data['categories'][$key]['children'][$key1]['href'] = $new_href;
                    }elseif(strpos($child['href'], 'beds/all-beds') !== false){
                        $new_href = str_replace('beds/all-beds', 'beds', $child['href']);
                        $data['categories'][$key]['children'][$key1]['href'] = $new_href;
                    }elseif(strpos($child['href'], 'sofas/all-sofas') !== false){
                        $new_href = str_replace('sofas/all-sofas', 'sofas', $child['href']);
                        $data['categories'][$key]['children'][$key1]['href'] = $new_href;
                    }
                }
            }
        }
        return $data;
    }
}
