<?php

class ControllerCommonFooter extends Controller
{

    private $error = array();

    public function index()
    {

        $this->load->language('common/footer');

        $this->load->model('catalog/menu');

        $data['menu_parent'] = array();

        $data['bIndexBot'] = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false);

        $menu_parent_id = $this->model_catalog_menu->getMenuFooterParentId(0);

        foreach ($menu_parent_id as $parent_id) {

            $data['menus'] = array();

            $menu_footer = $this->model_catalog_menu->getMenuFooter($parent_id['menu_id']);

            if ($menu_footer) {
                foreach ($menu_footer as $menu) {

                    if($menu['menu_id'] == 16){
                        $data['contact_href'] = $menu['href'];
                    }
                    $data['menus'][] = array(
                        'name' => $menu['name'],
                        'href' => $menu['href']
                    );
                }
            }

            $data['menu_parent'][] = array(
                'name' => $parent_id['name'],
                'child' => $data['menus']
            );
        }
        
        if (!strstr(strtolower($_SERVER['HTTP_USER_AGENT']), "googlebot")) {

            if (!isset($this->session->data['ip'])) {
                $this->session->data['ip'] = $_SERVER['REMOTE_ADDR']; // the IP address to query
                $query = json_decode(file_get_contents('http://api.ipapi.com/' . $this->session->data['ip'] . '?access_key=' . $this->config->get('config_ipapi') . '&format=1'));
                $this->session->data['region_code'] = $query->region_code;
            } elseif ($this->session->data['ip'] != $_SERVER['REMOTE_ADDR']) {
                $this->session->data['ip'] = $_SERVER['REMOTE_ADDR']; // the IP address to query
                $query = json_decode(file_get_contents('http://api.ipapi.com/' . $this->session->data['ip'] . '?access_key=' . $this->config->get('config_ipapi') . '&format=1'));
                $this->session->data['region_code'] = $query->region_code;
            }
            if (isset($this->session->data['region_code']) && $this->session->data['region_code'] != '') {
                $this->load->model('localisation/banner_zone');
                $this->load->model('tool/image');
                $country_code = $this->model_localisation_banner_zone->getBannerZonebyCode($this->session->data['region_code']);
                if (count($country_code)) {
                    $country_code['image'] = $this->model_tool_image->resize($country_code['image'], 700, 400);
                    $data['banner_zone'] = $country_code;
                }
            }


        }


        $data['home'] = $this->url->link('common/home');
        $data['facebook'] = $this->config->get('config_fb');
        $data['instagram'] = $this->config->get('config_inst');
        $data['pinterest'] = $this->config->get('config_pint');
        $data['address'] = $this->config->get('config_address')[$this->config->get('config_language_id')];
        $data['email'] = $this->config->get('config_email');
        $data['worktime'] = $this->config->get('config_open')[$this->config->get('config_language_id')];
        $data['showroom_phone'] = $this->config->get('config_kiev_phone');
        $data['privacy_policy'] = $this->url->link('information/information', 'information_id=12');
        $data['text_cookies_text'] = sprintf($this->language->get('text_cookies_text'), $this->url->link('information/information', 'information_id=12'));

        $data['technical_question'] = $this->config->get('config_question');
        $data['quality'] = $this->config->get('config_quality');

        $data['contact'] = $this->url->link('information/contact');
        $data['return'] = $this->url->link('account/return/add', '', true);
        $data['sitemap'] = $this->url->link('information/sitemap');
        $data['tracking'] = $this->url->link('information/tracking');
        $data['manufacturer'] = $this->url->link('product/manufacturer');
        $data['voucher'] = $this->url->link('account/voucher', '', true);
        $data['affiliate'] = $this->url->link('affiliate/login', '', true);
        $data['special'] = $this->url->link('product/special');
        $data['account'] = $this->url->link('account/account', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['newsletter'] = $this->url->link('account/newsletter', '', true);

        $data['login_href'] = $this->url->link('account/login', '', true);
        $data['register_href'] = $this->url->link('account/register', '', true);

        $data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        $data['instruction'] = sprintf($this->language->get('text_instruction'), $data['login_href'], $data['register_href']);

        $data['scripts'] = $this->document->getScripts('footer');

        return $this->load->view('common/footer', $data);
    }

    public function getPromoCode()
    {
        $this->load->model('localisation/banner_zone');
        $json = array();
        $data = array();
        $coupon = array();
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validategetPromoCode($this->request->post)) {

            if (!$this->customer->isLogged()) {
                if ($customer_id = $this->model_localisation_banner_zone->getCustomerByPhone($this->request->post['form_phone'])) {

                    if ($coupon_id = $this->model_localisation_banner_zone->getPromoCodeByCustomer($customer_id)) {
                        $json['error']['coupon'] = $this->language->get('error_promo');
                    } else {
                        $coupon = array(
                            'name' => $this->request->post['form_phone'],
                            'code' => $this->generateCoupon(),
                            'discount' => $this->request->post['discount'],
                            'type' => 'P',
                            'total' => 0,
                            'logged' => 0,
                            'shipping' => 0,
                            'date_start' => date("Y-m-d"),
                            'date_end' => date("Y-m-d", strtotime(date("Y-m-d") . ' + 30 days')),
                            'uses_total' => 1,
                            'uses_customer' => 1,
                            'status' => 1
                        );

                        $coupon_id = $this->model_localisation_banner_zone->addCoupon($coupon);
                        $this->model_localisation_banner_zone->addCouponToCustomer($coupon_id, $customer_id);
                        // $result = $this->turbosms->sendCoupon($this->request->post['form_phone'], $coupon['code'],$coupon['discount']);


                    }
                } else {
                    $this->load->model('account/customer');
                    $password = $this->generatePassword();

                    $register_data = array(
                        'firstname' => $this->request->post['form_name'],
                        'lastname' => '',
                        'telephone' => $this->request->post['form_phone'],
                        'email' => '',
                        'password' => $password
                    );

                    // $this->turbosms->send($this->request->post['form_phone'], $password);
                    $customer_id = $this->model_account_customer->addCustomer($register_data);
                    $this->customer->loginByPhone($register_data['telephone'], $register_data['password']);

                    unset($this->session->data['guest']);
                    unset($register_data);

                    $coupon = array(
                        'name' => $this->request->post['form_phone'],
                        'code' => $this->generateCoupon(),
                        'discount' => $this->request->post['discount'],
                        'type' => 'P',
                        'total' => 0,
                        'logged' => 0,
                        'shipping' => 0,
                        'date_start' => date("Y-m-d"),
                        'date_end' => date("Y-m-d", strtotime(date("Y-m-d") . ' + 30 days')),
                        'uses_total' => 1,
                        'uses_customer' => 1,
                        'status' => 1
                    );

                    $coupon_id = $this->model_localisation_banner_zone->addCoupon($coupon);
                    $this->model_localisation_banner_zone->addCouponToCustomer($coupon_id, $customer_id);
                    //  $result = $this->turbosms->sendCoupon($this->request->post['form_phone'], $coupon['code'],$coupon['discount']);

                }
            } else {
                if ($coupon_id = $this->model_localisation_banner_zone->getPromoCodeByCustomer($this->customer->isLogged())) {
                    $json['error']['coupon'] = $this->language->get('error_promo');
                } else {
                    $coupon = array(
                        'name' => $this->request->post['form_phone'],
                        'code' => $this->generateCoupon(),
                        'discount' => $this->request->post['discount'],
                        'type' => 'P',
                        'total' => 0,
                        'logged' => 0,
                        'shipping' => 0,
                        'date_start' => date("Y-m-d"),
                        'date_end' => date("Y-m-d", strtotime(date("Y-m-d") . ' + 30 days')),
                        'uses_total' => 1,
                        'uses_customer' => 1,
                        'status' => 1
                    );

                    $coupon_id = $this->model_localisation_banner_zone->addCoupon($coupon);
                    $this->model_localisation_banner_zone->addCouponToCustomer($coupon_id, $this->customer->isLogged());
                    //   $result = $this->turbosms->sendCoupon($this->request->post['form_phone'], $coupon['code'],$coupon['discount']);

                }

            }
            $data = array(
                'form_phone' =>$this->request->post['form_phone'],
                'form_name' => $this->request->post['form_name'],
                'code' => $coupon['code'],
                'discount' => $this->request->post['discount'],
            );
        }


        if (isset($this->error['form_phone'])) {
            $json['error']['form_phone'] = $this->error['form_phone'];
        }
        if (isset($this->error['form_name'])) {
            $json['error']['form_name'] = $this->error['form_name'];
        }
        if (!isset($json['error'])) {
            $json['success'] = true;
            $this->sendToAdmin($data);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


    public function generatePassword()
    {

        $chars = "1234567890";
        $max = 4;

        $size = StrLen($chars) - 1;

        $password = null;

        while ($max--)
            $password .= $chars[rand(0, $size)];

        return $password;
    }


    public function generateCoupon()
    {
        $this->load->model('localisation/banner_zone');

        $chars = "1234567890";
        $max = 6;

        $size = StrLen($chars) - 1;

        $coupon = null;

        while ($max--)
            $coupon .= $chars[rand(0, $size)];

        if ($this->model_localisation_banner_zone->checkCoupon($coupon)) {
            $this->generateCoupon();
        }
        return $coupon;
    }

    protected function validategetPromoCode()
    {
        if (strlen(preg_replace('/[^0-9]/', '', $this->request->post['form_phone'])) < 12) {
            $this->error['form_phone'] = $this->language->get('error_phone');
        }
        if ((utf8_strlen(trim($this->request->post['form_name'])) < 2) || (utf8_strlen(trim($this->request->post['form_name'])) > 32)) {
            $this->error['form_name'] = $this->language->get('error_name');
        }
        return !$this->error;

    }

    public function feedbackFormSend()
    {

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST'/* && $this->validateContactForm($this->request->post) && $this->request->post['g-recaptcha-response']*/) {

            $this->load->language('mail/contact_form');

            if (isset($this->request->post['form_name'])) {
                $data['form_name'] = $this->request->post['form_name'];
            }

            if (isset($this->request->post['form_email'])) {
                $data['form_email'] = $this->request->post['form_email'];
            }

            if (isset($this->request->post['form_phone'])) {
                $data['form_phone'] = $this->request->post['form_phone'];
            }

            if (isset($this->request->post['form_message'])) {
                $data['form_message'] = $this->request->post['form_message'];
            };

           /* if (isset($this->request->files['form_file'])) {
                $uploaddir = '/home/delave00/zorro.kyiv.ua/www/system/storage/upload/tmp/';
                $uploadfile = $uploaddir . basename($this->request->files['form_file']['name']);

                if (move_uploaded_file($this->request->files['form_file']['tmp_name'], $uploadfile)) {
                    $pathToFile = $uploadfile;
                }
                $data['form_file'] = $pathToFile;
            }*/

            $json['success'] = true;


            $subject = $this->language->get('text_subject_feedback');

            $message = sprintf($this->language->get('text_name'), $data['form_name']) . "\n";
            $message .= sprintf($this->language->get('text_email'), $data['form_email']) . "\n";
            $message .= sprintf($this->language->get('text_phone'), $data['form_phone']) . "\n";


            if ($data['form_message']) {
                $message .= $this->language->get('text_message') . ': ' . $data['form_message'] . "\n";
            }

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
            if($data['form_file']){
                $mail->addAttachment($data['form_file']);
            }
            $mail->setSubject($subject);
            $mail->setText($message);
            $mail->send();

            // Send to additional alert emails
            $emails = explode(',', $this->config->get('config_alert_email'));

            foreach ($emails as $email) {
                if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $mail->setTo($email);
                    $mail->send();
                }
            }

        }

        if (isset($this->error['form_name'])) {
            $json['error']['form_name'] = $this->error['form_name'];
        }

        if (isset($this->error['form_phone'])) {
            $json['error']['form_phone'] = $this->error['form_phone'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function contactFormSend()
    {

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateContactForm($this->request->post)) {

            $this->load->language('mail/contact_form');

            if (isset($this->request->post['form_name'])) {
                $data['form_name'] = $this->request->post['form_name'];
            }

            if (isset($this->request->post['form_email'])) {
                $data['form_email'] = $this->request->post['form_email'];
            }

            if (isset($this->request->post['form_phone'])) {
                $data['form_phone'] = $this->request->post['form_phone'];
            }

            $json['success'] = true;


            $subject = $this->language->get('text_subject');

            $message = sprintf($this->language->get('text_name'), $data['form_name']) . "\n";
            $message .= sprintf($this->language->get('text_phone'), $data['form_phone']) . "\n";

            if ($this->request->post['form_email']) {
                $message .= sprintf($this->language->get('text_email'), $data['form_email']) . "\n";
            }

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
            $mail->setSubject($subject);
            $mail->setText($message);
            $mail->send();

            // Send to additional alert emails
            $emails = explode(',', $this->config->get('config_alert_email'));

            foreach ($emails as $email) {
                if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $mail->setTo($email);
                    $mail->send();
                }
            }

        }

        if (isset($this->error['form_name'])) {
            $json['error']['form_name'] = $this->error['form_name'];
        }

        if (isset($this->error['form_phone'])) {
            $json['error']['form_phone'] = $this->error['form_phone'];
        }

//        if (isset($this->error['form_email'])) {
//            $json['error']['form_email'] = $this->error['form_email'];
//        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function subscribeForm()
    {

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateSubscribeForm($this->request->post)) {

            if (isset($this->request->post['subscribe_email'])) {
                $data['subscribe_email'] = $this->request->post['subscribe_email'];
            }

            $json['success'] = true;
            $this->load->model('account/customer');
            $this->model_account_customer->addSubscriber($this->request->post);

            // Add subscriber email into MailerLite
            $groupsApi = (new MailerLiteApi\MailerLite(ML_KEY))->groups();
            $subscriber = [
                'email' => $this->request->post['subscribe_email'],
                'type' => 'active',
            ];
            $response = $groupsApi->addSubscriber(ML_GROUP_ID, $subscriber);
        }

        if (isset($this->error['subscribe_email'])) {
            $json['error']['subscribe_email'] = $this->error['subscribe_email'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateSubscribeForm()
    {

        if ((utf8_strlen($this->request->post['subscribe_email']) > 96) || !filter_var($this->request->post['subscribe_email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['subscribe_email'] = $this->language->get('error_email');
        }

        return !$this->error;

    }

    protected function sendToAdmin($data)
    {
        $this->load->language('mail/contact_form');

        $subject = $this->language->get('subject');

        $message = $this->language->get('text_body') . "\n";
        $message .= sprintf($this->language->get('text_name'), $data['form_name']) . "\n";
        $message .= sprintf($this->language->get('text_phone'), $data['form_phone']) . "\n";
        $message .= sprintf($this->language->get('text_code'), $data['code'], $data['discount']) . "\n";


        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);
        $mail->setText($message);
        $mail->send();
    }

    protected function validateContactForm()
    {

        if ((utf8_strlen(trim($this->request->post['form_name'])) < 2) || (utf8_strlen(trim($this->request->post['form_name'])) > 32)) {
            $this->error['form_name'] = $this->language->get('error_name');
        }
        if (strlen(preg_replace('/[^0-9]/', '', $this->request->post['form_phone'])) < 12) {
            $this->error['form_phone'] = $this->language->get('error_phone');
        }

//        if ((utf8_strlen($this->request->post['form_email']) > 96) || !filter_var($this->request->post['form_email'], FILTER_VALIDATE_EMAIL)) {
//            $this->error['form_email'] = $this->language->get('error_email');
//        }

        return !$this->error;

    }
}
