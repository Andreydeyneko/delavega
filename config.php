<?php
// HTTP
define('HTTP_SERVER', 'https://' . $_SERVER['HTTP_HOST'] . '/');

// HTTPS
define('HTTPS_SERVER', 'https://' . $_SERVER['HTTP_HOST'] . '/');
define('DOMAIN_URL', 'https://' . $_SERVER['HTTP_HOST']);

// DIR
define('DIR_ROOT', $_SERVER['DOCUMENT_ROOT']);


define('DIR_APPLICATION', DIR_ROOT . 'catalog/');
define('DIR_SYSTEM', DIR_ROOT . 'system/');
define('DIR_IMAGE', DIR_ROOT . 'image/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_STORAGE', DIR_ROOT . 'system/storage/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', '/system/storage/download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'delavega.mysql.tools');
define('DB_USERNAME', 'delavega_speed');
define('DB_PASSWORD', '1JM-n8ao1-');
define('DB_DATABASE', 'delavega_speed');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// API MailerLite
define('ML_KEY', 'fb6a54122c17e2a1dae200b3dfdda189');
define('ML_GROUP_ID', '102803558');

// ERRORS
ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('html_errors', 1);

// CACHE
define('CACHE_HOSTNAME', '/home/delavega/.system/redis.sock');
define('CACHE_PORT', 0);
define('CACHE_PREFIX', 'oc_');

// CACHE
define('CACHE_HOSTNAME', '/home/delavega/.system/memcache/socket');
define('CACHE_PORT', '0');
define('CACHE_PREFIX', 'oc_');