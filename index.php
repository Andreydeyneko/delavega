<?php
// Version
define('VERSION', '3.0.2.0');
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));
// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

if (file_exists($li = DIR_APPLICATION.'/controller/extension/lightning/gamma.php')) require_once($li); //Lightning

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// MailerLite
require_once('vendor/autoload.php');

start('catalog');