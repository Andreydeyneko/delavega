<?php
class ModelSaleOwnOrder extends Model {
    public function getOwnOrders() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "own_order");

        return $query->rows;
    }

    public function getOwnProduct($product_id) {
        $query = $this->db->query("SELECT pd.name FROM " . DB_PREFIX . "product_description pd
        LEFT JOIN " . DB_PREFIX . "own_order ow 
        ON (pd.product_id = ow.product_id) 
        WHERE ow.product_id = '" . (int)$product_id . "' 
        AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row['name'];
    }

    public function getTotalOwnOrders() {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "own_order`";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }
}