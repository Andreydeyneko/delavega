<?php
class ModelSaleDownload3dOrder extends Model {
    public function getDownload_3d_Orders($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "download_3d ";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " ORDER BY id DESC";

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function deleteDownload_3d_Orders($id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "download_3d WHERE id = '" . (int)$id . "'");
    }

    public function getTotalDownload_3d_Orders() {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "download_3d`";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function download( $offset=null, $rows=null, $min_id=null, $max_id=null) {
        // we use our own error handler
        global $registry;
        $registry = $this->registry;
        set_error_handler('error_handler_for_export_import',E_ALL);
        register_shutdown_function('fatal_error_shutdown_handler_for_export_import');

        // Use the PHPExcel package from https://github.com/PHPOffice/PHPExcel
        $cwd = getcwd();
        $dir = (strcmp(VERSION,'3.0.0.0')>=0) ? 'library/export_import' : 'PHPExcel';
        chdir( DIR_SYSTEM.$dir );
        require_once( 'Classes/PHPExcel.php' );
        PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_ExportImportValueBinder() );
        chdir( $cwd );

        // find out whether all data is to be downloaded
        $all = !isset($offset) && !isset($rows) && !isset($min_id) && !isset($max_id);

        // Memory Optimization
        if ($this->config->get( 'export_import_settings_use_export_cache' )) {
            $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
            $cacheSettings = array( 'memoryCacheSize'  => '16MB' );
            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
        }

        try {
            // set appropriate timeout limit
            set_time_limit( 1800 );
            // create a new workbook
            $workbook = new PHPExcel();

            // set some default styles
            $workbook->getDefaultStyle()->getFont()->setName('Arial');
            $workbook->getDefaultStyle()->getFont()->setSize(10);
            //$workbook->getDefaultStyle()->getAlignment()->setIndent(0.5);
            $workbook->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $workbook->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $workbook->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);

            // pre-define some commonly used styles
            $box_format = array(
                'fill' => array(
                    'type'      => PHPExcel_Style_Fill::FILL_SOLID,
                    'color'     => array( 'rgb' => 'F0F0F0')
                ),
            );
            $text_format = array(
                'numberformat' => array(
                    'code' => PHPExcel_Style_NumberFormat::FORMAT_TEXT
                ),

            );
            $price_format = array(
                'numberformat' => array(
                    'code' => '######0.00'
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,

                )
            );
            $weight_format = array(
                'numberformat' => array(
                    'code' => '##0.00'
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                )
            );

            // create the worksheets
            $worksheet_index = 0;
            $workbook->createSheet();
            $workbook->setActiveSheetIndex($worksheet_index++);
            $worksheet = $workbook->getActiveSheet();
            $worksheet->setTitle( 'Download3d' );
            $this->populateDownload3dWorksheet( $worksheet, $box_format, $text_format );
            $worksheet->freezePaneByColumnAndRow( 1, 2 );

            $workbook->setActiveSheetIndex(0);

            // redirect output to client browser
            $datetime = date('Y-m-d');

            $filename = 'download3d-'.$datetime.'.xlsx';

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
            $objWriter->setPreCalculateFormulas(false);
            $objWriter->save('php://output');

            // Clear the spreadsheet caches
            $this->clearSpreadsheetCache();
            exit;

        } catch (Exception $e) {
            $errstr = $e->getMessage();
            $errline = $e->getLine();
            $errfile = $e->getFile();
            $errno = $e->getCode();
            $this->session->data['export_import_error'] = array( 'errstr'=>$errstr, 'errno'=>$errno, 'errfile'=>$errfile, 'errline'=>$errline );
            if ($this->config->get('config_error_log')) {
                $this->log->write('PHP ' . get_class($e) . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
            }
            return;
        }
    }
    protected function populateDownload3dWorksheet( &$worksheet, &$box_format, &$text_format ) {
        // Set the column widths
        $j = 0;
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('id'),6)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('name'),20)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('company'),20)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('city'),15)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('site'),30)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('email'),30)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('phone'),20)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('product'),25)+1);
        $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('date_added'),10)+1);


        // The attributes headings row and column styles
        $styles = array();
        $data = array();
        $i = 1;
        $j = 0;
        $data[$j++] = 'id';
        $data[$j++] = 'name';
        $data[$j++] = 'company';
        $data[$j++] = 'city';
        $data[$j++] = 'site';
        $data[$j++] = 'email';
        $data[$j++] = 'phone';
        $data[$j++] = 'product';
        $data[$j++] = 'date_added';

        $worksheet->getRowDimension($i)->setRowHeight(30);
        $this->setCellRow( $worksheet, $i, $data, $box_format );
        $dat= array();
        // The actual attributes values data
        $i += 1;
        $j = 0;
        $options = $this->getDownload_3d_Orders($dat);
        foreach ($options as $row) {
            $worksheet->getRowDimension($i)->setRowHeight(13);
            $data = array();
            $data[$j++] = $row['id'];
            $data[$j++] = $row['name'];
            $data[$j++] = $row['company'];
            $data[$j++] = $row['city'];
            $data[$j++] = $row['site'];
            $data[$j++] = $row['email'];
            $data[$j++] = $row['phone'];
            $data[$j++] = $row['product'];
            $data[$j++] = $row['date_added'];

            $this->setCellRow( $worksheet, $i, $data, $this->null_array, $styles );
            $i += 1;
            $j = 0;
        }
    }

    protected function clearSpreadsheetCache() {
        $files = glob(DIR_CACHE . 'Spreadsheet_Excel_Writer' . '*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    @unlink($file);
                    clearstatcache();
                }
            }
        }
    }
    protected function getLayoutsForCategories() {
        $sql  = "SELECT cl.*, l.name FROM `".DB_PREFIX."category_to_layout` cl ";
        $sql .= "LEFT JOIN `".DB_PREFIX."layout` l ON cl.layout_id = l.layout_id ";
        $sql .= "ORDER BY cl.category_id, cl.store_id;";
        $result = $this->db->query( $sql );
        $layouts = array();
        foreach ($result->rows as $row) {
            $categoryId = $row['category_id'];
            $store_id = $row['store_id'];
            $name = $row['name'];
            if (!isset($layouts[$categoryId])) {
                $layouts[$categoryId] = array();
            }
            $layouts[$categoryId][$store_id] = $name;
        }
        return $layouts;
    }


    protected function setColumnStyles( &$worksheet, &$styles, $min_row, $max_row ) {
        if ($max_row < $min_row) {
            return;
        }
        foreach ($styles as $col=>$style) {
            $from = PHPExcel_Cell::stringFromColumnIndex($col).$min_row;
            $to = PHPExcel_Cell::stringFromColumnIndex($col).$max_row;
            $range = $from.':'.$to;
            $worksheet->getStyle( $range )->applyFromArray( $style, false );
        }
    }


    protected function setCellRow( $worksheet, $row/*1-based*/, $data, &$default_style=null, &$styles=null ) {
        if (!empty($default_style)) {
            $worksheet->getStyle( "$row:$row" )->applyFromArray( $default_style, false );
        }
        if (!empty($styles)) {
            foreach ($styles as $col=>$style) {
                $worksheet->getStyleByColumnAndRow($col,$row)->applyFromArray($style,false);
            }
        }
        $worksheet->fromArray( $data, null, 'A'.$row, true );

    }


    protected function setCell( &$worksheet, $row/*1-based*/, $col/*0-based*/, $val, &$style=null ) {
        $worksheet->setCellValueByColumnAndRow( $col, $row, $val );
        if (!empty($style)) {
            $worksheet->getStyleByColumnAndRow($col,$row)->applyFromArray( $style, false );
        }
    }

}