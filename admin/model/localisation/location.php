<?php
class ModelLocalisationLocation extends Model {
	public function addLocation($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "location SET geocode = '" . $this->db->escape($data['geocode']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', telephone2 = '" . $this->db->escape($data['telephone2'])  . "', sort = '" . $this->db->escape($data['sort']) . "', email = '" . $this->db->escape($data['email']) . "', fax = '" . $this->db->escape($data['fax']) . "', image = '" . $this->db->escape($data['image']) . "'");

        $location_id = $this->db->getLastId();

        foreach ($data['location_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "location_description SET location_id = '" . (int)$location_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', sort = '" . $this->db->escape($data['sort'])  . "', address = '" . $this->db->escape($value['address']) . "', open = '" . $this->db->escape($value['open']) . "', comment = '" . $this->db->escape($value['comment']) . "'");
        }

        return $location_id;
	}

	public function editLocation($location_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "location SET geocode = '" . $this->db->escape($data['geocode']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', telephone2 = '" . $this->db->escape($data['telephone2']) . "', sort = '" . $this->db->escape($data['sort']) . "', email = '" . $this->db->escape($data['email']) . "', fax = '" . $this->db->escape($data['fax']) . "', image = '" . $this->db->escape($data['image']) . "' WHERE location_id = '" . (int)$location_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "location_description WHERE location_id = '" . (int)$location_id . "'");

        foreach ($data['location_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "location_description SET location_id = '" . (int)$location_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', address = '" . $this->db->escape($value['address']) . "', open = '" . $this->db->escape($value['open']) . "', comment = '" . $this->db->escape($value['comment']) . "'");
        }
	}

	public function deleteLocation($location_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = " . (int)$location_id);
        $this->db->query("DELETE FROM " . DB_PREFIX . "location_description WHERE location_id = '" . (int)$location_id . "'");
	}

	public function getLocation($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location l 
		LEFT JOIN " . DB_PREFIX . "location_description ld ON (l.location_id = ld.location_id) 
		WHERE l.location_id = '" . (int)$location_id . "' AND ld.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getLocations($data = array()) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location l 
		 LEFT JOIN " . DB_PREFIX . "location_description ld 
		 ON (l.location_id = ld.location_id) 
		 WHERE ld.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY l.sort ASC");

        return $query->rows;
	}

    public function getLocationDescriptions($location_id) {
        $location_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location_description WHERE location_id = '" . (int)$location_id . "'");

        foreach ($query->rows as $result) {
            $location_description_data[$result['language_id']] = array(
                'name'              => $result['name'],
                'address'           => $result['address'],
                'open'			    => $result['open'],
                'comment'           => $result['comment']
            );
        }

        return $location_description_data;
    }

	public function getTotalLocations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "location");

		return $query->row['total'];
	}
}
