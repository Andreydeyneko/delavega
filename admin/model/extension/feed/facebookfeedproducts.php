<?php 
class ModelExtensionFeedFacebookFeedProducts extends Model {
	public function install() {
        
		$this->db->query("
			CREATE TABLE `" . DB_PREFIX . "google_product_category` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`google_category_id` int(11) NOT NULL,
				`google_category_name` varchar(255) NOT NULL,
				`google_category_parent` int(11) NOT NULL,
				KEY `google_category_id` (`google_category_id`),
				PRIMARY KEY `id` (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
		");
        
        $this->db->query("
			CREATE TABLE `" . DB_PREFIX . "google_category` (
				`google_category_id` int(11) NOT NULL,
                `google_main_category_id` int(11) NOT NULL,
                `category_id` int(11) NOT NULL,
				KEY `google_category_id` (`google_category_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
		");

	}

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "google_product_category`;");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "google_category`;");

	}

	public function clearGoogleCategorys() {
		$this->db->query("TRUNCATE TABLE `" . DB_PREFIX . "google_product_category`;");

	}
    
    public function getGoogleCategorysByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$category_id . "'");

	return $query->row;
	}
    
    public function getGoogleCategoryByName($parent_name) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_name = '" . $this->db->escape($parent_name) . "'");

	return $query->row;
	}
    public function getGoogleCategoryById($google_category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_id = '" . $this->db->escape($google_category_id) . "'");

	return $query->row;
    }
    
    public function getGoogleMainCategory() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_parent = 0 ");

	return $query->rows;
    }
    public function getGoogleCategorys($data) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_parent = '" . (int)$this->db->escape($data['google_category_id']) . "' AND (google_category_name LIKE '%" . $this->db->escape($data['google_category_name']) . "%' OR google_category_id LIKE '" . $this->db->escape($data['google_category_name']) . "%')");

	return $query->rows;
    }
    
    public function updateGoogleCategorys($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "google_product_category SET google_category_id = '" . (int)$this->db->escape($data['google_category_id']) . "', google_category_name = '" . $this->db->escape($data['google_category_name']) . "', google_category_parent = '" . (int)$this->db->escape($data['google_category_parent']) . "'");

 	}
    
    public function saveGoogleCategory($data) {
        if ($data['google_main_category_id'] != 0 ){
            $this->db->query("DELETE FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "google_category SET category_id = '" . (int)$this->db->escape($data['category_id']) . "', google_main_category_id = '" . (int)$this->db->escape($data['google_main_category_id']) . "'");
        }elseif ($data['google_category_id'] !=0){
            $this->db->query("UPDATE " . DB_PREFIX . "google_category SET google_category_id = '" . (int)$this->db->escape($data['google_category_id']) . "' WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "'");
        }else{
            $this->db->query("DELETE FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "'");
        }

 	}
}
?>