<?php
class ModelCatalogProject extends Model {
    public function addProject($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "project SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

        $project_id = $this->db->getLastId();

        foreach ($data['project_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "project_description SET project_id = '" . (int)$project_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', designer = '" . $this->db->escape($value['designer']) . "'");
        }

        if (isset($data['cover_image'])) {
            foreach ($data['cover_image'] as $image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_image SET project_id = '" . (int)$project_id . "', title = '" . $this->db->escape($image['title']) . "', image = '" . $this->db->escape($image['image']) . "', sort_order = '" . (int)$image['sort_order'] . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "project_related WHERE project_id = '" . (int)$project_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_related SET project_id = '" . (int)$project_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "project_related WHERE project_id = '" . (int)$related_id . "' AND related_id = '" . (int)$project_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_related SET project_id = '" . (int)$related_id . "', related_id = '" . (int)$project_id . "'");
            }
        }

        $this->cache->delete('project');

        return $project_id;
    }

    public function editProject($project_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "project SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE project_id = '" . (int)$project_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "project_description WHERE project_id = '" . (int)$project_id . "'");

        foreach ($data['project_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "project_description SET project_id = '" . (int)$project_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', designer = '" . $this->db->escape($value['designer']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "project_image WHERE project_id = '" . (int)$project_id . "'");

        if (isset($data['cover_image'])) {
            foreach ($data['cover_image'] as $image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_image SET project_id = '" . (int)$project_id . "', title = '" . $this->db->escape($image['title']) . "', image = '" . $this->db->escape($image['image']) . "', sort_order = '" . (int)$image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "project_related WHERE project_id = '" . (int)$project_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "project_related WHERE related_id = '" . (int)$project_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "project_related WHERE project_id = '" . (int)$project_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_related SET project_id = '" . (int)$project_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "project_related WHERE project_id = '" . (int)$related_id . "' AND related_id = '" . (int)$project_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_related SET project_id = '" . (int)$related_id . "', related_id = '" . (int)$project_id . "'");
            }
        }

        $this->cache->delete('project');
    }

    public function deleteProject($project_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "project` WHERE project_id = '" . (int)$project_id . "'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "project_description` WHERE project_id = '" . (int)$project_id . "'");

        $this->cache->delete('project');
    }

    public function getProject($project_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "project WHERE project_id = '" . (int)$project_id . "'");

        return $query->row;
    }

    public function getProjects($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "project i LEFT JOIN " . DB_PREFIX . "project_description id ON (i.project_id = id.project_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

            $sort_data = array(
                'id.title',
                'i.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY id.title";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $project_data = $this->cache->get('project.' . (int)$this->config->get('config_language_id'));

            if (!$project_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project i LEFT JOIN " . DB_PREFIX . "project_description id ON (i.project_id = id.project_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

                $project_data = $query->rows;

                $this->cache->set('project.' . (int)$this->config->get('config_language_id'), $project_data);
            }

            return $project_data;
        }
    }

    public function getProjectDescriptions($project_id)
    {
        $project_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_description WHERE project_id = '" . (int)$project_id . "'");

        foreach ($query->rows as $result) {
            $project_description_data[$result['language_id']] = array(
                'title' => $result['title'],
                'designer' => $result['designer']
            );
        }

        return $project_description_data;
    }

    public function getTotalProjects() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project");

        return $query->row['total'];
    }

    public function getNewInfoId() {
        $query = $this->db->query("SHOW TABLE STATUS WHERE `Name` = '" . DB_PREFIX . "project'");

        return $query->row['Auto_increment'];
    }

    public function getProjectGallery($project_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_image WHERE project_id = '" . (int)$project_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getProjectRelated($project_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_related WHERE project_id = '" . (int)$project_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }

        return $product_related_data;
    }

    public function editProjectPage($data) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "project_page");

        foreach ($data['project_page_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "project_page SET language_id = '" . (int)$language_id . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }
    }

    public function getProjectPageDescriptions() {
        $project_page_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_page");

        foreach ($query->rows as $result) {
            $project_page_description_data[$result['language_id']] = array(
                'meta_title'       => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword'     => $result['meta_keyword'],
            );
        }

        return $project_page_description_data;
    }
}