<?php
// Heading
$_['heading_title']          = 'Новости';

// Text
$_['text_success']           = 'Новость успешно изменена!';
$_['text_list']              = 'Список новостей';
$_['text_add']               = 'Добавить новость';
$_['text_edit']              = 'Изменить новость';
$_['text_default']           = 'По умолчанию';
$_['text_pro_only']          = 'Only in Pro Version';

// Column
$_['column_title']           = 'Заголовок';
$_['column_tags']            = 'Категории';
$_['column_image']           = 'Изображение';
$_['column_status']          = 'Статус';
$_['column_sort_order']	     = 'Порядок сортировки';
$_['column_action']          = 'Действие';

// Entry
$_['entry_title']            = 'Заголовок';
$_['entry_author']           = 'Автор';
$_['entry_intro']            = 'Превью';
$_['entry_description']      = 'Описание';
$_['entry_meta_title'] 	     = 'Мета тег Title';
$_['entry_meta_keyword'] 	 = 'Мета тег Keywords';
$_['entry_meta_description'] = 'Мета тег Description';
$_['entry_keyword']          = 'SEO url';
$_['entry_bottom']           = 'Отображать первой в списке';
$_['entry_show_title'] 		 = 'Отображать заголовок';
$_['entry_show_description'] = 'Отображать описание';
$_['entry_show_in_sitemap']  = 'Show in Sitemap';
$_['entry_store']            = 'Stores';
$_['entry_image']            = 'Изображение';
$_['entry_image_social']     = 'Image for Social Networks';
$_['entry_required']         = 'Обязательно';
$_['entry_status']           = 'Статус';
$_['entry_date']       		 = 'Дата';
$_['entry_date_start']     	 = 'Date Start';
$_['entry_date_end']       	 = 'Date End';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_related']          = 'Related Products';
$_['entry_layout']           = 'Layout Override';
$_['entry_tags']          	 = 'Article Tags';

// Help
$_['help_keyword']           = 'Не используйте пробелы, вместо этого замените пробелы - и убедитесь, что ключевое слово уникально глобально.';
$_['help_bottom']            = 'Большой блок в списке новостей';
$_['help_show_title']    	 = 'Заголовок на странице новости';
$_['help_show_description']  = 'Описание на странице новости';
$_['help_datetime']          = 'YYYY-MM-DD HH:MM';
$_['help_date']          	 = 'YYYY-MM-DD';
$_['help_related']           = '(Autocomplete)';
$_['help_tags']          	 = '(Autocomplete)';
$_['help_image_social']      = 'Special image for Twitter and Facebook. Image should have aspect ratio ~2:1 (i.e. 1024x512 or similar).';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify articles!';
$_['error_config']           = 'Warning: Please install and configure TLT Blog modules! You can do it <a href="%s" >here</a>.';
$_['error_title']            = 'Article title must be greater than 3 and less than 255 characters!';
$_['error_intro']            = 'Introduction must be greater than 3 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
$_['error_date']             = 'Creation date required!';