<?php
// Heading
$_['heading_title']    = 'Facebook Feed Products';

// Text   
$_['text_feed']        = 'Facebook Feed Products';
$_['text_success']     = 'uccess: You have modified Facebook feed!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_data_feed']  = 'Data Feed Url:';
$_['entry_google_product_category']  = 'Data Google product category Url:';
$_['entry_product_category']  = 'Product category';
$_['entry_google_main_product_category']  = 'Google main product category';
$_['entry_google_all_product_category']  = 'Google all product category';
// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Facebook feed!';
?>