<?php
class ControllerCatalogProject extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/project');

        $this->getList();
    }

    public function add() {
        $this->load->language('catalog/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/project');
        $this->load->model('extension/kcf');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
            /* ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------- */
            if(isset($this->request->post['kfc_feeld']) && $this->request->post['kfc_feeld']){
                $kcf_type = 3; //значит категории
                $this->model_extension_kcf->getKcfUpdateFeeld($kcf_type, $this->request->get['project_id'], $this->request->post['kfc_feeld']);
            }
            /* ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ */
            /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
            $this->model_catalog_project->addProject($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('catalog/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/project');
        $this->load->model('extension/kcf');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
            /* ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------- */
            if(isset($this->request->post['kfc_feeld']) && $this->request->post['kfc_feeld']){
                $kcf_type = 3; //значит категории
                $this->model_extension_kcf->getKcfUpdateFeeld($kcf_type, $this->request->get['project_id'], $this->request->post['kfc_feeld']);
            }
            /* ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ */
            /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

            $this->model_catalog_project->editProject($this->request->get['project_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/project');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $project_id) {
                $this->model_catalog_project->deleteProject($project_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'id.title';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('catalog/project/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/project/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['projects'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $project_total = $this->model_catalog_project->getTotalProjects();

        $results = $this->model_catalog_project->getProjects($filter_data);

        foreach ($results as $result) {
            $data['projects'][] = array(
                'project_id'     => $result['project_id'],
                'title'          => $result['title'],
                'sort_order'     => $result['sort_order'],
                'edit'           => $this->url->link('catalog/project/edit', 'user_token=' . $this->session->data['user_token'] . '&project_id=' . $result['project_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_title'] = $this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . '&sort=id.title' . $url, true);
        $data['sort_sort_order'] = $this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . '&sort=i.sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $project_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($project_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($project_total - $this->config->get('config_limit_admin'))) ? $project_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $project_total, ceil($project_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/project_list', $data));
    }

    protected function getForm() {
        $data['text_form'] = !isset($this->request->get['project_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['title'])) {
            $data['error_title'] = $this->error['title'];
        } else {
            $data['error_title'] = array();
        }

        if (isset($this->error['meta_title'])) {
            $data['error_meta_title'] = $this->error['meta_title'];
        } else {
            $data['error_meta_title'] = array();
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['project_id'])) {
            $data['action'] = $this->url->link('catalog/project/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('catalog/project/edit', 'user_token=' . $this->session->data['user_token'] . '&project_id=' . $this->request->get['project_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('catalog/project', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['project_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $project_info = $this->model_catalog_project->getProject($this->request->get['project_id']);
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['project_description'])) {
            $data['project_description'] = $this->request->post['project_description'];
        } elseif (isset($this->request->get['project_id'])) {
            $data['project_description'] = $this->model_catalog_project->getProjectDescriptions($this->request->get['project_id']);
        } else {
            $data['project_description'] = array();
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($project_info)) {
            $data['status'] = $project_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($project_info)) {
            $data['sort_order'] = $project_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }

        $this->load->model('catalog/product');

        if (isset($this->request->post['product_related'])) {
            $products = $this->request->post['product_related'];
        } elseif (isset($this->request->get['project_id'])) {
            $products = $this->model_catalog_project->getProjectRelated($this->request->get['project_id']);
        } else {
            $products = array();
        }

        $data['product_relateds'] = array();

        foreach ($products as $product_id) {
            $related_info = $this->model_catalog_product->getProduct($product_id);

            if ($related_info) {
                $data['product_relateds'][] = array(
                    'product_id' => $related_info['product_id'],
                    'name'       => $related_info['name']
                );
            }
        }

        $this->load->model('tool/image');

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
        /* ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------ KCF ------- */
        /* выбор набора полей */
        $kcf_type = 3; //значит категории
        $data['kcf_types'] = $this->model_extension_kcf->getKcfSet($kcf_type);
        $data['project_id'] = isset($this->request->get['project_id']) ? $this->request->get['project_id'] : $this->model_catalog_project->getNewInfoId();

        /* получим сам набор полей, привязанный к странице */
        $data['kcf_feelds'] = $this->model_extension_kcf->getKcfFeelds($kcf_type, $data['project_id']);
        $data['img_start'] = HTTP_CATALOG . 'image/';

        /* Получим данные для Выбора типа полей */
        $data['kcftypes_now'] = $this->model_extension_kcf->getKcfTypeNow($kcf_type, $data['project_id']);
        /* ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ END KCF ------ */
        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

        // Images
        if (isset($this->request->post['cover_image'])) {
            $cover_image = $this->request->post['cover_image'];
        } elseif (isset($this->request->get['project_id'])) {
            $cover_image = $this->model_catalog_project->getProjectGallery($this->request->get['project_id']);
        } else {
            $cover_image = array();
        }

        $data['cover_image'] = array();

        foreach ($cover_image as $images) {
            if (is_file(DIR_IMAGE . $images['image'])) {
                $image = $images['image'];
                $thumb = $images['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }
            $data['cover_image'][] = array(
                'image'      => $image,
                'title'      => $images['title'],
                'thumb'      => $this->model_tool_image->resize($thumb, 100, 100),
                'sort_order' => $images['sort_order']
            );
        }
        // END Image

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/project_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/project')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['project_description'] as $language_id => $value) {
            if ((utf8_strlen($value['title']) < 1) || (utf8_strlen($value['title']) > 64)) {
                $this->error['title'][$language_id] = $this->language->get('error_title');
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/project')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('setting/store');

        foreach ($this->request->post['selected'] as $project_id) {
            if ($this->config->get('config_account_id') == $project_id) {
                $this->error['warning'] = $this->language->get('error_account');
            }

            if ($this->config->get('config_checkout_id') == $project_id) {
                $this->error['warning'] = $this->language->get('error_checkout');
            }

            if ($this->config->get('config_affiliate_id') == $project_id) {
                $this->error['warning'] = $this->language->get('error_affiliate');
            }

            if ($this->config->get('config_return_id') == $project_id) {
                $this->error['warning'] = $this->language->get('error_return');
            }
        }

        return !$this->error;
    }
}