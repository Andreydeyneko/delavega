<?php
class ControllerCustomerNewsletter extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('newsletter_title'));

        $this->load->model('customer/customer');

        $this->getList();
    }

    protected function getList() {

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('newsletter_title'),
            'href' => $this->url->link('customer/newsletter', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['subscribers'] = array();

        $results = $this->model_customer_customer->getSubscribers();

        foreach ($results as $result) {

            $data['subscribers'][] = array(
                'id'             => $result['id'],
                'email'          => $result['email'],
                'date_added'     => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
            );
        }

        $data['user_token'] = $this->session->data['user_token'];

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/newsletter_list', $data));
    }
}