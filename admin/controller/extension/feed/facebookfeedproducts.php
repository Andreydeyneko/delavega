<?php 

class ControllerExtensionFeedFacebookFeedProducts extends Controller {

	private $error = array(); 

	public function index() {

		$this->load->language('extension/feed/facebookfeedproducts');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		$this->load->model('catalog/option');
        $this->load->model('extension/feed/facebookfeedproducts');
        $data['success'] = '';

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() && !isset($this->request->post['form_ub'])) {
			$this->model_setting_setting->editSetting('facebookfeedproducts', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL'));
		} elseif (isset($this->request->post['google_product_category_url']) && $this->request->post['google_product_category_url'] !='' )  {
            $data1 = array();
            
            if ($file = file($this->request->post['google_product_category_url'])){
                
                $this->model_extension_feed_facebookfeedproducts->clearGoogleCategorys();
              foreach ($file as $f) {  

                if (!strstr($f, '#')){
                    $buffer = explode("-", $f);
                    $google_category_name = htmlentities(trim($buffer[1]));
                    $google_category_id = trim($buffer[0]);
                    $google_category_parent = 0;
                    if (count($buffer1= explode(">", $buffer[1]))>1){
                    //$google_category_name = trim($buffer1[count($buffer1)-1]); 
                    $parent = $this->model_extension_feed_facebookfeedproducts->getGoogleCategoryByName(htmlentities(trim($buffer1[0])));
                    $google_category_parent = $parent["google_category_id"];
                               
                }
                    
                $data1  = array('google_category_id'=> $google_category_id, 'google_category_name'=>$google_category_name, 'google_category_parent'=>$google_category_parent); 
                $this->model_extension_feed_facebookfeedproducts->updateGoogleCategorys($data1);
                }
           
              }
            } else {
              $this->error['warning']='File not found';  
            }
        }
        $data['user_token'] = $this->session->data['user_token'];

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_data_feed'] = $this->language->get('entry_data_feed');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['entry_google_product_category'] = $this->language->get('entry_google_product_category');
		$data['entry_product_category'] = $this->language->get('entry_product_category');
		$data['entry_google_main_product_category'] = $this->language->get('entry_google_main_product_category');
		$data['entry_google_all_product_category'] = $this->language->get('entry_google_all_product_category');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/feed/facebookfeedproducts', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => ' :: '
   		);

		$data['action'] = $this->url->link('extension/feed/facebookfeedproducts', 'user_token=' . $this->session->data['user_token'], 'SSL');
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL');

		if (isset($this->request->post['facebookfeedproducts_status'])) {

			$data['facebookfeedproducts_status'] = $this->request->post['facebookfeedproducts_status'];
			$data['facebookfeedproducts_currency'] = $this->request->post['facebookfeedproducts_currency'];
			$data['facebookfeedproducts_language'] = $this->request->post['facebookfeedproducts_language'];
			$data['facebookfeedproducts_size'] = $this->request->post['facebookfeedproducts_size'];
			$data['facebookfeedproducts_color'] = $this->request->post['facebookfeedproducts_color'];
			$data['facebookfeedproducts_pattern'] = $this->request->post['facebookfeedproducts_pattern'];
			$data['facebookfeedproducts_material'] = $this->request->post['facebookfeedproducts_material'];
		} else {
			$data['facebookfeedproducts_status'] = $this->config->get('facebookfeedproducts_status');
			$data['facebookfeedproducts_currency'] = $this->config->get('facebookfeedproducts_currency');
			$data['facebookfeedproducts_language'] = $this->config->get('facebookfeedproducts_language');
			$data['facebookfeedproducts_size'] = $this->config->get('facebookfeedproducts_size');
			$data['facebookfeedproducts_color'] = $this->config->get('facebookfeedproducts_color');
			$data['facebookfeedproducts_pattern'] = $this->config->get('facebookfeedproducts_pattern');
			$data['facebookfeedproducts_material'] = $this->config->get('facebookfeedproducts_material');
		}
		
		$this->load->model('catalog/category');
        $data1 = '';
        $results = $this->model_catalog_category->getCategories($data1);

		foreach ($results as $result) {
        $google_category_name ='';
        $google_category_id = '';
        $google_category_id = $this->model_extension_feed_facebookfeedproducts->getGoogleCategorysByCategoryId($result['category_id']);
        if ($google_category_id){
            $google_category_name = $this->model_extension_feed_facebookfeedproducts->getGoogleCategoryById($google_category_id['google_category_id']);    
        }

            $data['categories'][] = array(
				'category_id' => $result['category_id'],
				'google_category_id' => ($google_category_id) ? $google_category_id["google_main_category_id"]:'',
				'google_category_name' => ($google_category_name) ? $google_category_name["google_category_name"]:'',
				'name'        => $result['name']
            );
		}
        $results = $this->model_extension_feed_facebookfeedproducts->getGoogleMainCategory();

        foreach ($results as $result) {

			$data['googlemaincategorys'][] = array(
				'google_category_id'    => $result['google_category_id'],
				'google_category_name'  => $result['google_category_name']
				);
		} 
        
        
        
        $data_opt= array();
		$opt = $this->model_catalog_option->getOptions($data_opt);

		foreach ($opt as $op) {

			$data['options'][] = array(
				'name'       => $op['name']
			);
		}
		

		$this->load->model('localisation/currency');
		$mydata=array();
		$currencies = $this->model_localisation_currency->getCurrencies($mydata);

		foreach ($currencies as $result) {
			$data['currencies'][] = array(
				'currency_id'   => $result['currency_id'],
				'title'         => $result['title'],
				'code'          => $result['code'],
				'value'         => $result['value']								
			);
		}	

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages($data);

		foreach ($languages as $result) {
			$data['languages'][] = array(
				'language_id' => $result['language_id'],
				'name'        => $result['name'] ,
				'code'        => $result['code']				
			);		
		}

		
		$data['text_edit'] = 'Edit';
		$data['data_feed'] = HTTP_CATALOG . 'index.php?route=extension/feed/facebookfeedproducts';
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/feed/facebookfeedproducts', $data));

	} 

	

	protected function validate() {

		if (!$this->user->hasPermission('modify', 'extension/feed/facebookfeedproducts')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;

	}
    	public function install() {
		$this->load->model('extension/feed/facebookfeedproducts');
		$this->model_extension_feed_facebookfeedproducts->install();
        $file = file('https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt');
        foreach ($file as $f) {  

            if (!strstr($f, '#')){
                $buffer = explode("-", $f);
                $google_category_name = htmlentities(trim($buffer[1]));
                $google_category_id = trim($buffer[0]);
                $google_category_parent = 0;
                if (count($buffer1= explode(">", $buffer[1]))>1){
                    $parent = $this->model_extension_feed_facebookfeedproducts->getGoogleCategoryByName(htmlentities(trim($buffer1[0])));
                    $google_category_parent = $parent["google_category_id"];
                }
                    
                $data  = array('google_category_id'=> $google_category_id, 'google_category_name'=>$google_category_name, 'google_category_parent'=>$google_category_parent); 
                $this->model_extension_feed_facebookfeedproducts->updateGoogleCategorys($data);
            
            }
           
        }
        
	}

	public function uninstall() {
		$this->load->model('extension/feed/facebookfeedproducts');
		$this->model_extension_feed_facebookfeedproducts->uninstall();
	}
    
	public function savegooglecat() {
		
        if ($this->request->server['REQUEST_METHOD'] == 'GET'){
            $this->load->model('extension/feed/facebookfeedproducts');
            if (isset($this->request->get["google_main_category_id"])) {
                $data = array('category_id'=>$this->request->get["category_id"] , 'google_main_category_id'=> $this->request->get['google_main_category_id'], 'google_category_id'=>NULL);
            }else{
                $data = array('category_id'=>$this->request->get["category_id"] , 'google_main_category_id'=> NULL, 'google_category_id'=>$this->request->get['google_category_id']);
            }
            $this->model_extension_feed_facebookfeedproducts->saveGoogleCategory($data);
            
        }
        
	}
	
    public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
		$this->load->model('extension/feed/facebookfeedproducts');

			$data = array(
				'google_category_name' => $this->request->get['filter_name'],
				'google_category_id'       => $this->request->get['filter_id']
			);


		$results = $this->model_extension_feed_facebookfeedproducts->getGoogleCategorys($data);
			foreach ($results as $result) {
				$json[] = array(
					'google_category_id' => $result['google_category_id'], 
					'google_category_name'            => strip_tags(html_entity_decode($result['google_category_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$this->response->setOutput(json_encode($json));
    }

}

?>